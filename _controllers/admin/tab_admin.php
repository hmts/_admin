<?php
class c_tab_admin extends c_admin
{
//var $includeComponents=array();
//var $perPage="3";

//var $getAllowed=array();
    function _beforeInit($a)
    {
        $a=parent::_beforeInit($a);
        $a['url_mod'] = rootWWW.$a['route']['model']."/tab/".$a['USE_TAB']."/";
        //echo '';
        return $a;
    }

    function _afterInit($a)
    {
        $a=parent::_afterInit($a);
        $a['url_mod'] = rootWWW.$a['route']['model']."/tab/".$a['USE_TAB']."/";
        //echo '';
        return $a;
    }




    function index($a)
    {
        $a=parent::index($a);
        $a['VIEW']=rootDOC."_views/admin/".$a['USE_TAB']."/index.php";



        if(isset($_GET['csv']) && AUTH::isLogin("TAB_aUser"))
        {

            $filename=$a['USE_TAB']."_".date("d-M-Y-his").".csv";
            if(!empty($_GET['csv']))
            {
                //Can use $_GET['csv'] to set the name of the file
                $filename=$_GET['csv']."_".date("d-M-Y-his").".csv";
            }
            //pr("START");
            //Verifico se la view personalizzata esiste altrimenti carico la base di default
            $view_CSV=rootDOC."_views/admin/".$a['USE_TAB']."/csv.php";
            if(!file_exists($view)){
                $view_CSV=rootDOC."_views/admin/csv.php";
            }
                    //pr($view_CSV);
            setExcelOutput($a,array(
                'views'=>$view_CSV,
                'file_name'=>$filename,
                'title'=>"disponibilita-".date("d-M-Y-his")."",
                'csv'=>true,
                //'echo'=>true,
                ));
            exit;
        }

        return $a;
    }


    function makeLinkBack($a)
    {

        $URL_NOW=rootWWW.$a['__URL'];

        if($_SERVER['HTTP_REFERER']!=$URL_NOW)
        {
        $_SESSION['LINK_BACK']=$_SERVER['HTTP_REFERER'];
        }

        $a['LINK_BACK']=$_SESSION['LINK_BACK'];

            //pr(rootWWW.$a['__URL']);
            //pr($_SESSION['LINK_BACK']);

        if(substr(str_replace(rootWWW,"",$a['LINK_BACK']),0,5)!="admin")
        {
            $a['LINK_BACK']=$a['url_mod'];
        }
        //pr($a['LINK_BACK']);
        return $a;
    }
    function upd($a)
    {
        $a=parent::upd($a);
        $a['VIEW']=rootDOC."_views/admin/".$a['USE_TAB']."/upd.php";
        $a=$this->makeLinkBack($a);

        return $a;
    }

    function _new($a)
    {
        $a=parent::_new($a);
        $a['VIEW']=rootDOC."_views/admin/".$a['USE_TAB']."/new.php";
        $a=$this->makeLinkBack($a);
        return $a;
    }






    function print_record($a)
    {
        $a=parent::print_record($a);
        $a['TEMPLATE_DIR']=rootDOC."_template/_admin/print/";
        $a['VIEW']=rootDOC."_views/admin/".$a['USE_TAB']."/print.php";
        return $a;
    }


    function del_img($a)
    {
        /*
            del_img()

            No View for this action
            only set data and call the function
            for delete files, setting the SET => IMG

        */
        //pr($a);
        //$a['GET_DATA']['del_img'];
        //echo $a['USE_TAB'];

            //$db_files['IMG'];

            $r=dbAction::_record(array('tab'=>$a['USE_TAB'],'value'=>$a['GET_DATA']['del_img'],'echo'=>'0'));
            //pr($r);
            $DELETE_FILES=_CORE::delete_db_files($r,$_GET['set']);
            $URL_UPD=rootWWW.$a['route']['model']."/tab/".$a['USE_TAB']."/upd/".$a['GET_DATA']['del_img']."/";

            //echo "<br>".$URL_UPD;
            Header("location: ".$URL_UPD);
            exit;

        //return $a;
    }



    function zipAll($a)
    {
        //$a=parent::zipAll($a);
        //$a['TEMPLATE_DIR']=rootDOC."_template/_admin/print/";
        //$a['VIEW']=rootDOC."_views/admin/".$a['USE_TAB']."/print.php";





        if(isset($_SESSION['AUTH']) && !empty($_SESSION['AUTH']['_aUser'])){
          /*ini_set('memory_limit', '-1');
          ini_set('max_execution_time', 300); //300 seconds = 5 minutes
          error_reporting(E_ALL);
          ini_set('display_errors', '1');
          */


          $a['VIEW'] = rootDOC . "_views/admin/".$a['USE_TAB']."/download.php";

          global $rootDOC;


          //pr($_SESSION);
          /*
          $root = $rootDOC. '_files';
          $iter = new RecursiveIteratorIterator(
              new RecursiveDirectoryIterator($root, RecursiveDirectoryIterator::SKIP_DOTS),
              RecursiveIteratorIterator::SELF_FIRST,
              RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
          );

          $paths = array($root);
          foreach ($iter as $path => $dir) {
              if (!$dir->isDir()) {
                  $paths[] = $path;
              }
          }
          */
          //POther Path to EMLCms
          $rootDOC = str_replace("//","/",$rootDOC);
          $rootDOC2 = $rootDOC;
          $rootDOC2 = str_replace('/_admin/',"",$rootDOC2);
          $rootDOC2 = str_replace("/admin/","",$rootDOC2);
          $rootDOC2 = str_replace("/var/www/site/","",$rootDOC2);

          $nomeProgetto = $rootDOC2;

          $progettiTroppoPesanti = array(
            '7hillstourscom',
            '7hillstours.com',
            'iccrs.org',
            'agenzia-immobiliare-roma.com',
            'falegnameriaindustriale.it',
            'letsamore.eu',
            'massimopelliccioniit',
            'rotaryclubromasudest.it',
            'sistemaguida2000.it'

          );
          $nomeProgetto = rtrim($nomeProgetto, '/');

          $a['_files_troppo_pesanti_'] = false;
          if(in_array($nomeProgetto,$progettiTroppoPesanti)){
            $a['_files_troppo_pesanti_'] = true;

          }

          $rootDOC2 = "/var/www/site/" . $rootDOC2 . "/EMLcms/";
          $rootDOC_EML_config = $rootDOC2 . "app/config/";

          //$root = $rootDOC_EML_config . '_files';
          $root = $rootDOC . '_files';
          //pr($rootDOC);
          //pr($root);
          //$root = substr_replace($root,"//","/");

          $filesCheck = scandir( $root );
          //pr($files);

          $fileZipExists = false;
          foreach($filesCheck as $fileCheck) {
            $needleCheck = ".gz";
            $lengthCheck = strlen($needleCheck);
            if( !is_dir($fileCheck) && (substr($fileCheck, -$lengthCheck) === $needleCheck) ){
              $fileZipExists = true;
            }
          }

          $a['fileZipExists'] = $fileZipExists;
          if($fileZipExists){

            return $a;
          }


          //pr($root);
          if(!$a['_files_troppo_pesanti_']){
            $iter = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($root, RecursiveDirectoryIterator::SKIP_DOTS),
                RecursiveIteratorIterator::SELF_FIRST,
                RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
            );

            $paths = array($root);
            foreach ($iter as $path => $dir) {
                if (!$dir->isDir()) {
                    $paths[] = $path;
                }
            }
          }



          //$paths[] =
          //pr($paths);
          //exit;
          if(file_exists($rootDOC. '_config/config.yml')){
              $paths[] = $rootDOC. '_config/config.yml';
          }
          if(file_exists($rootDOC. '_config/parameters.yml')){
              $paths[] = $rootDOC. '_config/parameters.yml';
          }

          $files = $paths;

          /**/
          //$zipname = $nomeProgetto . '_' . time() .  '.tar';

          $zipname = $nomeProgetto . '.tar';

          //$zip = new ZipArchive;
          //$zip->open($rootDOC . "_files/" . $zipname, ZipArchive::CREATE);
          //$zip->open($zipname, ZipArchive::CREATE);
          //$filename = $zip->filename;

          $filename = $rootDOC . "_files/" . $zipname;
          //pr($filename);
          $zip = new PharData($filename);


          foreach ($files as $file) {

            //if( !is_dir($file) && !substr($file, 0, 1) === '.'  ){
            $needle = ".gz";
            $length = strlen($needle);
            $needle2 = ".tar";
            $length2 = strlen($needle2);
            if( !is_dir($file) && !(substr($file, -$length) === $needle)  && !(substr($file, -$length2) === $needle2)  ){
              //pr($file);
              if(file_exists($file) && strlen($file)<100 ){
                  $zip->addFile($file);
              }

            }
          }
          $zip->compress(Phar::GZ);
          //$zip->close();
          /**/

          /*
          header('Content-Type: application/zip');
          header("Content-Disposition: attachment; filename='" . $zipname . "'");
          //header('Content-Length: ' . filesize($zipname));
          //header("Location: " . $filename);
          readfile($filename);
          unlink($filename);
          */


          /*
          header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
          //header('Content-Type: application/zip;\n');
          header("Content-Type: application/x-tar");
          header("Content-Transfer-Encoding: Binary");
          //header("Content-Disposition: attachment; filename=\"".basename($zipname . ".gz")."\"");
          header("Content-Disposition: attachment; filename='".$zipname . ".gz'");



          //header("Content-Disposition: attachment; filename='".$tar."'");
          //header("Content-Length: $size");
          //header("Content-Transfer-Encoding: binary");


          readfile($filename . ".gz");

          unlink($filename);
          unlink($filename . ".gz");

          exit();
          */
          /**/


        }

        return $a;
    }

}
