<?php
class c_admin extends cAll
{
//var $includeComponents=array();
//var $perPage="3";

//var $getAllowed=array();

var $getAllowed=array(
    //'xml'=>array('validation'=>'is_only_set','login'=>'0'),
    'db_bu'=>array('validation'=>'is_only_set','login'=>'1'),
    'db_data'=>array('validation'=>'is_only_set','login'=>'2'),
    'ftp_data'=>array('validation'=>'is_only_set','login'=>'2'),
    'ftp'=>array('validation'=>'is_only_set','login'=>'2'),
    'content_page'=>array('validation'=>'is_only_set','login'=>'1'),

//    'sub_admin'=>array('validation'=>'is_only_set','SUB_MODEL'=>'sub_admin','login'=>'1','login_set'=>'1'),

    /*
    If validation is: 'is_in_array'
    need to pass name of the array where to looking for
    MUST BE in a external ARRAY (the ROUTING class will globalized it)
    OR in the $a array...
    */
    'prodotti_search'=>array('SUB_MODEL'=>'prodotti_search','validation'=>'is_only_set','action'=>'set_json_array'),
    'ordini_search'=>array('SUB_MODEL'=>'ordini_search','validation'=>'is_only_set','action'=>'set_json_array'),

    'autori_search'=>array('SUB_MODEL'=>'autori_search','validation'=>'is_only_set','action'=>'set_json_array'),
    'tab'=>array('SUB_MODEL'=>'tab_admin','validation'=>'is_in_array','array'=>'TAB_ADMIN_ARRAY','action'=>'index','multi_child'=>
                 array(
                       0=>array('new'=>array('validation'=>'is_one','action'=>'new')),
                       1=>array('del'=>array('validation'=>'is_natural_number','action'=>'del')),
                       2=>array('upd'=>array('validation'=>'is_natural_number','action'=>'upd')),
                       3=>array('id_editori'=>array('validation'=>'is_natural_number')),
                       4=>array('p'=>array('validation'=>'is_natural_number')),
                       5=>array('del_img'=>array('validation'=>'is_natural_number','action'=>'del_img')),
                       6=>array('print'=>array('validation'=>'is_natural_number','action'=>'print_record')),
                       7=>array('zipAll'=>array('validation'=>'is_one','action'=>'zipAll')),

                       ),
                 ),

);



    function _beforeInit($a)
    {
        $a=parent::_beforeInit($a);
        return $a;
    }



    function tab($a)
    {
        //pr($a);
        //pr($a['GET_DATA']);
        $tab_view=rootDOC."_views/admin/tab/".$a['GET_DATA']['tab']."/index.php";
        /*
        Uncommenting next line allow to have a default view for missing tab view
        */
        //if(!file_exists($tab_view))
        //{
        //$tab_view=rootDOC."_views/admin/tab/index_tab_default.php";
        //}
        //pr($tab_view);
        $a['VIEW']=$tab_view;

        return $a;
    }



    function xml($a)
    {
        $a['VIEW']=rootDOC."_views/admin/xml/index.php";
        return $a;
    }






    function db_bu($a)
    {

        $DB_DATA=connect(array('bu'=>true));
        //pr($DB_DATA);

        $BU_dir=rootDOC."_files/db/";
        $BU_file=$BU_dir.time().".sql";
        $files=_CORE::dirToArray($BU_dir,array('type'=>'file','exclude_files'=>array('.htaccess')));
        $a['files_db_bu']=$files;

        $BU_dir_WWW=str_replace(rootDOC,rootWWW,$BU_dir);
        $a['BU_dir_WWW']=$BU_dir_WWW;
        $a['BU_dir']=$BU_dir;


        if(isset($a['GET_DATA']['download']))
        {
            //filesize
            $F_name=$a['GET_DATA']['download'].".sql";
            $F=$BU_dir.$F_name;
            header('Content-type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.$F_name.'"');

            readfile($F);

            //$fp = fopen($F, "r");
            //fpassthru($fp);
            //fclose($fp);
            exit;
            //_CORE::redirect(array('location'=>rootWWW."".$a['GET_DATA']['url']));
        }


        if(isset($a['GET_DATA']['del']))
        {
        unlink($BU_dir.$_GET['del'].".sql");
        _CORE::redirect(array('location'=>rootWWW."".$a['GET_DATA']['url']));
        }
        if(isset($a['GET_DATA']['SAVE']))
        {
        $C="mysqldump ".$DB_DATA['n']." >".$BU_file." -u ".$DB_DATA['u']." -p".$DB_DATA['p']."";
        //$C="mysqldump ".$DB_DATA['n']." -u ".$DB_DATA['u']." -p".$DB_DATA['p']." | gzip -c > ".$BU_file.".gz";

        //pr($C);
        system($C);
        //pr(rootWWW."".$a['GET_DATA']['url']);
        _CORE::redirect(array('location'=>rootWWW."".$a['GET_DATA']['url']));

        }

        $a['VIEW']=rootDOC."_views/admin/db_bu.php";
        return $a;
    }



    function content_page($a)
    {
        $a['VIEW']=rootDOC."_views/admin/content_page.php";
        return $a;
    }

    function db_data($a)
    {

        if($_SESSION['AUTH']['_aUser']['id_a_utenti_cat']!="2")
            $a['STATUS']="404";
        $a['VIEW']=rootDOC."_views/admin/db_data.php";
        return $a;
    }


    function ftp_data($a)
    {

        if($_SESSION['AUTH']['_aUser']['id_a_utenti_cat']!="2")
            $a['STATUS']="404";
        $a['VIEW']=rootDOC."_views/admin/ftp_data.php";
        return $a;
    }


    function ftp($a)
    {

        if($_SESSION['AUTH']['_aUser']['id_a_utenti_cat']!="2")
            $a['STATUS']="404";
        $a['VIEW']=rootDOC."_views/admin/ftp.php";
        return $a;
    }




}
?>
