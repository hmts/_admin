<?php
if(isset($_GET['rename-images'])){

  function buildValue($from,$record,$a){
    $output = "";
    foreach($from as $k=>$f){
      if(array_key_exists($f, $record)){
        if($f=="slug" && empty($record[$f])){
          $slug = slug($record['h1']);
          $record[$f] = $slug;
          $data = array( 'tab'=>$a['USE_TAB'], 'id'=>$record['id'], 'data'=>array( 'slug'=>$slug ) );
          dbAction::_update($data);
        }
          $output=$output.$record[$f];
      }else{
        $output=$output.$f;
      }
    }
    return $output;
  }


  if($_POST['rename-images']){
    //Include _db_files
    $db_files_file = rootDOC . "_config/_db_files/" . $a['USE_TAB'] . ".php";
    if(file_exists($db_files_file)){
        include($db_files_file);
    }

    if(!$db_files){
        echo '<p>ERRORE NESSUN FILE DI CONFIGURAZIONE IMMAGINE TROVATO<br>' . $db_files_file . '</p>';
    }
    echo '<p>rename-images</p>';


    $imagesDir = rootDOC . $db_files['IMG']['upload_path'];
    $LOOP = $a['LOOP'][$a['USE_TAB']];
    $from = explode(";",$_POST['from']);
    $to = explode(";",$_POST['to']);
    if($LOOP){
      foreach($LOOP as $k=>$v){
        $fromValue = buildValue($from,$v,$a);
        $toValue = buildValue($to,$v,$a);

        foreach ($db_files['IMG']['image_sets'] as $key => $value) {

          $fromFile = $imagesDir . $key . "/". $fromValue . $value['est'];
          $toFile   = $imagesDir . $key . "/". $toValue   . $value['est'];
          if(file_exists($fromFile)){
            pr("RENAME - FROM: " . $fromFile . " - TO: " . $toFile);
            rename($fromFile,$toFile);
          }
        }
      }
    }

  }


echo '
<form action="" method="post" style="margin:30px;padding:20px;background:#efefef;border-radius:5px;">
<input type="hidden" name="rename-images" value="1">
<input type="text" name="from" value="id" style="font-size:18px;">
<input type="text" name="to" value="id;_;slug" style="font-size:18px;">
<input type="submit" value="rinomina immagini" style="font-size:18px;">

</form>
';



  exit;
}
