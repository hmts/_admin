<?php
//pr($LIBRI);


$makePagination=New makePagination();
$Back=$makePagination->Prev("Indietro",$a['pagination_data']);
$Next=$makePagination->Next("Avanti",$a['pagination_data']);
//$Numbers=$makePagination->Numbers($a['pagination_data'],true); (if==true output in html if not is an array)
$Numbers=$makePagination->Numbers($a['pagination_data']);
$totPages=$a['pagination_data']['tot_pages'];
$totRecords=$a['pagination_data']['tot_records'];
$currentPage=$a['pagination_data']['current_page'];
$urlLastPage=$a['pagination_data']['url_last'];
//pr($a['pagination_data']);

if(empty($a['NAME_OF_TAB']))$a['NAME_OF_TAB']=$a['USE_TAB'];
?>    


<?php if($totPages>1){ ?>
<div id="pagination">


<ul class="numbers">

<li class="tit"><?php echo $a['NAME_OF_TAB']; ?> &nbsp;</li>

<li><?php echo $Back; ?></li>

<?php for($i=0;$i<count($Numbers);$i++){?>
<li class="<?php echo $Numbers[$i]['class'];?>"><a href="<?php echo $Numbers[$i]['url']; ?>"><?php echo $Numbers[$i]['n']; ?></a></li>
<?php } ?>

<li><?php echo $Next; ?></li>
</ul>

    <div class="info">
    Pagina: <?php echo $currentPage; ?> di
    <a href="<?php echo $urlLastPage; ?>/"><?php echo $totPages; ?></a> (<?php echo $totRecords; ?> records)
    </div>

<div id="clear"></div>
</div>
<div id="clear"></div>
<?php } else { ?>
<div id="pagination">
<ul class="numbers">
<li class="tit">Totale <?php echo $a['NAME_OF_TAB']; ?> &nbsp;</li>
<li><?php echo $totRecords;?></li>
</ul>
<div id="clear"></div>
</div>
<?php } ?>