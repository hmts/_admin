<?php
//pr($_SESSION['FILTERS']);
if(isset($_SESSION['FILTERS'][$a['USE_TAB']][$TAB_Element]))
{
    
    $idEle = $_SESSION['FILTERS'][$a['USE_TAB']][$TAB_Element]['value'];
    $ele = dbAction::_record(array('tab'=>$TAB_Element,'value'=>$idEle,'echo'=>0));
    $eleUri = rootWWW.'admin/tab/'.$TAB_Element.'/upd/'.$idEle."/";
    echo '<div style="font-size:11px;padding:9px;background:#efefef;border-top:#afafaf 1 solid;">';
    echo '<img src="'.$a['TEMPLATE_DIR_WWW'].'_img/alert_body_msg.png" '
         . 'style="width:50px;height:50px;float:left;margin:10px 15px 0 0;">';
    echo 'In visualizzazione l\'elenco di '.$a['USE_TAB'].'<br>per l\'elemento: '
         . '<a href="'.$eleUri.'" style="color:green;font-size:15px;">'
         . '<strong>'.$ele['h1'].'</strong>'
         . '</a>'   
         . '<br>';
    echo '<a class="back" style="display:inline-block;float:none;width:auto;padding:5px;margin:5px 0 0 0;" '
         . 'href="'.$eleUri.'">';
    echo 'Vai alla scheda dell\'elemento';
    echo '</a>';
    echo '<div style="clear:both;"></div>';
    echo '</div>';
}

//Gategory
else if(isset($_SESSION['FILTERS'][$a['USE_TAB']][$TAB_Category]))
{
    
    $idCat = $_SESSION['FILTERS'][$a['USE_TAB']][$TAB_Category]['value'];
    $cat = dbAction::_record(array('tab'=>$TAB_Category,'value'=>$idCat,'echo'=>0));
    $catUri = rootWWW.'admin/tab/'.$TAB_Category.'/upd/'.$idCat."/";
    echo '<div style="font-size:11px;padding:9px;background:#efefef;border-top:#afafaf 1 solid;">';
    echo '<img src="'.$a['TEMPLATE_DIR_WWW'].'_img/alert_body_msg.png" '
         . 'style="width:50px;height:50px;float:left;margin:10px 15px 0 0;">';
    echo 'In visualizzazione l\'elenco di '.$a['USE_TAB'].'<br>per la categoria: '
         . '<a href="'.$catUri.'" style="color:green;font-size:15px;">'
         . '<strong>'.$cat['h1'].'</strong>'
         . '</a>'   
         . '<br>';
    echo '<a class="back" style="display:inline-block;float:none;width:auto;padding:5px;margin:5px 0 0 0;" '
         . 'href="'.$catUri.'">';
    echo 'Vai alla scheda della categoria';
    echo '</a>';
    echo '<div style="clear:both;"></div>';
    echo '</div>';
}