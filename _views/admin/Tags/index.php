<?php include(_CORE::addView("admin/Tags/filtri")); ?>

<?php //pr($_SESSION['FILTERS']); ?>

<?php //pr($a['USE_TAB']); ?>
<?php $LOOP=$a['LOOP'][$a['USE_TAB']]; ?>

<?php include(_CORE::addView("admin/_elements/pagination")); ?>
<?php include(_CORE::addView("admin/_elements/go_to_filter_page")); ?>


<?php if($LOOP){ ?>


<table class="index" id="tableProdotti">
    
    <tr class="header">
        
        <td class="help" onmouseover="toolTip('LANG<br>La lingua del record nella pagina')" onmouseout="toolTip()">
            Lang</td>
        <td >
            Tag</td>
        
        <td class="help" onmouseover="toolTip('Area<br>Indica se il link è assegnato ad una area')" onmouseout="toolTip()">
            Area</td>
        <td class="help" onmouseover="toolTip('Categoria<br>Indica se il link è assegnato ad una categoria')" onmouseout="toolTip()">
            Categoria</td>
        
        
        <td colspan="2">&nbsp;</td>
    </tr>
    
    
    <?php foreach($LOOP as $k=>$v){ ?>
    
    <?php
    $url_mod=rootWWW."admin/tab/".$a['USE_TAB']."/upd/".$v['id']."/";
    $url_del=rootWWW."admin/tab/".$a['USE_TAB']."/del/".$v['id']."/";
    ?>
    
    
    <?php 
    $errorOnLine = FALSE;
    if(!empty($v['REL'][$TAB_Area]) && !empty($v['REL'][$TAB_Category]['h1'])){ 
        $errorOnLine = "<b style=color:red;>ATTENZIONE Errore su questa riga</b>:"
                     . "<br>Assegnazione doppia ad area: "
                     . "<br>".$v['REL'][$TAB_Area]['h1']
                     . " e a categoria: ".$v['REL'][$TAB_Category]['h1']
                     . "<br>Assegnare solo o una categoria o un area."
                     . "";
    } 
    ?>
    <tr ondblclick="javascript:location='<?php echo $url_mod; ?>'" class="<?php echo $v['tr_class']." ".$addClass; ?>"<?php
        if($errorOnLine){
            echo ' style="background:#ffe4e4;" ';
            echo ' onmouseover="toolTip(\''. $errorOnLine .'\')" onmouseout="toolTip()" ';
        }
    ?>>
    
    <td style="text-align: center;"><?php echo strtoupper($v['lang']); ?> </td>
    
    <td width="33%">
        <?php echo $v['text']; ?>
    
    </td> 
    
    <td width="33%">
    <?php 
    if(!empty($v['REL'][$TAB_Area]['h1'])){ 
        echo '<a class="back" href="'.rootWWW."admin/tab/".$TAB_Area.'/upd/'.$v['REL'][$TAB_Area]['id'].'/" '
                . ' onmouseover="toolTip(\'Clicca per modificare la categoria: <b>'.$v['REL'][$TAB_Area]['h1'].'</b> \')" onmouseout="toolTip()" '
                . ' style="width:auto;" >';
            echo "".$v['REL'][$TAB_Area]['h1']."";
            echo "</a>";
    } 
    ?>
    </td> 

    <td width="33%">
    <?php 
    if(!empty($v['REL'][$TAB_Category]['h1'])){ 
        echo '<a class="back" href="'.rootWWW."admin/tab/".$TAB_Category.'/upd/'.$v['REL'][$TAB_Category]['id'].'/" '
                . ' onmouseover="toolTip(\'Clicca per modificare la categoria: <b>'.$v['REL'][$TAB_Category]['h1'].'</b> \')" onmouseout="toolTip()" '
                . ' style="width:auto;" >';
            echo "".$v['REL'][$TAB_Category]['h1']."";
            echo "</a>";
    } 
    ?>
    </td>
    
    
    
    <td><a class="upd" href="<?php echo $url_mod; ?>" ><?php echo __('modifica');?></a></td>
    <td><a class="del" href="<?php echo $url_del; ?>" onclick="return confirm('<?php echo __('Confermi Eliminazione?',false);?>');"><?php echo __('elimina');?></a></td>
    
    
    </tr>
    <?php } ?>
        
</table>



<?php include(_CORE::addView("admin/_elements/pagination")); ?>


<?php } else { ?>
<div class="norecordfound">nessun record trovato</div>
<?php } ?>
