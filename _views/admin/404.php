
<br />
<div style="text-align:left;">
<img src="<?php echo $a['TEMPLATE_DIR_WWW'];?>_img/404.png" />
<br />
Page Not Found<br />
<br /><br />
<?php
if(isset($a['GET_DATA']['upd']))
{
    //Check if the current record is on other language
    $rL = dbAction::_record(array(
        'tab'=>$a['GET_DATA']['tab'],
        'value'=>$a['GET_DATA']['upd']
    ));
    //pr($rL);
    
    if($rL)
    {
        //There is a record in other language,
        //Display it:
        $languages = getLanguages();
        echo 'Hai impostato la lingua: '.$_SESSION['Language'][key($_SESSION['Language'])]."<br>";
        echo 'E stai visitiando una risorsa  disponibile in '.$languages[$rL['lang']];
        echo '<br>';
        echo 'Per visualizzare la risorsa corrente seleziona la lingua '
             . '"<a style="color:blue;" href="?setLanguage='.$rL['lang'].'">'.$languages[$rL['lang']].'</a>"';
        echo '<br><br>';
        echo 'Se invece vuoi creare una traduzione<br>per questa risorsa clicca su '
            . ' "<a style="color:blue;" href="'.rootWWW."admin/tab/".$a['GET_DATA']['tab'].'/new/1/">crea nuovo record</a>"';
        echo '<br><br>';
        
    }
}
?>

</div>