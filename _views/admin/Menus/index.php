<?php include(_CORE::addView("admin/Menus/filtri")); ?>

<?php //pr($_SESSION['FILTERS']); ?>

<?php //pr($a['USE_TAB']); ?>
<?php
//$LOOP=$a['LOOP'][$a['USE_TAB']];

$AND = NULL;
//pr($_SESSION['FILTERS'][$TAB_Menus][$TAB_Menustype]);
if( isset($_SESSION['FILTERS'][$TAB_Menus][$TAB_Menustype]) && !empty($_SESSION['FILTERS'][$TAB_Menus][$TAB_Menustype]) )
    $AND=" AND ".strtolower($_SESSION['FILTERS'][$TAB_Menus][$TAB_Menustype]['key'])."='".$_SESSION['FILTERS'][$TAB_Menus][$TAB_Menustype]['value']."'";

if(isset($_SESSION['Language'])){
    $AND.=" AND lang='".key($_SESSION['Language'])."' ";
}

//if($_SESSION['FILTERS'][$a['USE_TAB']])
$LOOP=dbAction::_loop(array(
    'tab'=>$TAB_Menus,
    'where'=>" WHERE id_menus is null ".$AND,
    'orderby'=>"ORDER BY visibility DESC, id_menustype, weight, id DESC",
    //'echo'=>true
    ));
?>


<?php include(_CORE::addView("admin/_elements/pagination")); ?>
<?php include(_CORE::addView("admin/_elements/go_to_filter_page")); ?>


<?php if($LOOP){ ?>


<table class="index" id="tableProdotti">

    <tr class="header">
        <td class="help" onmouseover="toolTip('Weight<br>Indica la posizione negli elenchi del link nel menu')" onmouseout="toolTip()">
            </td>
        <td class="help" onmouseover="toolTip('Lang<br>Indica in quale lingua appare il menu')" onmouseout="toolTip()">
            Lang</td>
        <td class="help" onmouseover="toolTip('MenuType<br>Indica in quale menu si va a posizionare questo link')" onmouseout="toolTip()">
            Type</td>
        <td class="help" onmouseover="toolTip('Label<br>Si tratta del testo dell\'anchor della voce di menu ')" onmouseout="toolTip()">
            Label</td>
        <td colspan="2">&nbsp;</td>
    </tr>


    <?php foreach($LOOP as $k=>$v){ ?>

    <?php
    $url_mod=rootWWW."admin/tab/".$a['USE_TAB']."/upd/".$v['id']."/";
    $url_del=rootWWW."admin/tab/".$a['USE_TAB']."/del/".$v['id']."/";

    $addClass="isVis";
    if($v['visibility']=="0")
    {
        $addClass="notVis";
    }
    ?>


    <tr ondblclick="javascript:location='<?php echo $url_mod; ?>'" class="<?php echo $v['tr_class']." ".$addClass; ?>">
        <!--
    <td><?php if($v['IMG']['min']) { ?><img src="<?php echo $v['IMG']['min']['url']; ?>" width="30"><?php } ?></td>
        -->

    <td style="text-align: center;"><?php echo $v['weight']; ?> </td>
    <td style="text-align: center;"><?php echo strtoupper($v['lang']); ?> </td>
    <td>
      <?php echo $v['REL'][$TAB_Menustype]['text']; ?>
    </td>

    <td width="75%">
        <b>
          <?php echo $v['label']; ?>
        </b></td>
    <td><a class="upd" href="<?php echo $url_mod; ?>" ><?php echo __('modifica');?></a></td>
    <td><a class="del" href="<?php echo $url_del; ?>" onclick="return confirm('<?php echo __('Confermi Eliminazione?',false);?>');"><?php echo __('elimina');?></a></td>
    </tr>
        <?php
        $LOOP2[$v['id']]=dbAction::_loop(array(
            'tab'=>$TAB_Menus,
            'where'=>" WHERE id_menus='".$v['id']."' ",
            'orderby'=>"ORDER BY visibility DESC, id_menustype, weight, id DESC",
            ));

        if(!empty($LOOP2[$v['id']]))
        {
            foreach($LOOP2[$v['id']] as $v2){
            $url_mod2=rootWWW."admin/tab/".$a['USE_TAB']."/upd/".$v2['id']."/";
            $url_del2=rootWWW."admin/tab/".$a['USE_TAB']."/del/".$v2['id']."/";
            ?>
        <tr ondblclick="javascript:location='<?php echo $url_mod2; ?>'" class="<?php echo $v2['tr_class']; ?>">
            <!--
        <td><?php if($v2['IMG']['min']) { ?><img src="<?php echo $v2['IMG']['min']['url']; ?>" width="30"><?php } ?></td>
            -->
        <td ><?php echo $v['weight']; ?>.<?php echo $v2['weight']; ?>
        <td style="text-align: center;"><?php echo strtoupper($v2['lang']); ?> </td>

        </td>
        <td ><?php echo $v['REL'][$TAB_Menustype]['text']; ?></td>
        <td >
        &nbsp;└─> <b<?php if($v2['visibility']!="1"){echo ' style="text-decoration:line-through;color:#909090;" title="non visibile"';}?>>
          <?php echo $v2['label']; ?>
        </b>

        </td>
        <td><a class="upd" href="<?php echo $url_mod2; ?>" ><?php echo __('modifica');?></a></td>
        <td><a class="del" href="<?php echo $url_del2; ?>" onclick="return confirm('<?php echo __('Confermi Eliminazione?',false);?>');"><?php echo __('elimina');?></a></td>
        </tr>

            <?php
            }
        }
        ?>
    <?php } ?>

</table>



<?php include(_CORE::addView("admin/_elements/pagination")); ?>


<?php } else { ?>
<div class="norecordfound">
  nessun record trovato
  <form method="post">
    <input type="hidden" name="copy_tab_records" value="<?php echo $a['USE_TAB']; ?>">
  Copia da
  <input type="text" name="lang">
  <input type="submit" name="go">
  </form>
</div>
<?php } ?>
