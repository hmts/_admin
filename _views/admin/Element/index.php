<?php include(_CORE::addView("admin/_elements/rename-images")); ?>
<?php include(_CORE::addView("admin/Element/filtri")); ?>

<?php //pr($_SESSION['FILTERS']); ?>

<?php //pr($a['USE_TAB']); ?>
<?php $LOOP=$a['LOOP'][$a['USE_TAB']]; ?>

<?php include(_CORE::addView("admin/_elements/pagination")); ?>
<?php include(_CORE::addView("admin/_elements/go_to_filter_page")); ?>


<?php $oO=New outputfObj(); ?>


<?php if($LOOP){ ?>


<table class="index" id="tableProdotti">

    <tr class="header">
        <td>Img</td>

        <td class="help" onmouseover="toolTip('Data di creazione')" onmouseout="toolTip()">
            Cre.</td>
        <td class="help" onmouseover="toolTip('Data ultima modifica')" onmouseout="toolTip()">
            Mod.</td>


        <td class="help" onmouseover="toolTip('Extra<br>Informazionin extra sul record<br>Alcune voci sono cliccabili')" onmouseout="toolTip()">
            Extra</td>


        <td class="help" onmouseover="toolTip('Weight<br>Indica la posizione negli elenchi del record')" onmouseout="toolTip()">
            </td>
        <td class="help" onmouseover="toolTip('LANG<br>La lingua del record nella pagina')" onmouseout="toolTip()">
            Lang</td>

        <td class="help" onmouseover="toolTip('H1<br>Il Titolo del record nella pagina')" onmouseout="toolTip()">
            H1</td>
        <td class="help" onmouseover="toolTip('H1<br>Il Sottotitolo del record nella pagina')" onmouseout="toolTip()">
            H2</td>
        <td class="help" onmouseover="toolTip('Text<br>Il Testo del record nella pagina<br>in questa colonna se presente viene anche visualizzato l\'abstract')" onmouseout="toolTip()">

            text</td>
        <td colspan="2">&nbsp;</td>
    </tr>
    <?php foreach($LOOP as $k=>$v){ ?>

    <?php
        $url_mod=rootWWW."admin/tab/".$a['USE_TAB']."/upd/".$v['id']."/";
        $url_del=rootWWW."admin/tab/".$a['USE_TAB']."/del/".$v['id']."/";

    $addClass="isVis";
    if($v['isaccessible']=="0")
    {
        $addClass="notVis";
    }

    ?>


    <tr ondblclick="javascript:location='<?php echo $url_mod; ?>'" class="<?php echo $v['tr_class']." ".$addClass; ?>">

    <td><?php if($v['IMG']['s']) { ?><img src="<?php echo $v['IMG']['s']['url']; ?>" ><?php } ?></td>

    <td><?php echo $oO->dateCalendar($v['createdon']);?></td>
    <td><?php echo $oO->dateCalendar($v['modifyon']);?></td>




    <td style="text-align: center;">
        <table class="info">
            <!--
        <tr><td>View</td><td><?php if(empty($v['view']))echo 'default'; else echo $v['view']; ?></td></tr>
            -->
            <!--
        <tr>
        <td>Cat</td>
        <td>
        <?php /*
        if(!empty($v['REL'][$TAB_Category])){
            foreach ($v['REL'][$TAB_Category] AS $I)
            {
                echo ' '.$I['h1'].' ';
            }
        }*/
        ?>
        </td>
        </tr>
        -->
        <tr>
        <td>Tag</td>
        <td>
        <?php
        if(!empty($v['REL'][$TAB_Tags])){
            foreach ($v['REL'][$TAB_Tags] AS $I)
            {
                echo ' '.$I['text'].' ';
            }
        }

        ?>
        </td>
        </tr>


        <tr>
        <td><a href="<?php echo rootWWW."admin/tab/".$TAB_Attachment."/?setS_id_".$TAB_Element."=".$v['id'] ?>">Att.</a></td>
        <td>
        <?php
        $R = dbAction::_loop(array(
            'relations'=>FALSE,
            'tab'=>$TAB_Attachment,'where'=>"WHERE id_element='".$v['id']."'"));
        if($R){
            echo count($R)."";
        }else echo "0";
        ?>
        </td>
        </tr>
        <tr>
        <td><a href="<?php echo rootWWW."admin/tab/".$TAB_Images."/?setS_id_".$TAB_Element."=".$v['id'] ?>">Imgs</a></td>
        <td>
        <?php
        $R = dbAction::_loop(array(
            'relations'=>FALSE,
            'tab'=>$TAB_Images,'where'=>"WHERE id_element='".$v['id']."'"));
        if($R){
            echo count($R)."";
        }else echo "0";
        ?>
        </td>
        </tr>

        <tr>
        <td><a href="<?php echo rootWWW."admin/tab/".$TAB_Links."/?setS_id_".$TAB_Element."=".$v['id'] ?>">Link</a></td>
        <td>
        <?php
        $R = dbAction::_loop(array(
            'relations'=>FALSE,
            'tab'=>$TAB_Links,'where'=>"WHERE id_element='".$v['id']."'"));
        if($R){
            echo count($R)."";
        }else echo "0";
        ?>
        </td>
        </tr>


        </table>
    </td>

    <td style="text-align: center;"><?php echo $v['weight']; ?> </td>
    <td style="text-align: center;"><?php echo strtoupper($v['lang']); ?> </td>

    <td width="33%">
    <b><?php echo $v['h1']; ?> </b>
    <span style="font-size:10px;">
        <br><i>view:</i> <?php if(empty($v['view']))echo 'default'; else echo $v['view']; ?>

    <?php
    if(!empty($v['REL'][$TAB_Category])){
        echo '<br><i>';
        if(count($v['REL'][$TAB_Category])>1){echo "categorie: ";}else{echo "categoria: ";}
        echo '</i>';
        foreach ($v['REL'][$TAB_Category] AS $I)
        {
            echo ' '.$I['h1'].' ';
        }
    }
    ?>
    </span>
    </td>

    <td width="33%">
    <?php if(!empty($v['h2']))echo "<br>".$v['h2']; ?>
    </td>
    <td width="33%">
    <?php if(!empty($v['abstract']))echo "<div style='color:grey;font-size:9px;padding:3px;'>abstract:<br>".substr(strip_tags($v['abstract']),0,30)."</div>"; ?>
    <?php if(!empty($v['text']))echo "<div style='font-size:10px;padding:3px;'>text:<br>".substr(strip_tags($v['text']),0,90)."</div>"; ?>
    </td>


    <td><a class="upd" href="<?php echo $url_mod; ?>" ><?php echo __('modifica');?></a></td>

    <td>
        <?php if($v['slug']=="home"){ ?>
        <a class="del-disabled" href="#nodelete" onclick="return alert('<?php echo __('Elemento non cancellabile');?>');"><?php echo __('elimina');?></a>
        <?php }else{?>
        <a class="del" href="<?php echo $url_del; ?>" onclick="return confirm('<?php echo __('Confermi Eliminazione?',false);?>');"><?php echo __('elimina');?></a>
        <?php }?>
    </td>


    </tr>
    <?php } ?>

</table>



<?php include(_CORE::addView("admin/_elements/pagination")); ?>


<?php } else { ?>
<div class="norecordfound">
  nessun record trovato
</div>
<?php } ?>

<?php if (count($LOOP) < 3 ) {?>
<form method="post">
  <input type="hidden" name="copy_tab_records" value="<?php echo $a['USE_TAB']; ?>">
Copia da
<input type="text" name="lang">
<input type="submit" name="go">
</form>
<?php } ?>
