<?php
$fView = isset($featureView[$TAB_Element])?$featureView[$TAB_Element]:FALSE;

if($record['slug'] == "home")
    $fView = isset($featureView['home'])?$featureView['home']:FALSE;
?>
<div id="Featureds">
<?php if(isset($fView)){?>

    <?php //pr($fView);?>

    <?php foreach ($fView AS $k=>$v){?>
    <div class="<?php echo $k; ?> container" style="<?php if($record['view']!=$k){?>display: none;<?php } ?>">


        <?php foreach ($v['featureds'] AS $fType=>$fStyle){?>
          <?php
            $fTypeId = dbAction::_record(array(
              'relations'=>FALSE,
              'tab'=>$TAB_Featuredstype,'value'=>$fType,'field'=>'slug'));


              $loopF = FALSE;

                  $W = "WHERE id_featuredstype='" . $fTypeId['id'] . "' AND id_element='".$record['id']."'";
                  if($record['slug']=='home')
                      $W = "WHERE id_category is NULL AND id_element is NULL AND id_featuredstype='" . $fTypeId['id'] . "'";
              $loopF = dbAction::_loop(array(
                  'relations'=>FALSE,
                  'tab'=>$TAB_Featured,'where'=>$W));

                  $displayFeatureForm = true;
                  if( $featureViewSetting[$TAB_Element][$fType]['only_one_record'] && $loopF ){
                    $displayFeatureForm = false;
                  }
          ?>

        <div class="<?php echo $fType;?>" id="form_<?php echo $k;?>_<?php echo $fType;?>">
            <a name="_<?php echo $k;?>_<?php echo $fType;?>"></a>
                <?php //$fO->textEditor('fText', $record,"Testo","995px","210px",TRUE); ?>


            <?php if( $displayFeatureForm ){ ?>
            <div style="position:relative;  background-color: #ffffcc;padding:9px 15px 5px 15px;border-radius: 9px;border:1px solid #afafaf;width:964px;margin:0 0 15px 0;">

                <h2 style="padding:5px 10px 5px 5px;margin:0 0 10px 0;border-bottom:1px solid #afafaf;" title="<?php echo $fType;?>">
                    <?php echo $fStyle[0];?>
                    <span style="color:#ffffcc;"> - <?php echo $fType;?></span>
                </h2>

                    <?php

                    if(!$fTypeId)
                    {
                        echo'<span style="color:red">Errore relazione Tipo feature: "'.$fType.'" non trovata nel database. Contattare un amministratore di sistema.</span><br>';
                    }
                    else
                    {
                        echo $fO->hidden("f_id_featuredstype",$fTypeId['id']);


                    echo $fO->hidden('f_lang', key($_SESSION['Language']));
                    echo $fO->hidden("f_visibile","1");


                    echo $fO->hidden("f_id_element_for_redirect",$record['id']);

                    if($record['slug']!='home')
                        echo $fO->hidden("f_id_element",$record['id']);

                    ?>
                    <?php
                      /*
                        If is a "only_first_field" type dont show oter value
                        change the label
                       */
                       $h1Label = "h1";
                       $addStyleToform="";
                      if(isset($featureViewSetting[$TAB_Element][$fType]['only_first_field'])){
                        $h1Label = $featureViewSetting[$TAB_Element][$fType]['only_first_field'];
                        $addStyleToform="height:1px;overflow:hidden;";
                       }
                    ?>
                    <?php echo $fO->text('f_h1', NULL,$h1Label,'width:382px;margin:0 0 3px 0;'); ?>

                    <?php
                      /*
                      If is a "only_first_field" type dont show oter value
                      */
                      //if(!isset($featureViewSetting[$TAB_Element][$fType]['only_first_field'])){
                      ?>

                    <?php echo $fO->text('f_h2', NULL,"Sottotitolo",'width:360px;margin:0 0 3px 3px;'); ?>
                    <div style="clear:both;"></div>



                    <div style="<?php echo $addStyleToform;?>">
                    <?php echo $fO->text('f_text', NULL,"Testo",'width:382px;margin:0 3px 3px 0;'); ?>
                    <?php //echo $fO->range("f_weight",$record,'-100','100','1',"Weight","width:370px;padding:5px;margin:0px;",TRUE); ?>
                    <li class="editor" style="margin:18px 0 0 0;padding:0px;height:25px;width:360px" id="f_img_content_<?php echo $k; ?>_<?php echo $fType; ?>">
                        <input type="file" name="_f_img_" id="f_img_<?php echo $k; ?>_<?php echo $fType; ?>">
                    </li>


                    <script>
                    var setRedirect=function(val,type){
                        if(val>0){
                            $("#redirect").val("url=" + type + ":" + val);
                        }else{
                            $("#redirect").val("");
                        }
                    }
                    </script>
                    <?php
                    $redirect_category=NULL;
                    $redirect_element=NULL;

                    if(substr($record['redirect'],0,4)=="url=")
                    {
                        $urlR = substr($record['redirect'],4);
                        $urlR = explode(":", $urlR);
                        if($urlR[0]=="category"){
                            $redirect_category=$urlR[1];
                        }else if($urlR[0]=="element"){
                            $redirect_element=$urlR[1];
                        }
                    }
                    ?>


                    <div style="clear:both;"></div>
                    <li onMouseOver="toolTip('Crea il link da un Element')" onMouseOut="toolTip()" class="">
                        <label class="aSide">Redirect da Element</label>
                        <select data-name="<?php echo $k; ?>" data-type="<?php echo $fType; ?>" class="id_element_redirect" name="id_element_redirect" style="margin:0 3px 0 0;width:382px;" onChange="setRedirect(this.options[this.selectedIndex].value,'element');">
                        <?php $EleList = dbAction::_loop(array(
                            'relations'=>FALSE,
                            'tab'=>$TAB_Element,"where"=>"WHERE lang ='".key($_SESSION['Language'])."' AND slug!='home'",
                            'orderby'=>'ORDER BY view,h1'
                            ));

                        ?>
                            <option value="">Seleziona</option>
                        <?php foreach ($EleList AS $el){ ?>
                            <option value="<?php echo $el['id'];?>">
                                <?php echo $el['h1'];?>
                            </option>
                        <?php } ?>
                        </select>
                    </li>

                    <li onMouseOver="toolTip('Crea il link da una Category')" onMouseOut="toolTip()" class="">

                        <label class="aSide">Redirect da Category</label>
                        <select name="id_category_redirect" style="width:360px;margin-right:3px; " onChange="setRedirect(this.options[this.selectedIndex].value,'category');">
                        <?php $CatList = dbAction::_loop(array(
                            'relations'=>FALSE,
                            'tab'=>$TAB_Category,"where"=>"WHERE lang ='".key($_SESSION['Language'])."'",
                            'orderby'=>'ORDER BY view,h1'
                            ));

                        ?>
                            <option value="">Seleziona</option>
                        <?php foreach ($CatList AS $cat){ ?>
                            <option value="<?php echo $cat['id'];?>">
                                <?php echo $cat['h1'];?>
                            </option>
                        <?php } ?>
                        </select>
                    </li>

                    <?php echo $fO->text('redirect', $record,'redirect',' id= '); ?>
                    <?php //}//if(!isset($featureViewSetting[$TAB_Element][$fType]['only_first_field'])){  CLOSING ?>
                      <div style="clear:both;"></div>
                    </div>


                <div style="clear:both;"></div>
                <br>

                <input style="position:absolute;top:45px;right:25px;" type="button" value="Aggiungi" class="save addFeatured" data-name="<?php echo $k; ?>" data-type="<?php echo $fType; ?>"  />

                <div style="position:absolute;top:51px;right:55px;display:none;" class="loading_icon _loading_<?php echo $k; ?>" id="loadingicon_<?php echo $k;?>_<?php echo $fType;?>"></div>

                <?php } ?>
            </div>
            <?php } ?>

            <?php if( $featureViewSetting[$TAB_Element][$fType]['only_one_record'] && $loopF && !$displayFeatureForm ){ ?>
              <div style="position:relative;  background-color: #ffffcc;padding:9px 15px 5px 15px;border-radius: 9px;border:1px solid #afafaf;width:964px;margin:0 0 15px 0;">
                <h2 style="padding:5px 10px 5px 5px;margin:0 0 10px 0;border-bottom:1px solid #afafaf;" title="<?php echo $fType;?>">
                    <?php echo $fStyle[0];?>
                    <span style="color:#ffffcc;"> - <?php echo $fType;?></span>
                </h2>
                Questa voce accetta solo un record <?php echo count($loopF);?>
              </div>
            <?php } ?>


        <div class="result" id="show_result_<?php echo $k;?>_<?php echo $fType;?>"></div>


            <?php if($loopF){?>
                <?php foreach ($loopF AS $f){?>
                <div id="item_result_<?php echo $k;?>_<?php echo $fType;?>_<?php echo $f['id']?>" style="position:relative;  background-color: #ffffcc;padding:9px 15px 5px 15px;border-radius: 9px;border:1px solid #afafaf;width:964px;margin:0 0 15px 0;">

                    <?php echo $f['weight']?>)

                    <?php if($f['IMG']['s']) { ?><img src="<?php echo $f['IMG']['s']['url']; ?>" width="30" style="float:left;"><?php } ?>

                    <a class="MM_showAll" style="margin:0 0 0 5px" target="_blank" href="<?php echo rootWWW."admin/tab/".$TAB_Featured."/upd/".$f['id']."/"?>">
                        <?php echo $f['h1']?>
                    </a>

                      <?php echo $f['h2']?>

                    <span style="display:inline-block;padding:0 5px;">
                      <?php echo substr(strip_tags($f['text']),0,90)?>
                    </span>

                    <?php //echo pr($f)?>
                    <a class="del" style="padding:3px;width:10px;position: absolute;top:1px; right: 2px;" href="javascript:deleteFeature('<?php echo $f['id']; ?>','<?php echo $record['id']; ?>','<?php echo $k;?>','<?php echo $fType;?>');" onclick="return confirm('Cancellare?')">X</a>

                    <div style="clear:both"></div>
                </div>
                <?php } ?>
            <?php } ?>
            </div>
        <?php } ?>
    </div>
    <?php } ?>
<?php } ?>


</div>
