<?php $generaForm=new generaForm(); ?>
<?php echo $generaForm->_formTag($ACTION_FORM,$ACTION_FORM,$ACTION_FORM); ?>


<input type="hidden" name="id" value="<?php echo $record['id']; ?>">
<?php //pr($record);?>

<?php $fO=New fieldsObj(); ?>


<?php if (ElementChilds===TRUE) { ?>
<div class="section">
    <b class="tit">Elemento padre</b>



    <?php if(!empty($record['id_parent'])){?>
    <?php
        $parent = dbAction::_record(array(
            'relations'=>FALSE,
            'tab'=>$TAB_Element,'value'=>$record['id_parent']));
    ?>
        <div style="display: inline-block;">
            Questo elemento &egrave; una variante dell'elemento:
        </div>
        <div style="display: inline-block;">
            <b><a class="back" style="width: 210px;" href="<?php
            echo rootWWW."admin/tab/".$TAB_Element."/upd/".$parent['id']."/" ?>"><?php echo $parent['h1'];?></a></b>
        </div>
    <?php } ?>
            <div style="clear:both"></div>
    <div>
    <?php $childs=dbAction::_loop(array(
        'relations'=>FALSE,
        'tab'=>$TAB_Element,'where'=>"WHERE id_parent='".$record['id']."'",'orderby'=>"ORDER BY weight ASC"));?>
            <?php if($childs){?>
            <div style="margin:15px 0 5px 0;">
                <b>Elenco Elementi Figli (Varianti dell'elemento)</b>
                <table>
                    <tr style="background-color:#afafaf;color:#ffffff;">
                        <td style="font-size:11px;">H1</td>
                        <?php if (EasyEcommerce===TRUE) { ?>
                        <td style="font-size:11px;">Costo</td>
                        <td style="font-size:11px;">Qta</td>
                        <?php } ?>
                        <td></td>
                    </tr>

                    <?php foreach ($childs AS $child){?>
                    <tr >
                        <td style="font-size:11px;border-bottom: 1px solid;" width="100%"><?php echo $child['h1'];?></td>
                        <?php if (EasyEcommerce===TRUE) { ?>
                        <td align="center" style="font-size:11px;border-bottom: 1px solid;"><?php echo $child['price'];?></td>
                        <td align="center" style="font-size:11px;border-bottom: 1px solid;"><?php echo $child['qty'];?></td>
                        <?php } ?>
                        <td><a class="new" href="<?php echo rootWWW."admin/tab/".$TAB_Element."/upd/".$child['id']."/";?>">modifica</a></td>


                    </tr>
                    <?php } ?>
                </table>
            </div>
            <?php } else { ?>

                <?php
                $note="<img src='".$a['TEMPLATE_DIR_WWW']."_img/alert_body_msg.png' width='30' style='float:left;margin:3px 3px 0 0;'>Attenzione,<br>se si specifica un <b>Elemento Padre</b> questo elemento non verrà visualizzato<br>ne nelle liste ne nella pagina, ma solo dentro alla scheda dell'elemento padre.";
                echo $fO->selFromTab('id_parent', $record,array(
                    'tab'=>$TAB_Element,
                    'where' =>"WHERE (id_parent IS NULL OR id_parent='0') AND id!='".$record['id']."'",
                    'orderby'=>"ORDER BY weight, h1",
                    '_INPUT_SELECT_'=>array('id'=>'id','name'=>'h1'),
                    ),"Elemento Padre",NULL,$note); ?>

            <?php } ?>

    </div>

    <div style="clear:both"></div>
</div>
<?php } ?>


<?php if (EasyEcommerce===TRUE) { ?>

<div class="section">


    <?php if(!@$EasyEcommerceHideTitle){ ?>
        <b class="tit">Dati per l'e-commerce</b>
    <?php } ?>

    <?php
    if(in_array("stockid", $EasyEcommerceValue))
        echo $fO->text('stockid', $record,'Codice Prodotto',"width:450px");
    if(in_array("price", $EasyEcommerceValue))
        echo $fO->number('price', $record,'Costo',"width:150px",TRUE,TRUE);
    if(in_array("qty", $EasyEcommerceValue))
        echo $fO->number('qty', $record,'Quantita',"width:150px");
    if(in_array("grams", $EasyEcommerceValue))
        echo $fO->number('grams', $record,'Grammi',"width:150px");
    ?>


    <div style="clear:both"></div>
</div>
<?php } ?>




<div id="UPD" class="section Element">
    <b class='tit'>&nbsp; Dati elemento</b>




    <div class="sx">



        <div id="clear"></div>










    <ul>

    <?php $disabled=NULL;if($record['slug']=="home"){$disabled="disabled";}?>
    <?php echo $fO->siNo('isaccessible', $record,"Visibile",NULL,$disabled); ?>



    <?php
    if(!isset($_SESSION['Language']) || empty($_SESSION['Language']))
    {
        echo $fO->hidden('lang', key($language));
    }
    else
    {
    echo $fO->hidden('lang', key($_SESSION['Language']));
    }
    ?>



        <?php echo $fO->selView('view', $record,"Tipo di vista"); ?>





    <?php echo $fO->text('title', $record,NULL,NULL,NULL,"green"); ?>
    <?php echo $fO->text('description', $record,NULL,NULL,NULL,"green"); ?>
    <?php echo $fO->text('keywords', $record,NULL,NULL,NULL,"green"); ?>










    </ul>
    </div>

    <div class="dx">
        <ul>

            <?php echo $fO->text('h1', $record); ?>
            <?php echo $fO->text('h2', $record); ?>
            <?php echo $fO->text('abstract', $record); ?>


            <?php $readonly=NULL;if($record['slug']=="home"){$readonly="readonly";}?>
            <?php echo $fO->text('slug', $record,NULL,NULL,NULL,"green",$readonly); ?>



            <?php if (inHome===TRUE) { ?>
                <?php echo $fO->siNo('inhome', $record,"In Home"); ?>
            <?php } ?>
        </ul>
        <div class="dx1">
        <ul>






        </ul>
        </div>

        <div class="dx2">
        <ul>



        </ul>
        </div>

        <div id="clear"></div>

    <ul>





        <div style="clear:both;"></div>


        <!--
        <li class="editor" style="padding:9px;">
            <ul style="width:470px">
                <li>aaa</li>
            </ul>
        </li>
        -->


    </ul>

        <div class="dx1">
        <ul>
        <li>
        </li>

        </ul>
        </div>

        <div class="dx2">
        <ul>

        <li>
       </li>

        </ul>
        </div>

        <div id="clear"></div>




    </div>

    <div id="clear"></div>




    <br>


    <?php if(ExtraFields===TRUE && function_exists("getExtraFieldsType") && is_array(getExtraFieldsType())){ ?>

    <b class='tit'>&nbsp; Dati Extra</b>
    <div class="sx">

        <ul>
            <?php foreach (getExtraFieldsType() AS $extraSlug){?>
            <?php $recordExtra = dbAction::_record(array(
                'relations'=>FALSE,
                'tab'=>$TAB_Extrafields,'set_w'=>"WHERE id_element='".$record['id']."' AND slug='".$extraSlug."'",'relations'=>FALSE)); ?>
            <?php echo $fO->extraFields($extraSlug, $recordExtra); ?>
            <?php } ?>
        </ul>


    </div>

    <div id="clear"></div>
    <br>
    <?php }?>



</div>



    <input type="submit" value="salva dati" class="save" />
    <?php
    if($simpleEditor[$TAB_Element][$record['view']]){
        ?>
        <script language="javascript" type="text/javascript" src="<?php echo rootWWW;?>js/editarea_0_8_2/edit_area/edit_area_full.js"></script>
        <script language="javascript" type="text/javascript">
        editAreaLoader.init({
        	id : "textEditor"		// textarea id
        	,syntax: "css"			// syntax to be uses for highgliting
          ,word_wrap:true
          ,toolbar: "search, go_to_line, fullscreen, |, undo, redo, |, select_font,|, change_smooth_selection, highlight, reset_highlight, word_wrap, |, help"
          ,save_callback:"document.getElementById('UPD').submit();"
        	,start_highlight: true		// to display with highlight mode on start-up
        });
        </script>

        <!--
        <script src="<?php echo rootWWW;?>js/codemirror-5.12/lib/codemirror.js"></script>
        <link rel="stylesheet" href="<?php echo rootWWW;?>js/codemirror-5.12/lib/codemirror.css">
        <script src="<?php echo rootWWW;?>js/codemirror-5.12/mode/javascript/javascript.js"></script>
        <script>
        var textArea = document.getElementById("textEditor");
        var editor = CodeMirror.fromTextArea(textArea, {
            lineNumbers: true
        }).setValue("your code here");

        </script>
        -->
        <?php
        //error_reporting(-1);
        //ini_set('display_errors', 'On');
        $tidy=$record['text'];
        /**/
        $config = array(
           'indent'         => true,
           'output-xhtml'   => false,
           'show-body-only' => true,
           'wrap'           => 200);

        $tidy = new tidy;
        $tidy->parseString($record['text'], $config, 'utf8');
        $tidy->cleanRepair();
        /**/



        echo '<div><textarea id="textEditor" name="text" style="width:995px;height:350px;">'.$tidy.'</textarea></div>';
    }else{
      $fO->textEditor('text', $record,"Testo","995px","210px", TRUE, @$Activate_Simple_CK_Editor);
    }
    ?>
    <br>

    <?php include(_CORE::addView("admin/Element/featureds")); ?>

    <br>

    <div>
    <?php echo $fO->range("weight",$record,'-100','100','1',"Weight","width:965px;padding:15px;",TRUE); ?>
    </div>

    <li class="editor images" style="width:476px;padding:6px;">
        <?php include(_CORE::addView("admin/_elements/form_db_files"));?>
    </li>


    <script type="text/javascript">
        var MM_showAll = function(name){
            $('#' + name + ' input[type=checkbox]').each(function(){
                var ID=$(this).attr('id');
                var Label = $("label[for='"+ID+"']");
                $("#"+ID).show();
                $("#"+ID).nextAll('br').show();
                $(Label).show();
                $('#' + name + " a.MM_showAll").hide();
                $('#' + name + " a.MM_hideAll").show();
            });
        };
        var MM_hideAll = function(name){
                //$('input[name=' + name + ']');
                $('#' + name + ' input[type=checkbox]').each(function(){
                    var ID=$(this).attr('id');
                    var Label = $("label[for='"+ID+"']");

                    if(!$("#"+ID).is(':checked')){
                        $("#"+ID).hide();
                        $("#"+ID).nextAll('br').hide();
                        $(Label).hide();
                    }
                });

                $('#' + name + " a.MM_showAll").show();
                $('#' + name + " a.MM_hideAll").hide();
        }

            $(document).ready(function(){
                MM_hideAll("listCategory");
                MM_hideAll("listTags");
            });



    </script>


        <?php
        $selected_value=array();

        if(isset($a['FILTER_SET'][$TAB_Element][$TAB_Category]['value']))
            $selected_value[] = $a['FILTER_SET'][$TAB_Element][$TAB_Category]['value'];

        $catsIds=dbAction::_loop(array(
            'relations'=>FALSE,
            'tab'=>$TAB_ElementCategory,
            'where'=>' WHERE id_element ="'.$record['id'].'"',
            //'echo'=>true
        ));
        if($catsIds){
            foreach ($catsIds AS $catsId){
                $selected_value[]=$catsId['id_category'];
            }
        }



        if(empty($record['lang']))
        {
            if(empty($_SESSION['Language']))
                $record['lang']="it";
            else
                $record['lang']=key($_SESSION['Language']);
        }
        //pr($record);
        //pr($selected_value);
        ?>
        <script>

        </script>
        <li id="listCategory" onMouseOver="toolTip('Category')"
            onMouseOut="toolTip()" class="editor manytomany" style="width:214px;padding:10px;margin-bottom: 5px;" >
            <b>Categorie</b>
            <br>
            <!-- <select name="id_category" style="width:490px;" class="onp">-->
            <?php echo FILTER::write($a,array(
                'selected_value'=>$selected_value,
                'name_f'=>'id_category[]',
                'tab'=>$TAB_Category,
                'type'=>'checkbox',
                'where'=>"WHERE lang='".$record['lang']."'",
                'field_anchor'=>array("[","#REL#".$TAB_Area."#h1","] ","#h1",' (',"#lang",')'),'allow_zero'=>'nessuna',
                    'orderby'=>'ORDER BY id_area, h1'));
                ?>
            <!-- </select> -->

            <a class="MM_showAll" href="javascript:MM_showAll('listCategory');"><span>+</span> Mostra tutto</a>
            <a class="MM_hideAll" href="javascript:MM_hideAll('listCategory');"><span>-</span> Nascondi </a>
        </li>



        <?php
        $selected_value=array();
        $tagIds=dbAction::_loop(array(
            'relations'=>FALSE,
            'tab'=>$TAB_ElementTags,
            'where'=>' WHERE id_element ="'.$record['id'].'"',
            //'echo'=>true
        ));
        if($tagIds){
            foreach ($tagIds AS $tagId){
                $selected_value[]=$tagId['id_tags'];
            }
        }
        //pr($record);
        //pr($selected_value);
        ?>
        <li id="listTags" onMouseOver="toolTip('Tags')"
            onMouseOut="toolTip()" class="editor manytomany" style="width:214px;padding:10px;margin-bottom: 5px;" >
            <b>Tags</b>
            <br>
            <!-- <select name="id_category" style="width:490px;" class="onp">-->
            <?php echo FILTER::write($a,array(
                'selected_value'=>$selected_value,
                'name_f'=>'id_tags[]',
                'tab'=>$TAB_Tags,'type'=>'checkbox','field_anchor'=>array("[","#REL#".$TAB_Area."#h1","] ","[","#REL#".$TAB_Category."#h1","] ","#text",' (',"#lang",')'),'allow_zero'=>'nessuna','orderby'=>'ORDER BY text')); ?>
            <!-- </select> -->

            <a class="MM_showAll" href="javascript:MM_showAll('listTags');"><span>+</span> Mostra tutto</a>
            <a class="MM_hideAll" href="javascript:MM_hideAll('listTags');"><span>-</span> Nascondi </a>
        </li>





<input type="submit" value="salva dati" class="save long" />
</form>
