<?php include(_CORE::addView("admin/a_utenti/filtri")); ?>

<div id="pagination">
<a class="back" href="<?php echo $a['LINK_BACK']; ?>">torna all'elenco</a>

    <div id="clear"></div>

</div>
<?php
if($a['_files_troppo_pesanti_']){
  echo '<div style="padding:10px;border:3px solid red;font-size:15px;color:red;">I file superano lo spazio consentito per il download diretto, verranno scaricati solo i file di configurazione</div>';
}


if($a['fileZipExists']){
  echo 'Esiste già un file per il download, prima di poterne creare un\'altro è necessario cancellarlo.';
}else{
  echo 'Download pronto:';
}
?>

<div style="padding:10px;border:1px solid #000000;">
<?php
$files = scandir($rootDOC . '_files/');
//pr($files);

foreach($files as $file) {
  $needle = ".gz";
  $length = strlen($needle);
  if( !is_dir($file) && (substr($file, -$length) === $needle) ){
    echo 'Download: <a href="' . $rootWWW . '_files/' . $file . '" target="_blank">';
    echo $file;
    echo '</a>';
  }
}
?>
</div>
