<?php //pr($a);?>
<script>
    function deleteFeature(id,id_category,featName,featType){
        var url = '<?php echo rootWWW."admin/tab/" . $TAB_Featured . "/del/"; ?>' + id + "/";
        console.log(id);
        
        $.get(url)
                .done(function( data ) {
                    console.log(data);
                    $("#item_result_" + featName + "_" + featType + "_" + id).fadeOut();
                    
                    //window.location.href = "<?php echo rootWWW."admin/tab/" . $TAB_Category . "/upd/";?>" + id_category + "/<?php echo "?".time();?>#_" + featName + "_" + featType;
                })
                .fail(function( err ) {
                  console.log(err);
                })
            ;
        
    }
    function showFileInput(featName,featType){
        $("#f_img_content_" + featName + "_" + featType + " .img").remove();
        $("#f_img_" + featName + "_" + featType).show();
    }
    function getRemoteImage(url,featName,featType){
            
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'blob';
            xhr.onload = function(e) {
              if (this.status == 200) {
                var myBlob = this.response;
                console.log(myBlob);
                
                if(featName && featType){
                    var urlImgMini = url.replace("/original/", "/s/");
                    $("#f_img_" + featName + "_" + featType).hide();
                    $("#f_img_content_" + featName + "_" + featType).append(
                            '<span class="img">'
                          + '<input type="hidden" id="url_file_'+featName+'_'+featType+'" value="'+url+'"> '
                          + '<img src="' + urlImgMini + '" width="25" height="25" style="float:left;"> '
                          + '<div style="float:left;padding:4px 0 0 5px;">'
                          + ' File selezionato <a class="del" style="padding:3px;width:auto;display:inline-block;" '
                          + ' href="javascript:showFileInput(\''+featName+'\',\''+featType+'\');"> '
                          + ' annulla e carica nuovo</a> '
                          + ' <div style="clear:both;"></div>'
                          + '</div>'
                          + ' </span>'
                    );
                }
                
                return myBlob;
              }
            };
            xhr.send();
        };
        
$( document ).ready(function() {
        
        
    $(".id_category_redirect").change(function(){
        var featName = $(this).attr("data-name");
        var featType = $(this).attr("data-type");
        
        var idSelector = "#Featureds ." + featName ; 
        var idFormDiv = "#form" + "_" + featName + "_" + featType;
        
        var obj_h1 = $(idFormDiv + " [name='f_h1']");
        var obj_h2 = $(idFormDiv + " [name='f_h2']");
        var obj_text = $(idFormDiv + " [name='f_text']");
        
        
        
        
        
        var category_id = $(this).val();
        var url = '<?php echo rootWWW."admin/tab/" . $TAB_Category . "/upd/";?>' + category_id + '/?json';
        console.log("Remote IMAGE URL: " + url);
        $.get(url)
                .done(function( data ) {
                    console.log(data);
            
                    $("#f_img_content_" + featName + "_" + featType + " .img").remove();
                    $("#f_img_" + featName + "_" + featType).show();
                    
                    getRemoteImage('<?php echo rootWWW."_files/img/category/original/";?>' + data.id + '_' + data.slug + '.png',featName,featType);
            
                    if(!obj_h1.val()) obj_h1.val(data.h1);
                    if(!obj_h2.val()) obj_h2.val(data.h2);
                    if(!obj_text.val()){ 
                        //if(data.abstract.length)
                        obj_text.val(data.abstract);
                    }
            
                })
                .fail(function( err ) {
                  console.log(err);
                })
            ;
    });    
        
    $(".addFeatured").click(function(){
        
        //Add featured 
        //alert("featName");
        var featName = $(this).attr("data-name");
        var featType = $(this).attr("data-type");
        //alert(featName + " - " + featType);
        
        var idSelector = "#Featureds ." + featName ; 
        var idFormDiv = "#form" + "_" + featName + "_" + featType;
        var idLoadingIcon = "#loadingicon" + "_" + featName + "_" + featType;
        var idShowResult = "#show_result" + "_" + featName + "_" + featType;
        
        $(this).hide();
        $(idLoadingIcon).show();
        
        
        var $feat = $();
        var h1 = $(idFormDiv + " [name='f_h1']").val();
        var h2 = $(idFormDiv + " [name='f_h2']").val();
        var text = $(idFormDiv + " [name='f_text']").val();
        
        var lang = $(idFormDiv + " [name='f_lang']").val(); 
        var id_featuredstype = $(idFormDiv + " [name='f_id_featuredstype']").val(); 
        var id_category = $(idFormDiv + " [name='f_id_category']").val(); 
        var id_category_for_redirect = $(idFormDiv + " [name='f_id_category_for_redirect']").val(); 
        
        
        var redirect = $(idFormDiv + " [name='redirect']").val(); 
        var visibility = "1"; 
        
        var url = "<?php echo rootWWW."admin/tab/" . $TAB_Featured . "/new/1/";?>";
        var idImgInput = "f_img" + "_" + featName + "_" + featType;
        var sendForm = function(url, dati, file){
            
            console.log("Do the post");
            
            if(file)
                console.log("With FILE");
            else
                console.log("With NO FILE!");
            
            console.log(dati);
            
            $.post( url, dati)
                .done(function( data ) {
                  //$( idShowResult ).html( "ok: " + data );
                  window.location.href = "<?php echo rootWWW."admin/tab/" . $TAB_Category . "/upd/";?>" + dati.id_category_for_redirect + "/<?php echo "?".time();?>#_" + featName + "_" + featType;
                })
                .fail(function( err ) {
                  $( idShowResult ).html( "err: " + err );
                })
            ;
        };
        
        
        
        
        
        
        var selFileRemote = function(url, dati, file_remote){
            
            
            
            //var blob = getRemoteImage(file_remote); 
            
            //console.log("BLOB");
            //console.log(blob);
            //console.log("- - -");
            
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function(){
                var canvas = document.createElement('CANVAS'),
                ctx = canvas.getContext('2d'), dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL('image/png');
                //callback(dataURL);
                
                console.log("STAMPO DATI");
                console.log(dataURL);
                
                var cutThis = "data:image/png;base64,";
                var base64 = dataURL.replace(cutThis, "");
                console.log(base64);
                
                var f = {
                    name: "temp.png",
                    type: "image/png",
                    size: img.fileSize,
                    base64: base64,
                    _FILES_NAME_: 'img' //This is the name of the $_FILES[] php array
                };
                
                dati.base64 = f;
                
                console.log("DATI!!!");
                console.log(dati);
                console.log(img);
                
                canvas = null; 
                
                sendForm(url, dati, true);
                
            };
            img.src = file_remote;
            
        };
        
        var selFile = function(file, url, dati){
            var reader = new FileReader();
            reader.onload = function (readerEvt) {
                console.log("Carico img");
                //console.log(dati);
                
                var binaryString = readerEvt.target.result;
                var base64 = btoa(binaryString);
                //var rawData = reader.result;
                
                var f = {
                    name: file.name,
                    type: file.type,
                    size: file.size,
                    base64: base64,
                    _FILES_NAME_: 'img' //This is the name of the $_FILES[] php array
                };
                
                //dati.img = f.base64;
                //$(".result").append('<input type="text" id="__binary__" value="' + f.base64 + '">');
                console.log("Appendo! ");
                console.log(f.base64);
                //dati.base64 = f.base64;
                dati.base64 = f;
                
                console.log(dati);
                console.log(img);
                
                sendForm(url, dati, true);
                //console.log(dati);
                //return f.base64;
                        
            };
            reader.readAsBinaryString(file);
            //reader.readAsDataURL(file);
            
        }
        
        var img;
        var dati = { 
            NEW: '1', 
            h1:h1, 
            h2:h2, 
            text:text, 
            id_category:id_category, 
            id_category_for_redirect:id_category_for_redirect,
            visibility:visibility, 
            id_featuredstype:id_featuredstype, 
            lang:lang,
            redirect:redirect
        };
        console.log("NO img");
        console.log(dati);
        //var dati = dati_1;
        //var dati = $.extend({}, dati_1);
        
        var file = document.getElementById(idImgInput).files[0]; 
        //var file_remote = getRemoteImage($("url_file_"+featName+"_"+featType).val(),featName,featType); 
        console.log("url_file_"+featName+"_"+featType);
        var file_remote = $("#url_file_"+featName+"_"+featType).val(); 
        
        
        console.log("CHECK IT:::");
        console.log(featName);
        console.log(featType);
        console.log(file_remote);
        
        
        if(file){
            console.log("1");
            selFile(file, url, dati);
        }else if(file_remote){
            console.log("2");
            selFileRemote(url, dati, file_remote);
        }else{
            console.log("3");
            sendForm(url, dati,false);
        }
        
    });    
    $("select[name='view']").on('change', function() {
        //alert( this.value ); // or $(this).val()
        $("#Featureds .container").hide();
    
        $("#Featureds ." + this.value).show();
    });
    
});

    
</script>

