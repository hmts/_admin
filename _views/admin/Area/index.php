<?php include(_CORE::addView("admin/Area/filtri")); ?>

<?php //pr($_SESSION['FILTERS']); ?>

<?php //pr($a['USE_TAB']); ?>
<?php $LOOP=$a['LOOP'][$a['USE_TAB']]; ?>

<?php include(_CORE::addView("admin/_elements/pagination")); ?>
<?php include(_CORE::addView("admin/_elements/go_to_filter_page")); ?>

<?php if($LOOP){ ?>


<table class="index" id="tableProdotti">

    
    <tr class="header">
        <td>Img</td>
        <td class="help" onmouseover="toolTip('Weight<br>Indica la posizione negli elenchi del record')" onmouseout="toolTip()">
            </td>
        <td class="help" onmouseover="toolTip('Lang<br>La lingua del record nella pagina')" onmouseout="toolTip()">
            Lang</td>
        <td class="help" onmouseover="toolTip('H1<br>Il Titolo del record nella pagina')" onmouseout="toolTip()">
            H1</td>
        <td class="help" onmouseover="toolTip('H1<br>Il Sottotitolo del record nella pagina')" onmouseout="toolTip()">
            H2</td>
        <td class="help" onmouseover="toolTip('Text<br>Il Testo del record nella pagina<br>in questa colonna se presente viene anche visualizzato l\'abstract')" onmouseout="toolTip()">
        
            text</td>
        <td colspan="2">&nbsp;</td>
    </tr>
    
    <?php foreach($LOOP as $k=>$v){ ?>
    
    <?php
    $url_mod=rootWWW."admin/tab/".$a['USE_TAB']."/upd/".$v['id']."/";
    $url_del=rootWWW."admin/tab/".$a['USE_TAB']."/del/".$v['id']."/";
    
    $addClass="isVis";
    if($v['isaccessible']=="0")
    {
        $addClass="notVis";
    }
    
    ?>
    
    
    <tr ondblclick="javascript:location='<?php echo $url_mod; ?>'" class="<?php echo $v['tr_class']." ".$addClass; ?>">    
    
    <td><?php if($v['IMG']['s']) { ?><img src="<?php echo $v['IMG']['s']['url']; ?>" width="30"><?php } ?></td>
    
    <td style="text-align: center;"><?php echo $v['weight']; ?> </td>
    <td style="text-align: center;"><?php echo strtoupper($v['lang']); ?> </td>
    
    <td width="33%">
    <?php echo $v['h1']; ?>
    </td> 
    <td width="33%">
    <?php if(!empty($v['h2']))echo "<br>".$v['h2']; ?>
    </td> 
    <td width="33%">
    <?php if(!empty($v['abstract']))echo "<div style='color:grey;font-size:9px;padding:3px;'>abstract:<br>".substr(strip_tags($v['abstract']),0,30)."</div>"; ?>
    <?php if(!empty($v['text']))echo "<div style='font-size:10px;padding:3px;'>text:<br>".substr(strip_tags($v['text']),0,90)."</div>"; ?>
    </td> 
    
    <td><a class="upd" href="<?php echo $url_mod; ?>" ><?php echo __('modifica');?></a></td>
    <td><a class="del" href="<?php echo $url_del; ?>" onclick="return confirm('<?php echo __('Confermi Eliminazione?',false);?>');"><?php echo __('elimina');?></a></td>
    
    
    </tr>
    <?php } ?>
        
</table>



<?php include(_CORE::addView("admin/_elements/pagination")); ?>


<?php } else { ?>
<div class="norecordfound">nessun record trovato</div>
<?php } ?>
