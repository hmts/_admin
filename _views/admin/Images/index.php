<?php include(_CORE::addView("admin/Images/filtri")); ?>

<?php //pr($_SESSION['FILTERS']); ?>

<?php //pr($a['USE_TAB']); ?>
<?php $LOOP=$a['LOOP'][$a['USE_TAB']]; ?>

<?php include(_CORE::addView("admin/_elements/pagination")); ?>
<?php include(_CORE::addView("admin/_elements/go_to_filter_page")); ?>


<?php 
$relationSelected = FALSE;
if(isset($_SESSION['FILTERS'][$a['USE_TAB']][$TAB_Element])){ 
    $relationSelected = $_SESSION['FILTERS'][$a['USE_TAB']][$TAB_Element];
}else if(isset($_SESSION['FILTERS'][$a['USE_TAB']][$TAB_Category])){
    $relationSelected = $_SESSION['FILTERS'][$a['USE_TAB']][$TAB_Category];
}
?>

<?php //if($relationSelected){?>
<?php if($relationSelected && ImagesBulkUpload === TRUE){?>
<script>
    function sBu()
        {
         $("#sendBu").hide();
         $("#loadingBu").show();
         //validation code to see State field is mandatory.  
        }
</script>
<div style="font-size:11px;padding:9px;background:#efefef;border-top:#afafaf 1 solid;">
<?php $generaForm=new generaForm(); ?>
<?php echo $generaForm->_formTag("bu","bu","bu"); ?>
    Invio di immagini multiplo<br>
<input type="hidden" name="bulkImages" value="1">
<input type="hidden" name="key" value="<?php echo $relationSelected['key'];?>">
<input type="hidden" name="value" value="<?php echo $relationSelected['value'];?>">
<input type="file" name="images[]" multiple="multiple"> 
<input type="submit" id="sendBu" onclick="sBu()">
<img src="<?php echo $a['TEMPLATE_DIR_WWW'];?>_img/icon-loading.gif" id="loadingBu" style="display:none;">
</form>
</div>

<?php //pr($a);?>
<?php } ?>

<?php if($LOOP){ ?>


<table class="index" id="tableProdotti">
    
    <tr class="header">
        <td>Img</td>
        <td class="help" onmouseover="toolTip('Weight<br>Indica la posizione negli elenchi del record')" onmouseout="toolTip()">
            </td>
        <td class="help" onmouseover="toolTip('LANG<br>La lingua del record nella pagina')" onmouseout="toolTip()">
            Lang</td>
        <td class="help" onmouseover="toolTip('H1<br>Il Titolo della feature nella pagina')" onmouseout="toolTip()">
            H1</td>
        <td class="help" onmouseover="toolTip('H2<br>Il Sottotitolo della feature nella pagina')" onmouseout="toolTip()">
            H2</td>
        <td class="help" onmouseover="toolTip('Text<br>Il Testo della feature nella pagina<br>in questa colonna se presente viene anche visualizzato l\'abstract')" onmouseout="toolTip()">
        
            text</td>
        <td colspan="2">&nbsp;</td>
    </tr>
    
    
    <?php foreach($LOOP as $k=>$v){ ?>
    
    <?php
    $url_mod=rootWWW."admin/tab/".$a['USE_TAB']."/upd/".$v['id']."/";
    $url_del=rootWWW."admin/tab/".$a['USE_TAB']."/del/".$v['id']."/";
    $addClass="isVis";
    if($v['visibility']=="0")
    {
        $addClass="notVis";
    }
    ?>
    
    
    <?php 
    $errorOnLine = FALSE;
    if(!empty($v['REL'][$TAB_Element]) && !empty($v['REL'][$TAB_Category]['h1'])){ 
        $errorOnLine = "<b style=color:red;>ATTENZIONE Errore su questa riga</b>:"
                     . "<br>Assegnazione doppia ad elemento: "
                     . "<br>".$v['REL'][$TAB_Element]['h1']
                     . " e a categoria: ".$v['REL'][$TAB_Category]['h1']
                     . "<br>Assegnare solo o una categoria o un elemento."
                     . "";
    } 
    ?>
    <tr ondblclick="javascript:location='<?php echo $url_mod; ?>'" class="<?php echo $v['tr_class']." ".$addClass; ?>"<?php
        if($errorOnLine){
            echo ' style="background:#ffe4e4;" ';
            echo ' onmouseover="toolTip(\''. $errorOnLine .'\')" onmouseout="toolTip()" ';
        }
    ?>>
        
        
    <td><?php if($v['IMG']['s']) { ?><img src="<?php echo $v['IMG']['s']['url']; ?>" width="30"><?php } ?></td>
    
    <td style="text-align: center;"><?php echo $v['weight']; ?> </td>
    <td style="text-align: center;"><?php echo strtoupper($v['lang']); ?> </td>
    
    
    <td width="33%">
        
        <?php echo $v['h1']; ?>
        
        <?php if(!empty($v['REL'][$TAB_Category]['h1'])){ ?>
            <br>
    <nobr>[<?php echo $v['REL'][$TAB_Category]['h1']; ?>] </nobr>
        <?php } ?>
    <td width="33%">
        <?php echo $v['h2']; ?>
    
    </td> 
    
    <td width="33%">
        <?php if(!empty($v['text']))echo "<div style='font-size:10px;padding:3px;'>text:<br>".substr(strip_tags($v['text']),0,90)."</div>"; ?>
    </td> 
    
    
    
    <td><a class="upd" href="<?php echo $url_mod; ?>" ><?php echo __('modifica');?></a></td>
    <td><a class="del" href="<?php echo $url_del; ?>" onclick="return confirm('<?php echo __('Confermi Eliminazione?',false);?>');"><?php echo __('elimina');?></a></td>
    
    
    </tr>
    <?php } ?>
        
</table>



<?php include(_CORE::addView("admin/_elements/pagination")); ?>


<?php } else { ?>
<div class="norecordfound">nessun record trovato</div>
<?php } ?>
