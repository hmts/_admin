<?php
$brandHome = rootDOC."_config/brand_home.html"; //echo $brandHome;
$brandHome_php = rootDOC."_config/brand_home.php"; //echo $brandHome;
if(file_exists($brandHome))
{
    $getBrandHome = str_replace("{{WWW}}",$a['TEMPLATE_DIR_WWW'],file_get_contents($brandHome) );
    echo $getBrandHome; 
}
else if(file_exists($brandHome_php))
{
    //$getBrandHome = str_replace("{{WWW}}",$a['TEMPLATE_DIR_WWW'],file_get_contents($brandHome) );
    //echo $getBrandHome; 
    include($brandHome_php);
}
else
{
?>
<div style="margin:30px 0 30px 0;">
    <div>
        <div style="display: inline-block;vertical-align:middle;text-align: center;margin:0 20px 0 0;">
            <img src="<?php echo $a['TEMPLATE_DIR_WWW'];?>/_img/numero-verde.png">
        </div>
        <div style="display: inline-block;vertical-align:middle;">
            <span style="color:#004586;font-size: 21px;">
                Per tutti i livelli di servizio  
                <br>
                il <strong>call center</strong>
                è a disposizione <br>
                dal lunedì al venerdì <br>
                dalle 10.00 alle 12.30<br>
                e dalle 15.00 alle 17.30
            </span>
        </div>
    </div>
    
    <div>
        <div style="display: inline-block;vertical-align:middle;text-align: center;margin:0 20px 0 0;">
            <img src="<?php echo $a['TEMPLATE_DIR_WWW'];?>/_img/helpdesk.png" style="max-width: 272px;">
        </div>
        <div style="display: inline-block;vertical-align:middle;">
            <span style="color:#004586;font-size: 21px;">
                Per segnalare problemi<br>
                direttamente al nostro<br>
                servizio tecnico, è possibile<br>
                utilizzare l'account dedicato<br>
                ed accedere al nostro <br>
                gestionale. <br><br>
                <a class="btn" target="_blank" href="http://213.203.136.246/_gestionale/login/index.php">Segnala un Problema</a>
                
            </span>
        </div>
    </div>
</div>
<?php 
}
?>