CKEDITOR.plugins.add('imgbrowser',
{
    init: function(editor)
    {
        var pluginName = 'imgbrowser';
        CKEDITOR.ui.addButton('Imgbrowser',
            {
                label: 'Image browser',
                command: pluginName,
                click: function (editor) { window.open('/publish/browser/index.php','Image Browser','width=900,height=600'); }
            });
    }
});
