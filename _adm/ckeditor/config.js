/*
Copyright (c) 2003-2009, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';


//config.extraPlugins = 'footnote';

config.filebrowserUploadUrl = '/_siti/yantrayoga.org/ckupload.php';


config.toolbar = 'MyToolbar';

    config.toolbar_MyToolbar =
    [
	['Source','-','Save','-','Templates'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	['Image'],
        '/',
        ['Styles','Format','Font','FontSize'],['TextColor','BGColor'],
        ['Bold','Italic','Underline','Strike'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Link','Unlink','Anchor','-','Table','-','HorizontalRule','-','SpecialChar']
    ];


    filebrowserBrowseUrl : '/browser/browse/type/all',
    filebrowserUploadUrl : '/browser/upload/type/all',
    filebrowserImageBrowseUrl : '/browser/browse/type/image',
    filebrowserImageUploadUrl : '/browser/upload/type/image',
    filebrowserWindowWidth  : 800,
    filebrowserWindowHeight : 500



CKEDITOR.scriptLoader.load( CKEDITOR.basePath + 'plugins/image_uploader/plugin.js' );

};
