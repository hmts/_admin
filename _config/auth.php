<?php
/*
This costant defined all the variables need to login function
*/

define("LOGIN_TAB_DEFAULT",'TAB_aUser');
define("LOGIN_ID_FORM_DEFAULT",'LOGIN_CHECK');

define("HOME_REQUIRE_LOGIN",false);
define("HOME_REQUIRE_LOGIN_SET","0");
define("HOME_REQUIRE_LOGIN_TAB",LOGIN_TAB_DEFAULT);

/*
define("LOGIN_ID_FROM",'LOGIN_CHECK');
define("LOGIN_TAB",'utenti');
define("LOGIN_TAB_SET",'a_utenti_cat');
define("LOGIN_USR_field",'email');
define("LOGIN_PSW_field",'password');
define("LOGIN_ID_SET_field",'id_'.LOGIN_TAB_SET.'');
define("LOGIN_AD_WHERE",'');
define("LOGIN_AD_REDIRECT_input_form",'redirect');
*/


$AUTH_A=array(
    '_aUser'=>array(
        'table'=>'TAB_aUser',
        'table_set'=>'TAB_aUserSet',
        'usr_field'=>'email',
        'psw_field'=>'password',
        'id_form'=>'', //Must be unic IF empty Is set FROM DEFAULT
        'add_where'=>'',
        'redirect_from'=>'',
        'msg_login_yes'=>'Login Yes',
        'msg_login_no'=>'Login No',
        'psw_crypt_function'=>'md5',
        
    ),
    

    '_utenti'=>array(
        'table'=>'TAB_utenti',
        'table_set'=>'TAB_utenti_cat',
        'usr_field'=>'email',
        'psw_field'=>'password',
        'id_form'=>'_utenti', //Must be unic IF empty Is set FROM DEFAULT
        'add_where'=>" AND attivo='1' AND bannato='0' ",
        'redirect_from'=>'',
        'msg_login_yes'=>'Accesso alla tua area riservata',
        'msg_login_no'=>'I dati insertiti non sono corretti',
        'psw_crypt_function'=>'md5',
        
    ),

);





?>