<?php
$lang["home-title"]="UNORA";
#
#
#


$lang['cerca']="Cerca";

$lang['mesi'][1]="Gennaio";
$lang['mesi'][2]="Febbraio";
$lang['mesi'][3]="Marzo";
$lang['mesi'][4]="Aprile";
$lang['mesi'][5]="Maggio";
$lang['mesi'][6]="Giugno";
$lang['mesi'][7]="Luglio";
$lang['mesi'][8]="Agosto";
$lang['mesi'][9]="Settembre";
$lang['mesi'][10]="Ottobre";
$lang['mesi'][11]="Novembre";
$lang['mesi'][12]="Dicembre";

$lang['settimana']['1']="Lunedì";
$lang['settimana']['2']="Martedì";
$lang['settimana']['3']="Mercoledì";
$lang['settimana']['4']="Giovedì";
$lang['settimana']['5']="Venerdì";
$lang['settimana']['6']="Sabato";
$lang['settimana']['7']="Domenica";

$lang['settimana_short']['1']="Lun";
$lang['settimana_short']['2']="Mar";
$lang['settimana_short']['3']="Mer";
$lang['settimana_short']['4']="Gio";
$lang['settimana_short']['5']="Ven";
$lang['settimana_short']['6']="Sab";
$lang['settimana_short']['7']="Dom";

$lang['URI']['pagine']="pagina";
$lang['URI']['tipologie']="tipo";
$lang['URI']['progetti']="progetti";
$lang['URI']['video']="video";
$lang['URI']['news']="news";
$lang['URI']['search']="ricerca";

$lang['projects']="progetti";
$lang['news']="notizie";
$lang['events']="eventi";

$lang['mailing-list']="mailing list";
$lang['contatti']="contatti";
$lang['condividi']="condividi";

$lang['email']="E-Mail";
$lang['nome']="Nome";
$lang['cognome']="Cognome";
$lang['provincia']="Proivncia";
$lang['paese']="Paese";
$lang['Leggi-linformativa-sulla-privacy']="Leggi l'informativa sulla privacy";
$lang['iscriviti']="Iscriviti";
