<?php
$lang["home-title-es"]="ZaLab - Video Participativo y Documentales";
#
#
#

$lang['documentary']="Documentales";
$lang['participatory-video']="Video Participativo";
$lang['other-paths']="Otras Localidades";

$lang['search-result']="Resultados de búsqueda";

$lang['za-news']="Noticias de Za";
$lang['world-news']="Noticias del Mundo";
$lang['calendar']="Calendario";

$lang['news-from-the-blog']="Noticias del Blog";
$lang['related-videos']="Videos relacionados";
$lang['press-kit-downloads']="Cartel y accesorios";
$lang['photogallery']="Galería de fotos";
$lang['calendary']="Calendario";
$lang['no-eventi-periodo']="No hay eventos en este periodo";
$lang['eventi']="Eventos";
$lang['avanti']="Siguiente";
$lang['indietro']="Volver";
$lang['eventi-successivi']="Hechos posteriores";
$lang['leggi-evento']="Leer evento";

$lang['cerca']="Buscar";

$lang['mesi'][1]="Enero";
$lang['mesi'][2]="Febrero";
$lang['mesi'][3]="Marzo";
$lang['mesi'][4]="Abril";
$lang['mesi'][5]="Mayo";
$lang['mesi'][6]="Junio";
$lang['mesi'][7]="Julio";
$lang['mesi'][8]="Agosto";
$lang['mesi'][9]="Septiembre";
$lang['mesi'][10]="Octubre";
$lang['mesi'][11]="Noviembre";
$lang['mesi'][12]="Diciembre";

$lang['settimana']['1']="Lunes";
$lang['settimana']['2']="Martes";
$lang['settimana']['3']="Miércoles";
$lang['settimana']['4']="Jueves";
$lang['settimana']['5']="Viernes";
$lang['settimana']['6']="Sábado";
$lang['settimana']['7']="Domingo";

$lang['settimana_short']['1']="Lun";
$lang['settimana_short']['2']="Mar";
$lang['settimana_short']['3']="Mie";
$lang['settimana_short']['4']="Jue";
$lang['settimana_short']['5']="Vie";
$lang['settimana_short']['6']="Sab";
$lang['settimana_short']['7']="Dom";

$lang['URI']['pagine']="pagina-es";
$lang['URI']['tipologie']="tipos-es";
$lang['URI']['progetti']="proyectos-es";
$lang['URI']['video']="video-es";
$lang['URI']['news']="news-es";
$lang['URI']['search']="buscar-es";

$lang['projects']="proyectos";
$lang['news']="noticias";
$lang['events']="eventos";

$lang['mailing-list']="mailing list";
$lang['contatti']="contactos";
$lang['condividi']="participación";

$lang['email']="E-Mail";
$lang['nome']="Nombre";
$lang['cognome']="Apellido";
$lang['provincia']="Proivncia";
$lang['paese']="País";
$lang['Leggi-linformativa-sulla-privacy']="Lea la política de privacidad";
$lang['iscriviti']="Subscribe";
