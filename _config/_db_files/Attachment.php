<?php
$db_files=array(
'IMG'=>array( //Better UPPERCASE thei go into the loop
    'campo_upload'=>'img', //Field in the form
    'campo_db'=>'img', //Field in the database where store the file name
    //'name_file'=>array('#id','_','#slug'), //How to compose the file name if start with # must take it from table
    'name_file'=>array('#id'),
    'max_filesize'=>'15000000', //In kilobyte : 1 kilobyte = 1024 bytes
    'upload_path'=>'_files/img/attachment/', //From rootDOC path
    'allowed_estentions'=>array('.jpg','.jpeg','.gif','.png','.tif','.tiff'), //Need "."
    'type'=>'image', //Only descrittive one
    'image_sets'=>array( //Only for Image type
        //Script save a copy for all item of this array
        'min'=>array('w'=>'190','h'=>'190','est'=>'.png','default'=>rootDOC.'def_book_min.png'),
        'big'=>array('w'=>'300','h'=>'300','est'=>'.png','default'=>rootDOC.'def_book_big.png'),
        'large'=>array('w'=>'600','h'=>'500','est'=>'.png','default'=>rootDOC.'def_book_large.png'),
        'original'=>array('w'=>'0','h'=>'0','est'=>'.png'),
        
        ),
    //'alphabetic_storage'=>false,
    ),

'ATTACHMENT'=>array( //Better UPPERCASE thei go into the loop
    'campo_upload'=>'attachment', //Field in the form
    'campo_db'=>'fileext', //Field in the database where store the file name
    'name_file'=>array('#id'), //How to compose the file name if start with # must take it from table
    'max_filesize'=>'90000000', //In kilobyte : 1 kilobyte = 1024 bytes
    'upload_path'=>'_files/attachment/', //From rootDOC path
    'allowed_estentions'=>array('.pdf','.doc','.zip'), //Need "."
    'type'=>'attachment', //Only descrittive one
    ),
);