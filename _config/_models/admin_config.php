<?php
$rootCkeditor=$rootBase."_adm/ckeditor/";
$rootCkfinder="/_adm/ckfinder/";


$lang_default="it";
define("lang_default",$lang_default);





$TAB_ADMIN_ARRAY=array(
    $TAB_Area,
    $TAB_Attachment,
    $TAB_Menus,
    $TAB_Category,
    $TAB_CategoryElement,
    $TAB_Element,
    $TAB_Featured,
    $TAB_Featuredstype,
    $TAB_Images,
    $TAB_Links,
    $TAB_Menus,
    $TAB_Menustype,
    $TAB_TagElement,
    $TAB_Tags,
    $TAB_a_utenti,
);

$ADMIN_ACTION_LIST=array('new','del');

//pr($TAB_ADMIN_ARRAY);

$ADMIN_MENU_ARRAY=array(
'Impostazioni'=>array('url'=>'#1','login_set'=>'0','sub_menu'=>array(
    'Utenti Admin'=>array('url'=>rootWWW.'admin/tab/'.$TAB_a_utenti.'/','login_set'=>'0'),
    'DB Data'=>array('url'=>rootWWW.'admin/db_data/','login_set'=>'2'),
)),
'Archivi'=>array('url'=>'#1','login_set'=>'0','sub_menu'=>array(

    'Menu'=>array('url'=>rootWWW.'admin/tab/'.$TAB_Menus.'/','login_set'=>'0'),
    'Aree'=>array('url'=>rootWWW.'admin/tab/'.$TAB_Area.'/','login_set'=>'0'),
    'Categorie'=>array('url'=>rootWWW.'admin/tab/'.$TAB_Category.'/','login_set'=>'0'),
    'Elementi'=>array('url'=>rootWWW.'admin/tab/'.$TAB_Element.'/','login_set'=>'0'),
    'Featured'=>array('url'=>rootWWW.'admin/tab/'.$TAB_Featured.'/','login_set'=>'0'),
    'Immagini'=>array('url'=>rootWWW.'admin/tab/'.$TAB_Images.'/','login_set'=>'0'),
    'Links'=>array('url'=>rootWWW.'admin/tab/'.$TAB_Links.'/','login_set'=>'0'),
    'Allegati'=>array('url'=>rootWWW.'admin/tab/'.$TAB_Attachment.'/','login_set'=>'0'),
    'Tags'=>array('url'=>rootWWW.'admin/tab/'.$TAB_Tags.'/','login_set'=>'0'),


    )),

/*
'Varie'=>array('url'=>'#1','login_set'=>'0','sub_menu'=>array(
    'Menus'=>array('url'=>rootWWW.'admin/tab/'.$TAB_menus.'/','login_set'=>'2'),
    //'Testi'=>array('url'=>rootWWW.'admin/tab/'.$TAB_testi.'/','login_set'=>'2'),
    //'DB BackUp'=>array('url'=>rootWWW.'admin/db_bu/','login_set'=>'2'),
    //'Slide Home'=>array('url'=>rootWWW.'admin/tab/home_slide/','login_set'=>'2'),
    //'Menu'=>array('url'=>rootWWW.'admin/tab/menus/','login_set'=>'2'),

    )),
*/

);






function bulkAction($a)
{
    //$bulkAction_fields=array('id_libri_collane','id_libri_tipologie','id_libri_focus[]','percB2C','costoB2C','costoB2B','costo_fissoB2B');
    $bulkAction_fields=array('id_insiemi','id_collane','id_tipologie','id_focus[]','percB2C','costoB2C','costoB2B','costo_fissoB2B');

    //pr($a);exit;
    //pr($a['POST_DATA']);
    if(isset($a['POST_DATA']['BULK_ACTION']))
    {

        $loop_id=false;

        if($a['POST_DATA']['apply_to']=="sel")
        {
            $loop_id=$a['POST_DATA']['records_id'];
            //pr($loop_id);
        }
        else
        {
            /*

                Recovering data _loop config

            */
            $c=$a['LOOP_CONFIG'][$a['USE_TAB']];
            $loop=dbAction::_loop(array('tab'=>$c['tab'],'tab2'=>$c['tab2'],'where'=>$c['where'],'limit'=>' LIMIT 0,900 '));
            for($i=0;$i<count($loop);$i++)
            {
                $loop_id[]=$loop[$i]['id'];
            }

            //pr($loop_id);
        }

        /*

            If $loop_id = true
            means that there is a loop of id
            on to apply the bulk action

        */
        if($loop_id)
        {

            //pr($loop_id);

            /*

                Check if one of the value is set
                looking in POST_DATA from the
                $bulkAction_fields array

            */


            foreach($loop_id as $i=>$id)
            {
            //$W="";
            //pr($bulkAction_fields);
            $data=_CORE::onlyNotEmptyValue($a['POST_DATA']);
            //pr($data);
            $q=array('tab'=>$a['USE_TAB'],'id'=>$id,'data'=>$data,'only_this_fileds'=>$bulkAction_fields,'where'=>$W,'echo'=>'0','DEMO'=>false);
            $u=dbAction::_update($q);
            //echo'';
            }


            if($u)
            {
                htmlHelper::setFlash("msg","Aggiornamento eseguito su ".count($loop_id)." records");
                header("location: ".$a['url_mod']);exit;
            }
            else
            {
                htmlHelper::setFlash("err","Aggiornamento non eseguito,<br />o nessun dato da aggiornare.");

            }

        } else {
        htmlHelper::setFlash("err","Nessun Record da aggiornare");
        }
    }
    return $a;
}





$FILTERSPECIAL_A[$TAB_prodotti]=array(
    array('value'=>'no_editore','name'=>__('senza editore')),
    array('value'=>'no_pagine','name'=>__('senza pagine')),
    array('value'=>'no_costob2c','name'=>__('senza costo B2C')),
    array('value'=>'no_costob2b','name'=>__('senza costo B2B')),
    array('value'=>'no_scontob2c','name'=>__('senza sconto B2C')),
    array('value'=>'no_scontob2b','name'=>__('senza sconto B2B')),
    array('value'=>'con_scontob2c','name'=>__('con sconto B2C')),
    array('value'=>'con_scontob2b','name'=>__('con sconto B2B')),


    );

function filterSpecial_SET($a)
{
    global $FILTERSPECIAL_A;

    //pr($a['GET_DATA']);
    //if(in_array($a['GET_DATA']['filter_special'],$FILTERSPECIAL_A[$a['USE_TAB']]))
    $LOOK_FOR=search_in_array($FILTERSPECIAL_A[$a['USE_TAB']], "value", $a['GET_DATA']['filter_special']);
    if($a['GET_DATA']['filter_special']=="clear")
    {
        unset($_SESSION['FILTERS'][$a['USE_TAB']]['filter_special']);
        _CORE::redirect(array('location'=>$a['url_mod']));
    }
    elseif($LOOK_FOR)
    {
    //pr($LOOK_FOR);
    ### ----> SET
    $_SESSION['FILTERS'][$a['USE_TAB']]['filter_special']=$a['GET_DATA']['filter_special'];
    //pr($a['url_mod']);exit;
    _CORE::redirect(array('location'=>$a['url_mod']));
    }

    return $a;
}

function filterSpecial_CHECK($a,$settings=null) //$settings is for where!
{
    $FS=$_SESSION['FILTERS'][$a['USE_TAB']]['filter_special'];
    switch ($FS)
    {
    case 'no_editore':
        $settings['where']=" WHERE id_editori='0' ";
        break;

    }

    return $settings;
}







function tableSettings($a,$settings=null)
{
    global $TAB_Element;
    global $TAB_Featured;
    global $TAB_Tags;
    global $TAB_Menus;
    global $TAB_Area;
    global $TAB_Category;
    global $TAB_a_utenti;

    $t=$a['USE_TAB'];
    switch ($t)
    {
        case $TAB_Area:
            $settings=$settings;

            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY isaccessible DESC, weight, h1";
            $settings['no_default_db_files']=true;

        break;

        case $TAB_Category:
            $settings=$settings;

            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY view, id_area, isaccessible DESC, weight, h1";
            $settings['no_default_db_files']=true;

        break;

        case $TAB_Tags:
            $settings=$settings;

            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY text, slug, id DESC";
            $settings['no_default_db_files']=true;

        break;

        case $TAB_Menus:
            $settings=$settings;

            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY visibility DESC, id_menustype, weight, id DESC";
            $settings['no_default_db_files']=true;

        break;

        case $TAB_Element:
            $settings=$settings;

            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY isaccessible DESC, view, weight, id DESC";
            $settings['no_default_db_files']=true;

        break;

        case $TAB_Featured:
            $settings=$settings;

            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY visibility DESC, id_featuredstype, weight, id DESC";
            $settings['no_default_db_files']=true;

        break;


        case $TAB_a_utenti:
            $settings=$settings;

            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY id DESC";
            $settings['no_default_db_files']=true;

        break;

        default:
            $settings=$settings;

            $settings['records_for_pag']="30";
            $settings['orderby']=" ORDER BY weight ASC";
            $settings['no_default_db_files']=true;

        break;

            }

    return $settings;
}//END of f. tableSettings()

$viewTypeArr[$TAB_Area]=array('blog','services');
$viewTypeArr[$TAB_Element]=array('product','contacts');
$viewTypeArr[$TAB_Category]=array('product','mamazine');


/*
 * To activate in the admin
 * the languages select this function
 * must be here and must return an array of
 * at least 2 items
 */

//function getLanguages()
//{
//    return array(
//        'it'=>'Italiano',
//        'en'=>'English'
//    );
//}

/*
 * Activation of Plugin and SPecial FUnction
 */
//define("EasyEcommerce",TRUE);
//define("ElementChilds",TRUE);

//define("inHome",TRUE);
//define("ExtraFields",TRUE);
//function getExtraFieldsType()
//{
//    return array(
//        'slugExtraFields1',
//        'slugExtraFields1',
//    );
//}


//define("ImagesBulkUpload",TRUE);


$featureView['home']=array(
    'default'=>array('featureds'=>
        array(
            'banner'=>array('Bannerino info')
        )
    ),

);



$featureView[$TAB_Category]=array(
    'default'=>array('featureds'=>
        array(
            'banner'=>array('Bannerino info')
        )
    ),

);



$defaultElementsToCreate = array('home','cookie','privacy-policy');
