<?php
$rootCkeditor=$rootBase."_adm/ckeditor/"; 
$rootCkfinder="/_adm/ckfinder/";



function _isUserOfCat($a,$cat_a)
{
    if(!is_natural_number($a['USER']['id'])){$err="1";}
    if(!is_array($cat_a)){$err="2";}
    
    foreach ($cat_a AS $I)
    {
        //pr($a['_CAT_UTENTI_'][$I]);
        if($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_'][$I]){return TRUE;}
    }
    $err="3";
    
    if($err){/*pr($err);*/return FALSE;}
}



$lang_default="it";
define("lang_default",$lang_default); 

$OFFICE_PERMS_TO_ACTION=array(
                    'donation_settings'=>array('unora','ref_azienda'),
                    
                    );
$OFFICE_PERMS_TO_TAB=array(
                    //$TAB_utenti=>array('unora','ref_azienda'),
                    $TAB_utenti=>array('unora','ref_azienda'),
                    $TAB_onlus=>array('unora'),
                    $TAB_aziende=>array('unora'),
                    $TAB_donazioni=>array('unora','ref_azienda'),
                    $TAB_doc_ordini_pagamento=>array('unora','ref_azienda'),
                    $TAB_doc_ordini_pagamento_onp=>array('unora','ref_onp'),
                    $TAB_donazioni_ricevute=>array('unora'),
                    );

$TAB_OFFICE_ARRAY=array(
                       $TAB_utenti,
                       $TAB_onlus,
                       $TAB_aziende,
                       $TAB_donazioni,
                       $TAB_doc_ordini_pagamento,
                       $TAB_doc_ordini_pagamento_onp,
                       $TAB_donazioni_ricevute,
                        
                       );
$OFFICE_ACTION_LIST=array('new','del');

//pr($TAB_OFFICE_ARRAY);


/*
    
        Oltre ad impostare il menu, in base alle categorie
        setta anche la visibilità o meno delle pagien stesse
        tramite un controllo impostato nel modello principale /office/class.php
        
    
*/
$OFFICE_MENU_ARRAY=array(
'Contenuti'=>array('url'=>'#1','login_set'=>'1','sub_menu'=>array(
    
    'Utenti'=>array('url'=>rootWWW.'office/tab/'.$TAB_utenti.'/','login_set'=>'1'),    
    'Aziende'=>array('url'=>rootWWW.'office/tab/'.$TAB_aziende.'/','login_set'=>'1'),    
    'Onlus'=>array('url'=>rootWWW.'office/tab/'.$TAB_onlus.'/','login_set'=>'1'),    
    )),
    

    //Per onp
'Elenchi'=>array('url'=>'#1','login_set'=>'3','sub_menu'=>array(
    'Pagamenti'=>array('url'=>rootWWW.'office/tab/'.$TAB_doc_ordini_pagamento_onp.'/','login_set'=>'3'),
    )),

'Archivi'=>array('url'=>'#1','login_set'=>'2','sub_menu'=>array(
    'Donatori'=>array('url'=>rootWWW.'office/tab/'.$TAB_utenti.'/','login_set'=>'2'),    
    'Donazioni'=>array('url'=>rootWWW.'office/tab/'.$TAB_donazioni.'/','login_set'=>'2'),
    'Pagamenti'=>array('url'=>rootWWW.'office/tab/'.$TAB_doc_ordini_pagamento.'/','login_set'=>'2'),
    )),
    
'Donation Info'=>array('url'=>rootWWW.'office/donation_settings/','login_set'=>'2'),

'Varie'=>array('url'=>'#1','login_set'=>'1','sub_menu'=>array(
    //'Donation Info'=>array('url'=>rootWWW.'office/donation_settings/','login_set'=>'1'),
    'Donazioni'=>array('url'=>rootWWW.'office/tab/'.$TAB_donazioni.'/','login_set'=>'1'),
    'Pagamenti Aziende'=>array('url'=>rootWWW.'office/tab/'.$TAB_doc_ordini_pagamento.'/','login_set'=>'1'),
    'Pagamenti Onp'=>array('url'=>rootWWW.'office/tab/'.$TAB_doc_ordini_pagamento_onp.'/','login_set'=>'1'),
    
    
    //'Testi'=>array('url'=>rootWWW.'office/tab/'.$TAB_testi.'/','login_set'=>'2'),
    //'DB BackUp'=>array('url'=>rootWWW.'office/db_bu/','login_set'=>'2'),
    //'Slide Home'=>array('url'=>rootWWW.'office/tab/home_slide/','login_set'=>'2'),
    //'Menu'=>array('url'=>rootWWW.'office/tab/menus/','login_set'=>'2'),
    
    )),

    'Report'=>array('url'=>'#1','login_set'=>'1','sub_menu'=>array(
    'Lettere ai Donatori'=>array('url'=>rootWWW.'office/tab/'.$TAB_donazioni_ricevute.'/','login_set'=>'1'),
    
    )),
);






function bulkAction($a)
{
    //$bulkAction_fields=array('id_libri_collane','id_libri_tipologie','id_libri_focus[]','percB2C','costoB2C','costoB2B','costo_fissoB2B');
    $bulkAction_fields=array('id_insiemi','id_collane','id_tipologie','id_focus[]','percB2C','costoB2C','costoB2B','costo_fissoB2B');
    
    //pr($a);exit;
    //pr($a['POST_DATA']);
    if(isset($a['POST_DATA']['BULK_ACTION']))
    {
            
        $loop_id=false;
            
        if($a['POST_DATA']['apply_to']=="sel")
        {
            $loop_id=$a['POST_DATA']['records_id'];
            //pr($loop_id);
        }
        else
        {
            /*
                
                Recovering data _loop config
                
            */
            $c=$a['LOOP_CONFIG'][$a['USE_TAB']];
            $loop=dbAction::_loop(array('tab'=>$c['tab'],'tab2'=>$c['tab2'],'where'=>$c['where'],'limit'=>' LIMIT 0,900 '));
            for($i=0;$i<count($loop);$i++)
            {
                $loop_id[]=$loop[$i]['id'];
            }
                
            //pr($loop_id);
        }
            
        /*
            
            If $loop_id = true
            means that there is a loop of id
            on to apply the bulk action
            
        */
        if($loop_id) 
        {
                
            //pr($loop_id);
                
            /*
                
                Check if one of the value is set
                looking in POST_DATA from the
                $bulkAction_fields array
                
            */
                
                
            foreach($loop_id as $i=>$id)
            {
            //$W="";
            //pr($bulkAction_fields);
            $data=_CORE::onlyNotEmptyValue($a['POST_DATA']);
            //pr($data);
            $q=array('tab'=>$a['USE_TAB'],'id'=>$id,'data'=>$data,'only_this_fileds'=>$bulkAction_fields,'where'=>$W,'echo'=>'0','DEMO'=>false);
            $u=dbAction::_update($q);
            //echo'';    
            }
                
                
            if($u)
            {
                htmlHelper::setFlash("msg","Aggiornamento eseguito su ".count($loop_id)." records");
                header("location: ".$a['url_mod']);exit;
            }
            else
            {
                htmlHelper::setFlash("err","Aggiornamento non eseguito,<br />o nessun dato da aggiornare.");
            
            }
            
        } else {
        htmlHelper::setFlash("err","Nessun Record da aggiornare");
        }
    }
    return $a;
}







$FILTERSPECIAL_A[$TAB_utenti]=array(
    array('value'=>'stato_0','name'=>__('Stato: In Attesa')),
    array('value'=>'stato_1','name'=>__('Stato: Confermato')),
    array('value'=>'stato_2','name'=>__('Stato: Sospeso')),
    array('value'=>'stato_3','name'=>__('Stato: In Attesa & Confermato')),
    
    );


$FILTERSPECIAL_A[$TAB_aziende]=array(
    array('value'=>'az_stato_0','name'=>__('Stato: In Attesa')),
    array('value'=>'az_stato_1','name'=>__('Stato: Confermata')),
    array('value'=>'az_stato_2','name'=>__('Stato: Sospesa')),
    
    );

$FILTERSPECIAL_A[$TAB_onlus]=array(
    array('value'=>'onlus_stato_0','name'=>__('Stato: In Attesa')),
    array('value'=>'onlus_stato_1','name'=>__('Stato: Confermata')),
    array('value'=>'onlus_stato_2','name'=>__('Stato: Sospesa')),
    
    );

    
    
    
    
    
function filterSpecial_SET($a)
{
    global $FILTERSPECIAL_A;
    
    //pr($a['GET_DATA']);
    //if(in_array($a['GET_DATA']['filter_special'],$FILTERSPECIAL_A[$a['USE_TAB']]))
    $LOOK_FOR=search_in_array($FILTERSPECIAL_A[$a['USE_TAB']], "value", $a['GET_DATA']['filter_special']);
    if($a['GET_DATA']['filter_special']=="clear")
    {
        unset($_SESSION['FILTERS'][$a['USE_TAB']]['filter_special']);
        _CORE::redirect(array('location'=>$a['url_mod']));
    }
    elseif($LOOK_FOR)
    {
    //pr($LOOK_FOR); 
    ### ----> SET 
    $_SESSION['FILTERS'][$a['USE_TAB']]['filter_special']=$a['GET_DATA']['filter_special'];
    //pr($a['url_mod']);exit;
    _CORE::redirect(array('location'=>$a['url_mod']));
    }
    
    return $a;
}

function filterSpecial_CHECK($a,$settings=null) //$settings is for where!
{
    $FS=$_SESSION['FILTERS'][$a['USE_TAB']]['filter_special'];    
    switch ($FS)
    {
    case 'stato_0':
        $settings['where']=" WHERE stato='0' AND id_utenti_cat='".$a['_CAT_UTENTI_']['donatori']."'  ";
        break;
    case 'stato_1':
        $settings['where']=" WHERE stato='1' AND id_utenti_cat='".$a['_CAT_UTENTI_']['donatori']."'  "; //AND id_aziende='".$a['USER']['id_aziende']."'
        break;
    case 'stato_2':
        $settings['where']=" WHERE stato='2' AND id_utenti_cat='".$a['_CAT_UTENTI_']['donatori']."'  "; //AND id_aziende='".$a['USER']['id_aziende']."'
        break;
    case 'stato_3':
        $settings['where']=" WHERE (stato='0' OR stato='1') AND id_utenti_cat='".$a['_CAT_UTENTI_']['donatori']."'  "; //AND id_aziende='".$a['USER']['id_aziende']."'
        break;
        
    
    
    
    
    case 'az_stato_0':
        $settings['where']=" WHERE stato='0'  ";
        break;
    case 'az_stato_1':
        $settings['where']=" WHERE stato='1'  ";
        break;
    case 'az_stato_2':
        $settings['where']=" WHERE stato='2'  ";
        break;
        
    case 'onlus_stato_0':
        $settings['where']=" WHERE stato='0'  ";
        break;
    case 'onlus_stato_1':
        $settings['where']=" WHERE stato='1'  ";
        break;
    case 'onlus_stato_2':
        $settings['where']=" WHERE stato='2'  ";
        break;
    
    }
        
    return $settings;
}







function tableSettings($a,$settings=null)
{
    global $TAB_cat;
    global $TAB_menus;
    global $TAB_pagine;
    global $TAB_utenti;
    global $TAB_tipologie;
    global $TAB_progetti;
    global $TAB_video;
    global $TAB_eventi;
    global $TAB_testi;
    global $TAB_foto;
    global $TAB_allegati;
    global $TAB_aziende;
    global $TAB_onlus;
    global $TAB_donazioni;
    global $TAB_doc_ordini_pagamento;
    global $TAB_doc_ordini_pagamento_onp;
    global $TAB_donazioni_ricevute;
    
    $t=$a['USE_TAB'];
    switch ($t)
    {
        case $TAB_donazioni_ricevute: 
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY id ";
            $settings['no_default_db_files']=true;
            
        break;
        
        case $TAB_doc_ordini_pagamento_onp:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY id ";
            $settings['no_default_db_files']=true;
            
        break;
        
        case $TAB_doc_ordini_pagamento:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY id DESC";
            $settings['no_default_db_files']=true;
            
        break;
        case $TAB_donazioni:
            $settings=$settings;
                
            $settings['records_for_pag']="350";
            $settings['orderby']=" ORDER BY id DESC";
            $settings['no_default_db_files']=true;
            
        break;
        case $TAB_aziende:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY titolo, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_onlus:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY titolo, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_allegati:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY posizione, titolo_en, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_foto:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY posizione, titolo, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_testi:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY slug, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_tipologie:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY titolo, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_video:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY titolo_en, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_progetti:
            $settings=$settings;
                
            $settings['records_for_pag']="50";
            $settings['orderby']=" ORDER BY titolo, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_utenti:
            $settings=$settings;
                
            $settings['records_for_pag']="30";
            $settings['orderby']=" ORDER BY cognome, nome";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_menus:
            $settings=$settings;
                
            $settings['records_for_pag']="30";
            $settings['orderby']=" ORDER BY menu, posizione, nome, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            
        case $TAB_eventi:
            $settings=$settings;
                
            $settings['records_for_pag']="30";
            $settings['orderby']=" ORDER BY data_pubblicazione DESC, titolo, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
        
        case $TAB_pagine:
            $settings=$settings;
                
            $settings['records_for_pag']="30";
            $settings['orderby']=" ORDER BY titolo, id DESC";
            $settings['no_default_db_files']=true;
            
        break;
            }
    
    return $settings;
}//END of f. tableSettings()
