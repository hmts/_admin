<?php
/***
    This File is incleded by defaultfrom init.php
    and by default must be empty
    
    Every specific project
    can save data here
    
    Like array, function ad other vars.
    
***/




function my_round($cifra)
{
    return number_format( ( floor($cifra*100)/100 ) ,2);
}


function format_euro($cifra)
{
$cifra=number_format($cifra,2,".","");
return $cifra;        
}
function aasort ($array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    $array=$ret;
}


function _data($timestamp,$settings=null)
{
    //_data($timestamp,array('mesecorto','ore'));
    
    global $lang;
        
        $S=$settings;
        
        $GIORNO=date("d",$timestamp);
        $MESE=$lang['mesi'][date("n",$timestamp)];
        if($S['mesecorto'])
        {
            $MESE=substr($MESE,0,3);
        }
        $ANNO=date("Y",$timestamp);
        $data=$GIORNO." ".$MESE." ".$ANNO;
        if($S['ore'])
        {
        $data.=" ore: ".    date("H:i",$timestamp);
        }
    
    return $data;
}

$_STATO[0]="In Attesa";
$_STATO[1]="Confermato";
$_STATO[2]="Sospeso";



$MENU_TYPE_ARRAY=array('top1','footer1');

$SMTP_DATA=array();
$SMTP_DATA['server']="mail.consorzioreal.info"; 
//$SMTP_DATA'PORT']="587";   //587; 
$SMTP_DATA['username']="info@consorzioreal.info"; 
$SMTP_DATA['password']="8df7&435"; 
$SMTP_DATA['mittente']="noreplay@unora.org"; 
$SMTP_DATA['mittente_nome']="UNORA"; 


define("URL_PSW_RECOVER",rootWWW."recupera_psw/");
define("URL_PRIVACY",rootWWW."pagine/privacy/");
define("URL_LOGOUT",rootWWW."?logout");

define("URL_OFFICE",rootWWW."office/");



$REQ_A_dipendente=array('privacy','delego','email', 'nome','cognome','sede_lavoro','indirizzo','cap','citta','provincia','citta_di_nascita','codice_fiscale','tel','data_nascita_gg','data_nascita_mm','data_nascita_aaaa');
$REQ_A_contatti=array('privacy','newsletter','azienda','email', 'nome','cognome','sede_lavoro','indirizzo','cap','citta','tel');
$REQ_A_azienda=array('privacy','azienda','email','piva','sede_legale','cap','citta','tel','settore','email_referente','nome_referente','cognome_referente','mansione_referente','tel_referente');
$REQ_A_onp=array('privacy','email','associazione','piva','cf','indirizzo','cap','citta','tel');

function setObField($campo,$a_err_name)
{
global $REQ_A_azienda;
global $REQ_A_aziendanuova;
global $REQ_A_contatti;
global $REQ_A_onp;
$NAME_F="REQ_A_".$a_err_name;
$a_err=$$NAME_F;
    if(is_array($a_err)){
    if(in_array($campo,$a_err)){echo' *';}
    }
}
function fieldErrCheck($campo,$a_err)
{
    if(is_array($a_err)){
        if(in_array($campo,$a_err)){echo'err';}
    }
}
/*
if(!isset($_SESSION['LANG'])){$_SESSION['LANG']=$language;}
define("LANG",$_SESSION['LANG']);
*/





function setExcelOutput($a,$settings)
{
    
    
$view=$settings['views'];    
$filename="sheet.xls";
if(!empty($settings['file_name'])){$filename=$settings['file_name'];}
$title=$filename;
if(!empty($settings['title'])){$title=$settings['title'];}    


    
    if(!empty($settings['csv']))
    {
        if(!$settings['echo'])
        {
            
    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Disposition: attachment; filename='.$filename.'');
    //echo "\xEF\xBB\xBF"; // UTF-8 BOM
        }
        //echo $view."";
        if(!empty($view)){include($view);}
        
    //pr($settings);
    }
    else
    {
        if(!$settings['echo'])
        {
    header ("Content-Type: application/vnd.ms-excel");
    header ("Content-Disposition: inline; filename=$filename");
        }
    echo'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    echo'<html lang=it><head><title>'.$title.'</title></head><body>';
    if(!empty($view)){include($view);}
    echo'</body></html>';
    exit;   
    }

}
