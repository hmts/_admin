<?php
function campiTab($tab,$completo=null){
	switch ($tab) {

/*TABELLA: AREA*/
	case 'Area':

	$array=array('id','title','slug','description','keywords','h1','h2','text','abstract','weight','listed','isaccessible','redirect','lang','createdon','modifyon','view');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'title'=>array('Field' => 'title','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'slug'=>array('Field' => 'slug','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'description'=>array('Field' => 'description','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'keywords'=>array('Field' => 'keywords','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h1'=>array('Field' => 'h1','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h2'=>array('Field' => 'h2','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'text'=>array('Field' => 'text','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'abstract'=>array('Field' => 'abstract','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'weight'=>array('Field' => 'weight','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'listed'=>array('Field' => 'listed','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'isaccessible'=>array('Field' => 'isaccessible','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'redirect'=>array('Field' => 'redirect','Type' => 'varchar(900)','Null' => 'YES','Key' => '','Default' => '','Extra' => ''),
	'lang'=>array('Field' => 'lang','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'createdon'=>array('Field' => 'createdon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'modifyon'=>array('Field' => 'modifyon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'view'=>array('Field' => 'view','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: ATTACHMENT*/
	case 'Attachment':

	$array=array('id','id_category','id_element','h1','h2','text','weight','visibility','password','lang','createdon','modifyon','fileext');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'id_category'=>array('Field' => 'id_category','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_element'=>array('Field' => 'id_element','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'h1'=>array('Field' => 'h1','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h2'=>array('Field' => 'h2','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'text'=>array('Field' => 'text','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'weight'=>array('Field' => 'weight','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'visibility'=>array('Field' => 'visibility','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'password'=>array('Field' => 'password','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'lang'=>array('Field' => 'lang','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'createdon'=>array('Field' => 'createdon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'modifyon'=>array('Field' => 'modifyon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'fileext'=>array('Field' => 'fileext','Type' => 'varchar(50)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: CATEGORY*/
	case 'Category':

	$array=array('id','id_category','id_area','title','slug','description','keywords','h1','h2','text','abstract','weight','listed','isaccessible','redirect','lang','createdon','modifyon','view');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'id_category'=>array('Field' => 'id_category','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_area'=>array('Field' => 'id_area','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'title'=>array('Field' => 'title','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'slug'=>array('Field' => 'slug','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'description'=>array('Field' => 'description','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'keywords'=>array('Field' => 'keywords','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h1'=>array('Field' => 'h1','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h2'=>array('Field' => 'h2','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'text'=>array('Field' => 'text','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'abstract'=>array('Field' => 'abstract','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'weight'=>array('Field' => 'weight','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'listed'=>array('Field' => 'listed','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'isaccessible'=>array('Field' => 'isaccessible','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'redirect'=>array('Field' => 'redirect','Type' => 'varchar(900)','Null' => 'YES','Key' => '','Default' => '','Extra' => ''),
	'lang'=>array('Field' => 'lang','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'createdon'=>array('Field' => 'createdon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'modifyon'=>array('Field' => 'modifyon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'view'=>array('Field' => 'view','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: CATEGORY_ELEMENT*/
	case 'Category_Element':

	$array=array('id','id_Category','id_Element');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'id_Category'=>array('Field' => 'id_Category','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'id_Element'=>array('Field' => 'id_Element','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: ELEMENT*/
	case 'Element':

	$array=array('id','title','slug','description','keywords','h1','h2','text','abstract','weight','listed','isaccessible','redirect','lang','createdon','modifyon','view','price','qty','grams','stockid','id_parent','inhome');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'title'=>array('Field' => 'title','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'slug'=>array('Field' => 'slug','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'description'=>array('Field' => 'description','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'keywords'=>array('Field' => 'keywords','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h1'=>array('Field' => 'h1','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h2'=>array('Field' => 'h2','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'text'=>array('Field' => 'text','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'abstract'=>array('Field' => 'abstract','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'weight'=>array('Field' => 'weight','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'listed'=>array('Field' => 'listed','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'isaccessible'=>array('Field' => 'isaccessible','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'redirect'=>array('Field' => 'redirect','Type' => 'varchar(900)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'lang'=>array('Field' => 'lang','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'createdon'=>array('Field' => 'createdon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'modifyon'=>array('Field' => 'modifyon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'view'=>array('Field' => 'view','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'price'=>array('Field' => 'price','Type' => 'decimal(12,2)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'qty'=>array('Field' => 'qty','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'grams'=>array('Field' => 'grams','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'stockid'=>array('Field' => 'stockid','Type' => 'varchar(90)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'id_parent'=>array('Field' => 'id_parent','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'inhome'=>array('Field' => 'inhome','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: ELEMENTCATEGORY*/
	case 'ElementCategory':

	$array=array('id_element','id_category');

	$arrayCompleto=array(
	'id_element'=>array('Field' => 'id_element','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => ''),
	'id_category'=>array('Field' => 'id_category','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: ELEMENTTAGS*/
	case 'ElementTags':

	$array=array('id_element','id_tags');

	$arrayCompleto=array(
	'id_element'=>array('Field' => 'id_element','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => ''),
	'id_tags'=>array('Field' => 'id_tags','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: EXTRAFIELDS*/
	case 'Extrafields':

	$array=array('id','id_element','slug','value');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'id_element'=>array('Field' => 'id_element','Type' => 'int(11)','Null' => 'NO','Key' => 'MUL','Default' => '','Extra' => ''),
	'slug'=>array('Field' => 'slug','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'value'=>array('Field' => 'value','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: FEATURED*/
	case 'Featured':

	$array=array('id','id_featuredstype','id_area','id_category','id_element','h1','h2','text','weight','visibility','redirect','lang','createdon','modifyon');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'id_featuredstype'=>array('Field' => 'id_featuredstype','Type' => 'int(11)','Null' => 'NO','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_area'=>array('Field' => 'id_area','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_category'=>array('Field' => 'id_category','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_element'=>array('Field' => 'id_element','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'h1'=>array('Field' => 'h1','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h2'=>array('Field' => 'h2','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'text'=>array('Field' => 'text','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'weight'=>array('Field' => 'weight','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'visibility'=>array('Field' => 'visibility','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'redirect'=>array('Field' => 'redirect','Type' => 'varchar(900)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'lang'=>array('Field' => 'lang','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'createdon'=>array('Field' => 'createdon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'modifyon'=>array('Field' => 'modifyon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: FEATUREDSTYPE*/
	case 'Featuredstype':

	$array=array('id','text','slug');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'text'=>array('Field' => 'text','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'slug'=>array('Field' => 'slug','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: IMAGES*/
	case 'Images':

	$array=array('id','id_category','id_element','slug','h1','h2','text','weight','visibility','lang','createdon','modifyon');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'id_category'=>array('Field' => 'id_category','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_element'=>array('Field' => 'id_element','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'slug'=>array('Field' => 'slug','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h1'=>array('Field' => 'h1','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h2'=>array('Field' => 'h2','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'text'=>array('Field' => 'text','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'weight'=>array('Field' => 'weight','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'visibility'=>array('Field' => 'visibility','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'lang'=>array('Field' => 'lang','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'createdon'=>array('Field' => 'createdon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'modifyon'=>array('Field' => 'modifyon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: LINKS*/
	case 'Links':

	$array=array('id','id_category','id_element','h1','h2','text','uri','weight','visibility','lang','createdon','modifyon');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'id_category'=>array('Field' => 'id_category','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_element'=>array('Field' => 'id_element','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'h1'=>array('Field' => 'h1','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'h2'=>array('Field' => 'h2','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'text'=>array('Field' => 'text','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'uri'=>array('Field' => 'uri','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'weight'=>array('Field' => 'weight','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'visibility'=>array('Field' => 'visibility','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'lang'=>array('Field' => 'lang','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'createdon'=>array('Field' => 'createdon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'modifyon'=>array('Field' => 'modifyon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: MENUS*/
	case 'Menus':

	$array=array('id','id_menustype','id_category','id_element','label','weight','visibility','redirect','lang','createdon','modifyon','id_menus','id_area');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'id_menustype'=>array('Field' => 'id_menustype','Type' => 'int(11)','Null' => 'NO','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_category'=>array('Field' => 'id_category','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_element'=>array('Field' => 'id_element','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'label'=>array('Field' => 'label','Type' => 'longtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'weight'=>array('Field' => 'weight','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'visibility'=>array('Field' => 'visibility','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'redirect'=>array('Field' => 'redirect','Type' => 'varchar(900)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'lang'=>array('Field' => 'lang','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'createdon'=>array('Field' => 'createdon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'modifyon'=>array('Field' => 'modifyon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'id_menus'=>array('Field' => 'id_menus','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_area'=>array('Field' => 'id_area','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: MENUSTYPE*/
	case 'Menustype':

	$array=array('id','text','slug');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'text'=>array('Field' => 'text','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'slug'=>array('Field' => 'slug','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: TAGS*/
	case 'Tags':

	$array=array('id','text','lang','createdon','modifyon','slug','id_area','id_category');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'text'=>array('Field' => 'text','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'lang'=>array('Field' => 'lang','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'createdon'=>array('Field' => 'createdon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'modifyon'=>array('Field' => 'modifyon','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'slug'=>array('Field' => 'slug','Type' => 'varchar(255)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'id_area'=>array('Field' => 'id_area','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => ''),
	'id_category'=>array('Field' => 'id_category','Type' => 'int(11)','Null' => 'YES','Key' => 'MUL','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: A_ADMIN_MENU*/
	case 'a_admin_menu':

	$array=array('id','posizione','nome','admin','url');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'posizione'=>array('Field' => 'posizione','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'nome'=>array('Field' => 'nome','Type' => 'varchar(200)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'admin'=>array('Field' => 'admin','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'url'=>array('Field' => 'url','Type' => 'varchar(100)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: A_ADMIN_MENU_ITEMS*/
	case 'a_admin_menu_items':

	$array=array('id','nome','id_menu','selettore','id_s');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'nome'=>array('Field' => 'nome','Type' => 'varchar(200)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'id_menu'=>array('Field' => 'id_menu','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'selettore'=>array('Field' => 'selettore','Type' => 'varchar(50)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'id_s'=>array('Field' => 'id_s','Type' => 'varchar(200)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: A_ADMIN_USERS*/
	case 'a_admin_users':

	$array=array('id','nome','cognome','email','attivo','password','admin');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'nome'=>array('Field' => 'nome','Type' => 'varchar(100)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'cognome'=>array('Field' => 'cognome','Type' => 'varchar(100)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'email'=>array('Field' => 'email','Type' => 'varchar(100)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'attivo'=>array('Field' => 'attivo','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'password'=>array('Field' => 'password','Type' => 'varchar(100)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'admin'=>array('Field' => 'admin','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => '')
	);
	break;


/*TABELLA: A_UTENTI*/
	case 'a_utenti':

	$array=array('id','id_a_utenti_cat','nome','cognome','email','password','data_iscrizione','attivo','bannato','data_nascita','indirizzo','citta','provincia','regione','paese','cap','cf','piva');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'id_a_utenti_cat'=>array('Field' => 'id_a_utenti_cat','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'nome'=>array('Field' => 'nome','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'cognome'=>array('Field' => 'cognome','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'email'=>array('Field' => 'email','Type' => 'varchar(200)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'password'=>array('Field' => 'password','Type' => 'varchar(200)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'data_iscrizione'=>array('Field' => 'data_iscrizione','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'attivo'=>array('Field' => 'attivo','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'bannato'=>array('Field' => 'bannato','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'data_nascita'=>array('Field' => 'data_nascita','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'indirizzo'=>array('Field' => 'indirizzo','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'citta'=>array('Field' => 'citta','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'provincia'=>array('Field' => 'provincia','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'regione'=>array('Field' => 'regione','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'paese'=>array('Field' => 'paese','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'cap'=>array('Field' => 'cap','Type' => 'varchar(10)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'cf'=>array('Field' => 'cf','Type' => 'varchar(20)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'piva'=>array('Field' => 'piva','Type' => 'varchar(50)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;


/*TABELLA: A_UTENTI_CAT*/
	case 'a_utenti_cat':

	$array=array('id','nome','nome_uri','testo','testo2','visibile','posizione');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'nome'=>array('Field' => 'nome','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'nome_uri'=>array('Field' => 'nome_uri','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'testo'=>array('Field' => 'testo','Type' => 'mediumtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'testo2'=>array('Field' => 'testo2','Type' => 'mediumtext','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'visibile'=>array('Field' => 'visibile','Type' => 'int(1)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'posizione'=>array('Field' => 'posizione','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => '')
	);
	break;


/*TABELLA: ROUTES*/
	case 'routes':

	$array=array('id','name','slug','model','login','login_set','login_tab');

	$arrayCompleto=array(
	'id'=>array('Field' => 'id','Type' => 'int(11)','Null' => 'NO','Key' => 'PRI','Default' => '','Extra' => 'auto_increment'),
	'name'=>array('Field' => 'name','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'slug'=>array('Field' => 'slug','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'model'=>array('Field' => 'model','Type' => 'varchar(250)','Null' => 'NO','Key' => '','Default' => '','Extra' => ''),
	'login'=>array('Field' => 'login','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'login_set'=>array('Field' => 'login_set','Type' => 'int(11)','Null' => 'NO','Key' => '','Default' => '0','Extra' => ''),
	'login_tab'=>array('Field' => 'login_tab','Type' => 'varchar(200)','Null' => 'NO','Key' => '','Default' => '','Extra' => '')
	);
	break;



	default:
	$notFound="1";
	}




	if($notFound=="1")
	{
	return false;
	}
	else if(empty($completo))
	{
	return $array;
	} 
	else 
	{
	return $arrayCompleto;
	}

}


function elencoTAB(){
$a=array(
'Area','Attachment','Category','Category_Element','Element','ElementCategory','ElementTags','Extrafields','Featured','Featuredstype','Images','Links','Menus','Menustype','Tags','a_admin_menu','a_admin_menu_items','a_admin_users','a_utenti','a_utenti_cat','routes');
return $a;
}

?>