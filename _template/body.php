<body>

<?php //include(_CORE::addView("_elements/head")); ?>


<?php
  if (htmlHelper::hasFlash('msg')) 
  { 
    echo '<div id="flashMsg" onclick="toggleVisibility(\'flashMsg\',\'hide\');">'.htmlHelper::getFlash('msg')."</div>"; // output the flash message 
  }
  else if (htmlHelper::hasFlash('err')) 
  { 
    echo '<div id="flashMsg" onclick="toggleVisibility(\'flashMsg\',\'hide\');" class="err">'.htmlHelper::getFlash('err')."</div>"; // output the flash message 
  } 
?>


<!-- wrapper -->
<div class="wrapper">
    
        <!--main-->
        <div class="main_wrap container drop-shadow">     
       <?php include(_CORE::addView("_elements/head")); ?>
            
            
        <?php include($a['VIEW']); ?>
            
            
            
            
<?php include(_CORE::addView("_elements/footer")); ?>
    </div>
    <!-- // wrapper -->
</body>
