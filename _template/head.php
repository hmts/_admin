<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8>

<title><?php echo $a['title']; ?></title>

<?php //echo _CORE::addCss($a,"style.css"); ?>

<link href="<?php echo $a['TEMPLATE_DIR_WWW'];?>css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $a['TEMPLATE_DIR_WWW'];?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" id="camera-css"  href="<?php echo $a['TEMPLATE_DIR_WWW'];?>css/camera.css" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo $a['TEMPLATE_DIR_WWW'];?>css/skins/tango/skin.css" />
<link rel="stylesheet" href="<?php echo $a['TEMPLATE_DIR_WWW'];?>css/flexslider.css">
<link rel="stylesheet" href="<?php echo $a['TEMPLATE_DIR_WWW'];?>css/font.css">



    
<link rel="shortcut icon" href="<?php echo $a['TEMPLATE_DIR_WWW'];?>favicon.ico" type="image/x-icon" /> 
<link rel="icon" type="image/gif" href="<?php echo $a['TEMPLATE_DIR_WWW'];?>animated_favicon1.gif" >
    
<script src="<?php echo rootWWW; ?>js/toggle_vis.js" type="text/javascript"></script>
<script src="<?php echo rootWWW; ?>js/tooltip.js" type="text/javascript"></script> 

<script>
window.onload=function() {  
setTimeout("toggleVisibility('flashMsg','hide');",5000);
   }
</script>

<script type="text/javascript" src="<?php echo rootWWW; ?>js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo rootWWW; ?>js/hoverIntent.js"></script>
	<script type="text/javascript" src="<?php echo rootWWW; ?>js/superfish.js"></script>
	<script type="text/javascript" src="<?php echo rootWWW; ?>js/jquery.mobile.customized.min.js"></script>
    <script type="text/javascript" src="<?php echo rootWWW; ?>js/jquery.easing.1.3.js"></script> 
    <script type="text/javascript" src="<?php echo rootWWW; ?>js/camera.js"></script> 
	<script type="text/javascript" src="<?php echo rootWWW; ?>js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="<?php echo rootWWW; ?>js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo rootWWW; ?>js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="<?php echo rootWWW; ?>js/greyScale.js"></script>
    <script type="text/javascript" src="<?php echo rootWWW; ?>js/jquery.tweet.js"></script>
   	<script type="text/javascript" src="<?php echo rootWWW; ?>js/myscript.js"></script>

<?php echo $a['METATAG']; ?>
<?php echo $a['JS']; ?>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39149523-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<?php 
//$a=_CORE::printAlert($a);
//$a=_CORE::showInfo($a);
?>