<body>

<?php
  if (htmlHelper::hasFlash('msg'))
  {
    echo '<div id="flashMsg" onclick="javascript:toggleVisibility(\'flashMsg\', \'hide\')">'.htmlHelper::getFlash('msg')."</div>"; // output the flash message
  }
  else if (htmlHelper::hasFlash('err'))
  {
    echo '<div id="flashMsg" class="err">'.htmlHelper::getFlash('err')."</div>"; // output the flash message
  }
?>

<div id="main">
<?php include(_CORE::addView("admin/_elements/menu")); ?>

    <?php if(isset($_SESSION['AUTH']['_aUser'])){  ?>
    <table>
    <tr>
    <td style="min-width: 210px;" valign="top">
        <div style="padding:20px;">

            <div style="background-color:#efefef;border-radius:5px;padding:3px;margin:0 2px 5px 2px;">
            <table>
                <tr style="font-size:10px;">
                    <td valign="top"><i>Utente</i>:</td>
                    <td valign="top" align="center">
                        <b><a style="color:green;" href="<?php echo rootWWW."admin/tab/" . $TAB_a_utenti . "/upd/".$_SESSION['AUTH']['_aUser']['id']."/"?>">
                            <?php echo $_SESSION['AUTH']['_aUser']['nome']; ?>
                            <?php echo $_SESSION['AUTH']['_aUser']['cognome']; ?>
                        </a></b>
                    </td>
                </tr>
            </table>
            </div>

            <?php if(function_exists("getLanguages") ){ ?>
                <?php $languages = getLanguages(); $totLanguages = count($languages);?>
                <?php
                    //print_r($languages);
                //pr($_SESSION['AUTH']['_aUser']['paese']);
                ?>

                <?php if($totLanguages>1 && ($_SESSION['AUTH']['_aUser']['paese']=="" || $_SESSION['AUTH']['_aUser']['paese']=="_all_" || $_SESSION['AUTH']['_aUser']['paese']=="Italia")){ ?>
                <div style="padding:5px 5px 15px 5px;">
                    <div style="text-align:center;">
                        <b style="font-size:11px;">Seleziona Lingua</b>
                        <?php //pr($_SESSION['AUTH']['_aUser']);?>
                    </div>
                    <select style="height:35px;padding:5px;width:100%;" id="setLanguage" name="setLanguage" onchange="if(this.options[this.selectedIndex].value!='')window.location.href='<?php echo rootWWW."admin/"?>?'+this.name+'='+this.options[this.selectedIndex].value;">
                    <?php foreach ($languages AS $k=>$v) {?>
                        <option value="<?php echo $k; ?>"<?php
                            if(key($_SESSION['Language']) == $k)
                                echo ' selected';
                        ?>><?php echo $v; ?></option>
                    <?php } ?>
                    </select>
                </div>
                <?php } ?>
            <?php } ?>

            <a href="<?php echo rootWWW."admin/tab/" . $TAB_Menus . "/"?>" class="btn<?php if($a['GET_DATA']['tab'] == $TAB_Menus){echo' sel';} ?>">
                Menu
            </a>



            <?php
            //$_SESSION['Language']
            //$home = dbAction::_record(array('tab'=>$TAB_Element,'field'=>'slug','value'=>'home'));
            $home = dbAction::_record(array('tab'=>$TAB_Element,'set_w'=>"WHERE slug='home' AND lang='".key($_SESSION['Language'])."'"));
            ?>
            <?php if($home){

                $selHome = FALSE;
                if($a['GET_DATA']['tab'] == $TAB_Element && $a['GET_DATA']['upd']==$home['id']){ $selHome = TRUE; }
                ?>
            <a href="<?php echo rootWWW."admin/tab/" . $TAB_Element . "/upd/".$home['id']."/"?>" class="btn<?php if($selHome){echo' sel';} ?>">
                <span style="">Home</span>
            </a>
            <br>
            <?php }?>

            <?php
            $send = dbAction::_record(array('tab'=>$TAB_Element,'set_w'=>"WHERE slug='send' AND lang='".key($_SESSION['Language'])."'"));
            ?>
            <?php if($send){

                $selSend = FALSE;
                if($a['GET_DATA']['tab'] == $TAB_Element && $a['GET_DATA']['upd']==$send['id']){ $selSend = TRUE; }
                ?>
            <a href="<?php echo rootWWW."admin/tab/" . $TAB_Element . "/upd/".$send['id']."/"?>" class="btn<?php if($selSend){echo' sel';} ?>">
                <span style="">Send</span>
            </a>
            <br>
            <?php }?>


            <a href="<?php echo rootWWW."admin/content_page/"?>" class="btn<?php if(isset($a['GET_DATA']['content_page'])){echo' sel';} ?>">
                Contenuti
            </a>
            <a href="<?php echo rootWWW."admin/tab/" . $TAB_Area . "/"?>" class="btn<?php if($a['GET_DATA']['tab'] == $TAB_Area){echo' sel';} ?>">
                Aree
            </a>
            <a href="<?php echo rootWWW."admin/tab/" . $TAB_Category . "/"?>" class="btn<?php if($a['GET_DATA']['tab'] == $TAB_Category){echo' sel';} ?>">
                Categorie
            </a>
            <a href="<?php echo rootWWW."admin/tab/" . $TAB_Element . "/"?>" class="btn<?php if($a['GET_DATA']['tab'] == $TAB_Element && $a['R'][$a['USE_TAB']]['slug']!='home'){echo' sel';} ?>">
                Elementi
            </a>

        <?php if($selHome){?>
            <?php
            $ft=dbAction::_loop(array('tab'=>$TAB_Featuredstype));
            //pr($ft);
            foreach ($ft as $F){
                $fl=dbAction::_loop(array('tab'=>$TAB_Featured,"where"=>"WHERE id_Featuredstype='".$F['id']."'"));
                //pr($fl);
                if(empty($fl['id_area']) && empty($fl['id_category']) && empty($fl['id_element']) )
                    $ft2[]=$F;
            }

            ?>
            <?php if($ft2){?>
            <br><br>
            <div style="background-color:#efefef;border-radius:5px;padding:3px;margin:0 2px 5px 2px;font-size:11px;text-align: center">
                Home Featured
            </div>

                <?php
                if($featureView['home'] && is_array($featureView['home']['default']['featureds'])){
                    //pr($featureView['home']['default']['featureds']);
                    foreach ($featureView['home']['default']['featureds'] AS $FtK=>$FtV)
                        $FtToShow[]=$FtK;
                    $ft2_m = $ft2;
                    $ft2=array();
                    foreach ($ft2_m AS $F)
                    {
                        if(in_array($F['slug'], $FtToShow))
                        $ft2[]=$F;
                    }
                } 

                ?>

                <?php foreach ($ft2 AS $F){?>

                <a class="btn blue" href="<?php echo rootWWW."admin/tab/".$TAB_Featured."/"; ?>?setS_id_Featuredstype=<?php echo $F['id']; ?>">
                    <?php echo $F['text'];?>
                </a>
                <?php } ?>
            <?php } ?>

        <?php } ?>


        <?php if(
                $a['GET_DATA']['tab'] == $TAB_Element
                && isset($a['GET_DATA']['upd'])
                && $a['R'][$a['USE_TAB']]['slug']!='home'
                && !empty($a['R'][$TAB_Element])
                ){ ?>
            <br><br>
        <a class="btn blue" href="<?php echo rootWWW."admin/tab/".$TAB_Images."/"; ?>?setS_id_Element=<?php echo $a['GET_DATA']['upd']; ?>">
            Immagini
        </a>

        <a class="btn blue" href="<?php echo rootWWW."admin/tab/".$TAB_Attachment."/"; ?>?setS_id_Element=<?php echo $a['GET_DATA']['upd']; ?>">
            Allegati
        </a>

        <a class="btn blue" href="<?php echo rootWWW."admin/tab/".$TAB_Links."/"; ?>?setS_id_Element=<?php echo $a['GET_DATA']['upd']; ?>">
            Links
        </a>
        <a class="btn blue" href="<?php echo rootWWW."admin/tab/".$TAB_Featured."/"; ?>?setS_id_Element=<?php echo $a['GET_DATA']['upd']; ?>">
            Features
        </a>
        <?php } ?>



        <?php if($_SESSION['AUTH']['_aUser']['id_a_utenti_cat']=='2'){ ?>
        <br>
        <a class="btn blue" href="<?php echo rootWWW."admin/ftp_data/"; ?>">
            Dati FTP
        </a>
        <?php } ?>

        </div>
    </td>
    <td style="min-width: 210px;width: 100%;" valign="top">
        <?php include($a['VIEW']); ?>
    </td>
    </tr>
    </table>
    <?php } else { ?>
        <?php include($a['VIEW']); ?>
    <?php }  ?>


</div>

    <div style="text-align: center;padding:9px;font-size:11px;">
        <?php echo $a['DB_CONFIG']['n']; ?>
        -
        <?php echo $a['DB_CONFIG']['h']; ?>
    </div>


</body>
