<body>

<?php
  if (htmlHelper::hasFlash('msg')) 
  { 
    echo '<div id="flashMsg" onclick="javascript:toggleVisibility(\'flashMsg\', \'hide\')">'.htmlHelper::getFlash('msg')."</div>"; // output the flash message 
  }
  else if (htmlHelper::hasFlash('err')) 
  { 
    echo '<div id="flashMsg" class="err">'.htmlHelper::getFlash('err')."</div>"; // output the flash message 
  } 
?>

<div id="main">  
<?php include(_CORE::addView("admin/_elements/menu")); ?>

  <?php include($a['VIEW']); ?>
</div>

    <div style="text-align: center;padding:9px;font-size:11px;">
        <?php echo $a['DB_CONFIG']['n']; ?>
        -
        <?php echo $a['DB_CONFIG']['h']; ?>
    </div>
    
</body>
