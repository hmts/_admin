<?php

ini_set('post_max_size', '10M');
ini_set('upload_max_filesize', '10M');


//_models/_all.php
class mod_admin extends modAll
{
var $includeCoreComponents=array("phpMailerEasy/class.phpmailer.php");
var $includeComponents=array("url_functions.php","fieldsObj.php");



    function _beforeInit($a)
    {
        global $language,$TAB_Element;


        $this->copy_tab_records($a);


        if(function_exists("getLanguages"))
        {
            $languages = getLanguages();
            //print_r($languages);
            if(!isset($_SESSION['Language']))
            {
                foreach ($languages AS $k=>$v)
                {
                    if(!isset($_SESSION['Language']))
                        $_SESSION['Language'] = array($k=>$v);
                    }
            }
            if(!isset($_SESSION['Language']))
            {
                $_SESSION['Language'] = array($language=>$language);
            }

                //pr($_SESSION['Language']);


            if($_GET['setLanguage'])
            {

                $_SESSION['Language'] = array(
                    $_GET['setLanguage'] => $languages[$_GET['setLanguage']]
                );
                //pr($_SESSION['Language']);
                _CORE::redirect(array('location'=>rootWWW.$a['__URL']));
                //pr($a);exit;
            }



        }
        else
        {
            if(!isset($_SESSION['Language']))
            {
                $_SESSION['Language'] = array($language=>$language);
            }
        }


        $a=parent::_beforeInit($a);
        $this->initFilters($a);
        $a['USE_TAB']="".$_GET['tab'];
        $a['title']="ADMIN";
        //Init Admin Menu

        $DB_CONFIG=unserialize(base64_decode(DB_CONFIG));
        $a['DB_CONFIG']=$DB_CONFIG;
        //pr($a);
        //if($a['totPath']=="0"){
        if($a['PATH'][1]=='content_page'){
            $a=$this->_home($a);
        }


        /*
         * Check if home record
         * is created and
         * if not create it!
        */
        //print_r(key($_SESSION['Language']));
        if(isset($_SESSION['Language']) && !empty($_SESSION['Language'])){
            $this->setDefaultElements();
        }

        return $a;
    }

    function init($a)
    {
        header('Content-Type: text/html; charset=UTF-8');

        global $optional;
        //pr($optional);
        //pr(array_keys($optional));
        return $a;
    }

    function _afterInit($a)
    {
        return $a;
    }



    /*
    function _update($a,$settings=null)
    {
        $a=parent::_update($a,$settings);
        return $a;
    }
    */



    function _home($a)
    {
        global $TAB_Element;
        global $TAB_Category;
        global $TAB_Area;
        global $TAB_Tags;

        //pr(a);

        $homeSection=array(
            array("rn"=>"R1",'tit'=>"Area","tab"=>$TAB_Area),
            array("rn"=>"R2",'tit'=>"Categorie","tab"=>$TAB_Category),
            array("rn"=>"R3",'tit'=>"Elementi","tab"=>$TAB_Element)
        );

        foreach ($homeSection as $hS)
        {
        $Rn=$hS['rn'];
        $a['TIT'][$Rn]=$hS['tit'];
        $a['TAB'][$Rn]=$hS['tab'];
        $LoopCount=dbAction::_loop(array('tab'=>$a['TAB'][$Rn]));
            $totRecord=count($LoopCount);
            if($LoopCount)
            {
                if($totRecord>1)
                    $a['TXT'][$Rn]="ci sono <b>".$totRecord." voci</b> in questo Archivio";
                else
                    $a['TXT'][$Rn]="<b>una voce</b> in questo Archivio";
            }
            else
            {
                $a['TXT'][$Rn]="<b>non</b> ci sono voci in questo Archivio";
            }
        $a['LOOP'][$Rn]=dbAction::_loop(array('tab'=>$a['TAB'][$Rn],'orderby'=>"ORDER BY weight ",'limit'=>"LIMIT 0,3"));

        }

        /*
        $Rn="R1";
        $a['TIT'][$Rn]="Area";
        $a['TAB'][$Rn]=$TAB_Area;
        $L1=dbAction::_loop(array('tab'=>$a['TAB'][$Rn]));
        $a['TXT'][$Rn]="ci sono ".count($L1)." voci in Archivio";
        $a['LOOP'][$Rn]=dbAction::_loop(array('tab'=>$a['TAB'][$Rn],'orderby'=>"ORDER BY weight ",'limit'=>"LIMIT 0,3"));

         * $Rn="R2";
        $a['TIT'][$Rn]="Categorie";
        $a['TAB'][$Rn]=$TAB_Category;
        $L1=dbAction::_loop(array('tab'=>$a['TAB'][$Rn]));
        $a['TXT'][$Rn]="ci sono ".count($L1)." voci in Archivio";
        $a['LOOP'][$Rn]=dbAction::_loop(array('tab'=>$a['TAB'][$Rn],'orderby'=>"ORDER BY weight ",'limit'=>"LIMIT 0,3"));

        $Rn="R3";
        $a['TIT'][$Rn]="Elementi";
        $a['TAB'][$Rn]=$TAB_Element;
        $L1=dbAction::_loop(array('tab'=>$a['TAB'][$Rn]));
        $a['TXT'][$Rn]="ci sono ".count($L1)." voci in Archivio";
        $a['LOOP'][$Rn]=dbAction::_loop(array('tab'=>$a['TAB'][$Rn],'orderby'=>"ORDER BY weight ",'limit'=>"LIMIT 0,3"));
        */

        return $a;
    }
    function initFilters($a)
    {
        return $a;
    }




    //
    function setDefaultElements(){
      $langSuff = key($_SESSION['Language']);

      global $defaultElementsToCreate,$TAB_Element;

      if(!isset($defaultElementsToCreate) || empty($defaultElementsToCreate)){
        $defaultElementsToCreate = array('home','cookie','privacy-policy');
      }

      //pr($defaultElementsToCreate);

      foreach ($defaultElementsToCreate as $key => $value) {

        $slug = $value;
        $name = ucfirst(str_replace("-"," ",$value));


        $isaccessible = 0;
        if($slug!="home")
          $isaccessible = 1;

        $homeElementRecordExists = dbAction::_record(array(
            'tab'=>$TAB_Element,
            'set_w'=>"WHERE lang='".$langSuff."' AND slug='" . $slug . "'",
            //'echo'=>TRUE
            ));
        if(!$homeElementRecordExists){
            $insert = dbAction::_insert(array(
                //'echo'=>TRUE,
                'tab'=>$TAB_Element,
                'data'=>array(
                    'id_parent'=>NULL,
                    'title'=>NULL,
                    'slug'=>$slug,
                    'view'=>'default',
                    'description'=>NULL,
                    'keywords'=>NULL,
                    'h1'=>$name,
                    'h2'=>NULL,
                    'text'=>NULL,
                    'abstract'=>NULL,
                    'weight'=>NULL,
                    'listed'=>'0',
                    'isaccessible'=>$isaccessible,
                    'redirect'=>NULL,
                    'lang'=>$langSuff,
                    'creation'=>time(),
                    'modifyon'=>time(),
                    'price'=>'0.00',
                    'qty'=>'0',
                    'grams'=>'0',
                    'stockid'=>'0',
                    'inhome'=>'0'
                    )
            ));
        }
      }

    }


    function copy_tab_records($a){
      global $TAB_Featured;
      global $TAB_Element;
      global $TAB_Menus;



      if(isset($a['POST_DATA']['copy_tab_records']) && isset($a['POST_DATA']['lang'])){


        //Feature
        if($TAB_Featured == $a['POST_DATA']['copy_tab_records']){
          //Copy feature from lang if exists
          $loop = dbAction::_loop(array('tab'=>$TAB_Featured,
            'where'=>"WHERE lang='".$a['POST_DATA']['lang']."' AND id_area IS NULL AND id_category IS NULL AND id_element IS NULL "
        ));


          foreach ($loop as $I) {
            unset($I['id']);
            $I['lang']=key($_SESSION['Language']);
            dbAction::_insert(array('tab'=>$TAB_Featured,'data'=>$I));
          }
          _CORE::redirect(array('location'=>rootWWW.$a['__URL']));
        }


        //Element
        if($TAB_Element == $a['POST_DATA']['copy_tab_records']){
          //Copy feature from lang if exists
          $loop = dbAction::_loop(array('tab'=>$TAB_Element,
            'where'=>"WHERE lang='".$a['POST_DATA']['lang']."' AND id_parent IS NULL "
        ));


          foreach ($loop as $I) {
            unset($I['id']);
            $I['lang']=key($_SESSION['Language']);
            dbAction::_insert(array('tab'=>$TAB_Element,'data'=>$I));
          }
          _CORE::redirect(array('location'=>rootWWW.$a['__URL']));
        }


        //Menu
        if($TAB_Menus == $a['POST_DATA']['copy_tab_records']){
          //Copy feature from lang if exists
          $loop = dbAction::_loop(array('tab'=>$TAB_Menus,
            'where'=>"WHERE lang='".$a['POST_DATA']['lang']."' AND id_menus IS NULL "
        ));


          foreach ($loop as $I) {
            unset($I['id']);
            $I['lang']=key($_SESSION['Language']);
            dbAction::_insert(array('tab'=>$TAB_Menus,'data'=>$I));
          }
          _CORE::redirect(array('location'=>rootWWW.$a['__URL']));
        }


      }
    }
}
?>
