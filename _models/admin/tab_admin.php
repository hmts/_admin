<?php
//_models/_all.php
class mod_tab_admin extends mod_admin
{

    function _beforeInit($a)
    {
            
            
        $a=parent::_beforeInit($a);
            
            
        //pr($a);
        $a['url_mod']=rootWWW."".$a['route']['model']."/tab/".$a['GET_DATA']['tab']."/";
            
            
            $a['BEHAVIOR']="admin_".$a['GET_DATA']['tab'];
            
            
        /*
        Set $_SESSION['FILTER'][$TAB_prodotti][$TAB_insiemi]['value']
        
        */
        //pr($_SESSION['FILTERS'][$TAB_prodotti][$TAB_insiemi]);
        
        //pr($a['INSIEME']);
        return $a;
    }

    function init($a)
    {
        $a=parent::init($a);
        return $a;
    }
    
    function _afterInit($a)
    {
        $a=parent::_afterInit($a);
        //$a['url_mod']=rootWWW."".$a['route']['model']."/tab/".$a['GET_DATA']['tab']."/";
        $a=bulkAction($a); //Action Located in _config/_models/admin_config.php
        $a=filterSpecial_SET($a); //Action Located in _config/_models/admin_config.php
        
        if(isset($_SESSION['FILTERS'][$a['USE_TAB']][$TAB_insiemi]['value']))
        {
            $a['INSIEME']['id']=$_SESSION['FILTERS'][$a['USE_TAB']][$TAB_insiemi]['value'];
            $a['INSIEME']['W']="WHERE id_insiemi='".$a['INSIEME']['id']."'";
            $a['INSIEME']['AND']="AND id_insiemi='".$a['INSIEME']['id']."'";
            $a['INSIEME']['R']=dbAction::_record(array('tab'=>$TAB_insiemi,'value'=>$a['INSIEME']['id']));
            
            
        }
        return $a;
    }
    
    function _beforeSAVE($DATA,$a)
    {
        global $TAB_Area, $TAB_Tags, $TAB_Menus, $TAB_Element, $TAB_Extrafields, $TAB_a_utenti, $TAB_Featured;
        
        if(isset($DATA['title']) && empty($DATA['title']))
            $DATA['title']=$DATA['h1'];
        
        /*
         * All this default value are been added to develop safety in local
         */
        if(!isset($DATA['weight']) || empty($DATA['weight']))
            $DATA['weight']="0";
        if(!isset($DATA['listed']) || empty($DATA['listed']))
            $DATA['listed']="1";
        if(!isset($DATA['redirect']) || empty($DATA['redirect']))
            $DATA['redirect']="";
        if(!isset($DATA['price']) || empty($DATA['price']))
            $DATA['price']="0.00";
        if(!isset($DATA['qty']) || empty($DATA['qty']))
            $DATA['qty']="0";
        if(!isset($DATA['grams']) || empty($DATA['grams']))
            $DATA['grams']="0";
        if(!isset($DATA['stockid']) || empty($DATA['stockid']))
            $DATA['stockid']="";
        if(!isset($DATA['inhome']) || empty($DATA['inhome']))
            $DATA['inhome']="0";
        if(!isset($DATA['view']) || empty($DATA['view']))
            $DATA['view']="default";
        if(!isset($DATA['fileext']) || empty($DATA['fileext']))
            $DATA['fileext']="";
        if(!isset($DATA['password']) || empty($DATA['password']))
            $DATA['password']="";
        if(isset($DATA['weight']) && empty($DATA['weight']))
            $DATA['weight']="0";
        
        if( $a['USE_TAB'] == $TAB_Featured ){
            //pr($DATA);pr($_POST);pr($_FILES);exit;
            
        }
            
        
        
        if( $a['USE_TAB'] == $TAB_a_utenti )
        {
            if(!empty($DATA['password']))
                $DATA['password'] = md5 ($DATA['password']);
            if(empty($DATA['password']))
                $DATA['password'] = $DATA['password_save'];
            
        }
        //pr($DATA);exit;
        if( $a['USE_TAB'] == $TAB_Element )
        {
            if(!empty($DATA['extrafields']) && is_array($DATA['extrafields']) )
            {
                foreach ($DATA['extrafields'] AS $k=>$v)
                {
                    $exists = dbAction::_record(array('tab'=>$TAB_Extrafields,'set_w'=>"WHERE id_element='".$DATA['id']."' AND slug='".$k."'"));
                    if($exists)
                        dbAction::_update(array('tab'=>$TAB_Extrafields,'id'=>$exists['id'],'data'=>array('slug'=>$k,'value'=>$v,'id_element'=>$DATA['id'])));
                    else
                        dbAction::_insert(array('tab'=>$TAB_Extrafields,'data'=>array('slug'=>$k,'value'=>$v,'id_element'=>$DATA['id'])));
                }
                
                
            }
            
            //exit;
            
        }
        
        
        
        
        if($a['USE_TAB']==$TAB_Menus)
        {
            if($DATA['id_menus']=='0'){
                $DATA['id_menus']=NULL;
            }
            
        }
        if($a['USE_TAB']==$TAB_Area)
        {
            
        }
        
        if(isset($DATA['slug']) && empty($DATA['slug']))
        {
            $DATA['slug']=slug($DATA['h1']);
        }
        elseif(!empty($DATA['slug']))
        {
            $DATA['slug']=slug($DATA['slug']);
        }
        
        
        if($a['USE_TAB']==$TAB_Tags)
        {
            if(isset($DATA['slug']) && empty($DATA['slug']))
            {
                $DATA['slug']=slug($DATA['text']);
            }
            elseif(!empty($DATA['slug']))
            {
                $DATA['slug']=slug($DATA['slug']);
            }
        }
        
        
        if($DATA['id_area']=='0' || $DATA['id_area']=='')
        {
            $DATA['id_area']=NULL;
        }
        if($DATA['id_category']=='0' || $DATA['id_category']=='')
        {
            $DATA['id_category']=NULL;
        }
        if($DATA['id_element']=='0' || $DATA['id_element']=='')
        {
            $DATA['id_element']=NULL;
        }
        if($DATA['id_parent']=='0' || $DATA['id_parent']=='')
        {
            $DATA['id_parent']=NULL;
        }
        //createdon
        //modifyon
        if( isset($DATA['NEW']) ){$DATA['createdon']=time();}
        if( !isset($DATA['modifyon']) || empty($DATA['modifyon']) ){$DATA['modifyon']=time();}
        
        
        return $DATA;
    }

    
    function _afterSAVE($a)
    {    
    //$a['ID_SAVED']
        global $TAB_Area;
            //pr($TAB_video);
            
        
        return $a;
    }
    /*
        !!! 
        OverWrite the _record method in initModel.php
        
    */
    function _record($a,$settings=null)
    {
        //$settings['add_w'];
        //$settings['set_w'];
        //$settings['echo'];
            if(empty($settings['ID']))
            {
                $settings['ID']=$a['GET_DATA']['upd'];
            }
        
            
        $CAMPI_RECORD=campiTab($a['USE_TAB']);
        //pr($LOOP_CAMPI);
        if(isset($_SESSION['Language']) && in_array("lang", $CAMPI_RECORD))
        {
            //pr($a['where']);
            if(empty($settings['set_w']))
            {
                $settings['add_w']=$settings['add_w']." AND lang='".key($_SESSION['Language'])."' ";
            }
            else
            {
                $settings['set_w']=$settings['set_w']." AND lang='".key($_SESSION['Language'])."' ";
            }
        }
	    
            
            
            
        $settings['no_default_db_files']=true;
            
        $a=$this->_record_Exec($a,$settings);
        return $a;
    }

    
    /*
        !!! 
        OverWrite the _loop method in initModel.php
        
    */
    function _loop($a)
    {
        global $TAB_autori;
        global $TAB_insiemi;
        global $TAB_prodotti;
        global $TAB_editori;
        global $TAB_ordini;
        global $TAB_utenti;
        global $TAB_a_utenti;
        global $TAB_Element;
            
        //Set $settings[]
        $settings['url_pagination']=$a['url_mod'];
        $settings['records_for_pag']="10";
        $settings['current_page']="1";
        
        $settings['pagSuff']="p";
        $settings['query_recover']=array('q','t','id_record'); //AUTOMATIC PUT NO FOLLOW IF IS SET in pagination
        $settings['orderby']=" ORDER BY nome, slug, id DESC";
        $settings['no_default_db_files']=true;
        $settings['echo']="0";
            
        
        $LOOP_CAMPI=campiTab($a['USE_TAB']);
         
        if(isset($_SESSION['Language']) && in_array("lang", $LOOP_CAMPI))
        {
            //pr($settings['where']);
            if(empty($settings['where']))
            {
                $settings['where']=" WHERE lang='".key($_SESSION['Language'])."'";
            }
            else
            {
                $settings['where']=$settings['where']." AND lang='".key($_SESSION['Language'])."' ";
                if($a['tab']=="Element"){
                    //pr($settings['where']);
                    
                }
            }
        }
        
        //pr($settings['where']);
        
        //pr($settings['where']);
        
        $settings=tableSettings($a,$settings); //Action Located in _config/_models/admin_config.php    
            
        $settings=filterSpecial_CHECK($a,$settings); //Action Located in _config/_models/admin_config.php    
            
            /*
                
                Set a personalized WHERE...
                
                
                I M P O R T A N T   U P D A T E   H E R E 
                *********************************************
                *                                           *
                *  This scipt can be added in a function    *
                *  and this function can take advantage of  *
                *  relations db array for make the query    *
                *                                           *
                *********************************************
                
                
            */
            //$SAVE_SESSION=$_SESSION['FILTERS'][$TAB_prodotti][$TAB_insiemi];
            //unset($_SESSION['FILTERS'][$TAB_prodotti][$TAB_insiemi]);
                
                /* CHECK FILTER EXCEPT insiemi*/
                $aF=$_SESSION['FILTERS'];
                //pr($aF);
                
            
            
                
            if($a['POST_DATA']['q']!="")
            {
                $Q=$a['POST_DATA']['q'];
                $W=" (h1 LIKE '%".$Q."%' OR h2 LIKE '%".$Q."%'  OR text LIKE '%".$Q."%') ";
                if(empty($settings['where']))
                    $W="WHERE ".$W;
                else
                    $W=$settings['where']." AND ".$W;
                $settings['where']=$W;
            }
            //pr($settings['where']);
            
            
            
            //pr($a['USE_TAB']);
            if($a['USE_TAB']==$TAB_a_utenti)
            {
                $W = " id_a_utenti_cat='1' ";
                if(empty($settings['where']))
                    $W="WHERE ".$W;
                else
                    $W=$settings['where']." AND ".$W;
                $settings['where']=$W;
            }
            
            //pr($a['USE_TAB']);
            if($a['USE_TAB']==$TAB_Element)
            {
                $W = " slug!='home' ";
                if(empty($settings['where']))
                    $W="WHERE ".$W;
                else
                    $W=$settings['where']." AND ".$W;
                $settings['where']=$W;
            }
            
            
            
        /*
            Calling a method in initModel.php
        */
        $a=$this->_loop_Exec($a,$settings);
        return $a;
    } //End f _loop
        
        
        
        
        
    function _insert($a,$settings=null)
    {
        /*
            Setting value for _insert
        */
        //pr($a['POST_DATA']);
        $a['NEW']['data']=$this->_beforeSAVE($a['POST_DATA'],$a);
        
        $a['NEW']['echo']="0";
        $a['NEW']['MSG_INSERT_OK']="Record Creato!";
        $a['NEW']['MSG_INSERT_OFF']="Errore nella creazione del nuovo record...";
        $MAKE_SLUG_FROM=_defineArray(MAKE_SLUG_FROM); //Defined in /_config/_all.php
        //pr($MAKE_SLUG_FROM);
        $a['NEW']['make_slug_from']=$MAKE_SLUG_FROM[$a['USE_TAB']];
            
            
        $a['NEW']['URL_REDIRECT']=rootWWW."".$a['route']['model']."/tab/".$a['GET_DATA']['tab']."/";;
            
            /*
                STOP REDIRECT &
                add sitemap function
                if _insert_Exec return TURE (stopping redirect the function retur true or false)
                on var: $a[ACTION_NAME]['RESULT']
            */
            //$a['NEW']['STOP_REDIRECT']=true;
            //pr($a['NEW']['data']);pr(campiTab($a['USE_TAB'],1));exit;
            
            
        /*
            Set defauklt settings
        */
        $settings=array(array('action'=>'NEW','tab'=>$a['USE_TAB']));
        $a=$this->_insert_Exec($a,$settings);
            /*
            if($a['NEW']['RESULT'])
            {
                //UPDATE SITEMAP
                SITEMAP::_update(array('tab'=>$a['USE_TAB']),'id'=>$a['NEW']['RESULT']);
            }
            */
    return $a;   
    }
        
        
        
        
    function _update($a,$settings=null)
    {
        global $TAB_libri_cat;
        
            
            
        /*
            Setting value for _update_Exec
        */
        $a['UPD']['id']=$a['POST_DATA']['id'];
        $a['UPD']['data']=$this->_beforeSAVE($a['POST_DATA'],$a);
        $a['UPD']['where']="";
        $a['UPD']['echo']="0";
            
        //$a['UPD']['make_slug_from']="titolo";
        $MAKE_SLUG_FROM=_defineArray(MAKE_SLUG_FROM);
        $a['UPD']['make_slug_from']=$MAKE_SLUG_FROM[$a['USE_TAB']];
        
        $a['UPD']['MSG_UPDATE_OK']="Dati Salvati!";
        $a['UPD']['MSG_UPDATE_OFF']="Errore nel salvataggio dei dati.";
            
        $a['UPD']['URL_REDIRECT']=rootWWW.$a['route']['model']."/tab/".$a['USE_TAB']."/upd/".$a['UPD']['id']."/";
            
            /*
                STOP REDIRECT &
                add sitemap function
                if _update_Exec return TURE (stopping redirect the function retur true or false)
                on var: $a[ACTION_NAME]['RESULT']
            */
            //$a['UPD']['STOP_REDIRECT']=true;
            

        //DEFAULT SETTINGS!
        $settings=array(array('action'=>'UPD','tab'=>$a['USE_TAB'],'MSG_UPDATE_OK'=>$MSG_UPDATE_OK,'MSG_UPDATE_OFF'=>$MSG_UPDATE_OFF));
            
        $a=$this->_update_Exec($a,$settings);
            /*
            if($a['UPD']['RESULT'])
            {
                //UPDATE SITEMAP
                SITEMAP::_update($a,array('tab'=>$a['USE_TAB']),'id'=>$a['UPD']['RESULT']);
            }
            */
        return $a;
    } //End f _update
    
    
    function _delete($a,$settings=null)
    {
        //pr($a);
        $a['DEL']['id']=$a['GET_DATA']['del'];
        //$a['DEL']['where']="";
        $a['DEL']['echo']=FALSE;
        $a['DEL']['msg_ok']="Record Cancellato!";
        $a['DEL']['msg_err']="Record Non Cancellato! <br>".$_SESSION['mysqlError'];
        $a['DEL']['URL_REDIRECT']=$_SERVER["HTTP_REFERER"];
        
        $a=$this->_delete_Exec($a,$settings);
            
            
            
        return $a;
    } //End f _delete

}
?>