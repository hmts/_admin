<?php
//_models/_all.php
class modAll extends initModel
{
var $includeCoreComponents=array("phpMailerEasy/class.phpmailer.php");
var $includeComponents=array('Save_Form_From_front_to_office.php'); 

    function _beforeInit($a)
    {
        global $TAB_utenti;
        global $TAB_utenti_cat;
            
        header('Content-Type: text/html; charset=UTF-8');
        //global $language;
        //if(!isset($_SESSION['LANG'])){$_SESSION['LANG']=$language;}
        $_SESSION['LANG']="en";
        
        if(substr($a['PATH'][0],-2)=="it")
        {
        $_SESSION['LANG']="it";
        }
        elseif(substr($a['PATH'][0],-2)=="es")
        {
        $_SESSION['LANG']="es";
        }
        
        
        
        if($a['POST_DATA']['send_form']=="1")
        {
            $a=$this->sendForm($a);
        }
        
        
        define("LANG",$_SESSION['LANG']);
        
        
        
        $a['_LANG_FILE_']=rootDOC."_config/".LANG.".php";
        
        
        
        $a=parent::_beforeInit($a);
        
        //add a default css
        $a['STYLE_FOR_SECTION']="pagine.css";
        
        //$a['USER']=AUTH::isLogin("TAB_utenti");
        
        
        //pr($a);
        
        /*
        if(isset($_GET['q']))
        {
        $a['METATAG']='<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">';
        }
        */
        
        
        $a['title']="admin";
        
            
            
            
            
          //
          //pr("AAAAAAAAA");  
          //pr($a['GET_DATA']['attiva_donazione']);
          //
          //
          //
        global $TAB_utenti;
        if(isset($a['GET_DATA']['attiva_donazione']) && !empty($a['GET_DATA']['attiva_donazione']))
        {
        $selU=dbAction::_record(array(
                'tab'=>$TAB_utenti,
                'value'=>$a['GET_DATA']['attiva_donazione'],
                'field'=>'email_codice_verifica',
                'echo'=>false,
                ));
        //pr($selU);
        if($selU)
        {
            if($selU['email_verificata']=="0"){
            htmlHelper::setFlash("msg",__("Donazione Verificata! Grazie!",false));
            $upd=dbAction::_update(array(
                'tab'=>$TAB_utenti,
                'id'=>$selU['id'],
                'only_this_fileds'=>array('email_verificata'),
                'data'=>array('email_verificata'=>'1'),
                'echo'=>false
                
            ));
            }else{
            htmlHelper::setFlash("msg",__("Hai già verificato la tua Donazione !",false));
            }
        }
        }
        
        
        return $a;
    }
        
        
        
    function Init($a)
    {            
        $a=parent::Init($a);
        return $a;
    }
    
    function splitMenu($M)
    {
        global $MENU_TYPE_ARRAY;
        
        foreach($MENU_TYPE_ARRAY AS $I)
        {
            foreach($M AS $I2){
            if($I2['menu']==$I)
            $M2[$I][]=$I2;
            }
        }
        return $M2;
    }
    function setMenu($menuArray)
    {
        global $TAB_menus;
        global $TAB_routes;
        global $TAB_pagine;
        global $TAB_tipologie;
        global $TAB_progetti;
        global $TAB_video;
        global $language;
        //global $lang;
            
        $tot_menuArray=count($menuArray);
            
            //pr("TOT MENU".$tot_menuArray);
            //pr($menuArray);
            
            if(LANG!=$language){$SUFF_LANG="-".LANG;}
          
          include(rootDOC."_config/".LANG.".php");
            
        for($i=0;$i<$tot_menuArray;$i++)
        {
            
            //$menuArray[$i]['nome']="".$menuArray[$i]['nome'];
                
            $m=$menuArray[$i];
                //pr($m);
                
            if(!empty($m['url']))
            {
                $URL=$m['url'];
                if(!empty($m['url_'.LANG])){$URL=$m['url_'.LANG];}
                $menuArray[$i]['URL']=$URL;
            }
            else if($m['id_routes']!='0')
            {
                
                $menuArray[$i]['URL']=rootWWW.$m['REL'][$TAB_routes]['slug']."/";
                //pr($m['REL'][$TAB_routes]);
            }
            else if($m['id_pagine']!='0')
            {
                
                $menuArray[$i]['URL']=rootWWW."pagine/".$m['REL'][$TAB_pagine]['slug']."/";
                //pr($m['REL'][$TAB_pagine]);
            }
            else if($m['id_tipologie']!='0')
            {
                $menuArray[$i]['URL']=rootWWW.$lang['URI']['tipologie']."/".$m['REL'][$TAB_tipologie]['id']."/";
                //pr($m['REL'][$TAB_pagine]);
            }
            else if($m['id_progetti']!='0')
            {
                $menuArray[$i]['URL']=rootWWW.$lang['URI']['progetti']."/".$m['REL'][$TAB_progetti]['id']."/";
                //pr($m['REL'][$TAB_pagine]);
            }
            /*
            else if($m['id_video']!='0')
            {
                $menuArray[$i]['URL']=rootWWW.$lang['URI']['video']."/".$m['REL'][$TAB_video]['id']."/";
                //pr($m['REL'][$TAB_pagine]);
            }
            */
            else
            {
                //pr("AAA".$m['id_routes']);
            }
            
            
            $menuArray[$i]['nome']="<span title=\"".$menuArray[$i]['URL']."\">".$menuArray[$i]['nome']."</span>";
        }
        return $menuArray;
    }





    function sendForm($a)
    {
        global $REQ_A_dipendente;
        global $REQ_A_azienda;
        global $REQ_A_onp;
        global $REQ_A_contatti;
        
        
        
        
        //pr($a['POST_DATA']);
        //if($a['POST_DATA']['req_form']=="azienda")$A_required=$REQ_A_azienda;
        $NOME_REQ_VAR="REQ_A_".$a['POST_DATA']['req_form'];
        $A_required=$$NOME_REQ_VAR;
        
        
        
        //pr($A_required);
        foreach($A_required AS $E)
        {
            if(empty($a['POST_DATA'][$E]))
            {
                $a['ERR_FIELDS'][]=$E;
            }
        }
        
        //pr($A_required);exit;
        if(empty($a['ERR_FIELDS']))
        {
            //Salvo i dati
            
            Form_Front2Office::sava_data($a);
            
            
            
            //echo 'SEND EMAIL';
            
if(!empty($_POST))
{
foreach($_POST AS $k=>$v)
{
$msg.=$k.": ".$v."\n";
}
$to      = "segreteria.unora@gmail.com";
//$to      = "gabriele.marazzi@gmail.com";
$subject = "UNORA - MODULO ".time();
$message = $msg;
$headers = 'From: segreteria@unora.org' . "\r\n" .
    'Reply-To: segreteria@unora.org' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);

}
        
        htmlHelper::setFlash("msg",__("Dati inviati!",false));
        _CORE::redirect(array('location'=>rootWWW));
        }
        else
        {
            htmlHelper::setFlash("err",__("Compilare tutti i campi mancanti<br />segnalati in rosso",false));
        }
        //echo'a';exit;
        return $a;
    }
    
    
    
    
    
    function eco($var)
    {
        if(DEBUG){
            if(is_array($var))
            {
                $var='<pre style="border:1px #afafaf solid;border-bottom:5px #afafaf solid;background:#ffffcc;margin:5px;height:90px;overflow:auto;padding:5px;">'.
                print_r($var,TRUE)    
                .'</pre>';
            }
            echo $var;
        }
    }

}
?>
