<?php
//_models/_all.php
class mod_xml extends modAll
{
    /*
        
        This module is created
        mainly for create SITEMAP
        at fly
        
    */
    
    function _beforeInit($a)
    {
        $a=parent::_beforeInit($a);
        return $a;
    }


    function Init($a)
    {
        $a=parent::Init($a);
        return $a;
    }
    
    function _setSitemap($a)
    {
        //pr($a['SITEMAP_CONFIG']);
        $a=$this->_createSitemap($a,$a['SITEMAP']['name']);
        return $a;
    }
    
    
    
    
    
    function _createSitemap($a,$sitemap_name)
    {
        $a['SITEMAP']['data']="";
        
        if($a['SITEMAP']['INDEX'])
        {
                
            //$a['VIEW']=rootDOC."_views/xml/sitemap/index.php";
            //pr($a['SITEMAP']['INDEX']);
            $a['SITEMAP']['INDEX']=array();
                
            $S_FILE ="<"."?"."xml version='1.0' encoding='UTF-8'"."?".">"."\n";
            $S_FILE.='<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
                
            for($i=0;$i<count($a['SITEMAP_CONFIG']['items']);$i++)
            {
                if(!$a['SITEMAP_CONFIG']['items'][$i]['index'])
                {
                //pr($a['SITEMAP_CONFIG']['items'][$i]['name']);
                $S_URL=rootWWW."xml/sitempas/".$a['SITEMAP_CONFIG']['items'][$i]['name'].".xml.gz";
                //pr($S_URL);
                    
                $a['SITEMAP']['INDEX'][]=$S_URL;
                    
                $S_FILE.='<sitemap><loc>'.$S_URL.'</loc></sitemap>'."\n";
                    
                }   
            }
                
            $S_FILE.='</sitemapindex>'."\n";
                
            //header('content-type: application/x-gzip');
            //header('Content-Disposition: attachment; filename="'.$sitemap_name.'.xml.gz"');
            //$gzdata = GZENCODE($S_FILE, 9);
            //$gzdata = GZENCODE(rootDOC."_files/collane.sql", 9); 
            //echo $S_FILE;
            header("Content-type: text/xml; charset=utf-8");
            header('Content-Disposition: attachment; filename="'.$sitemap_name.'.xml"');
            echo $S_FILE;
            //echo exec("gzip ".$S_FILE);  
            
        }
        else
        {
            echo'NEED SET FOR SIMPLE SITEMAP... '.__FILE__.' line: '.__LINE__.'';
        }
        return $a;
    }

}
?>