<?php
class _EMAIL_ALERT
{
    function _ascolta($a,$settings=null)
    {
        global $TAB_schedule_email;
        
        
        //$a['POST_DATA']['sendSheduleEmail']=$a['GET_DATA']['sendSheduleEmail'];//ForTestOnly
        
        if(is_natural_number($a['POST_DATA']['sendSheduleEmail']))
        {
            if(is_natural_number($a['USER']['id']) && ($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] || $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']))
            {
                
                $R=dbAction::_record(array(
                'tab'=>$TAB_schedule_email,
                'value'=>$a['POST_DATA']['sendSheduleEmail'],
                
                ));
                
                //Se lo schedule esiste
                //non importa che sia inviato o meno, lo invio:
                //usando _send_schedule($a,$settings=null)
                //$R
                
                _EMAIL_ALERT::_send_schedule($a,array('id'=>$R['id']));
                
                
            }
        }
        
        
        if(is_natural_number($a['POST_DATA']['delSheduleEmail']))
        {
            if(is_natural_number($a['USER']['id']) && ($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] || $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']))
            {
                
                $R=dbAction::_record(array(
                'tab'=>$TAB_schedule_email,
                'value'=>$a['POST_DATA']['delSheduleEmail'],
                
                ));
                
                //Se lo schedule esiste
                //lo cancello
                $delS=dbAction::_delete(array('tab'=>$TAB_schedule_email,'id'=>$R['id']));
                //
                
                
            }
        }
        
        return $a;
    }
    
    
    
    function _send($a,$settings)
    {
        global $SMTP_DATA;
        global $TAB_utenti;
        
        //richiede che il modulo per invio email sia stato già incluso
        
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
         *          
         *          A T T E N Z I O N E   ! ! ! 
         *          ---------------------------
         *          è necessario un controllo sulle variabili in $settings 
         *          per evitare errori...
         * 
         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         */
        
        
        $oggetto=$settings['og'];
        if(empty($oggetto)){
            $oggetto="[OFFICE] - ALERT EMAIL - UNORA.ORG ".time();
        }
        $txt=$settings['msg'];
        
        
        if(!empty($settings['destinatari']))
        {
            $destinatari=$settings['destinatari'];//DEVE ESSERE UN ARRAY
        }
        else
        {
        //destinatari presi dagli utenti con $settings['id_utenti_cat'];
        $destinatari=dbAction::_loop(array(
            'tab'=>$TAB_utenti,
            'where'=>"WHERE id_utenti_cat='".$settings['id_utenti_cat']."'",
            
            ));
        }
        
        
        
        if($destinatari)
        {
        $SETT=array(
                'SMTP_DATA'=>$SMTP_DATA,
                'destinatari'=>$destinatari,
                'tit'=>$oggetto,
                'txt'=>$txt,
                'template'=>rootDOC."_views/_email/default.html",
                //'allegati'=>false,
                );
        
            if(!empty($settings['allegati']))
            {
                   $SETT['allegati']=$settings['allegati'];
            }
        
                //pr($SETT);exit;
                //pr($SETT);
                $invio=_CORE::smtpEmail($SETT);
                return $invio;
        }
    }
    
    
    
    
    function _schedule($a,$settings)
    {
        global $TAB_schedule_email;
        global $SMTP_DATA;
            
        //ShortCut
        $S=$settings;
        
        //$S['codice_riferimento']        
            if(empty($S['to'])){if(DEBUG){echo'DATI MANCANTI [to]';}return $a;}
            //if(empty($S['from'])){if(DEBUG){echo'DATI MANCANTI';}return $a;}
            if(empty($S['oggetto'])){if(DEBUG){echo'DATI MANCANTI [oggetto]';}return $a;}
            if(empty($S['testo'])){if(DEBUG){echo'DATI MANCANTI [testo]';}return $a;}
            if(empty($S['codice_riferimento'])){if(DEBUG){echo'DATI MANCANTI [codice_riferimento]';}return $a;}
            if(empty($S['id_utenti'])){if(DEBUG){echo'DATI MANCANTI [id_utenti]';}return $a;}
            
            //Sistemo la var "TO" lo rendo json e se non è un'array lo rendo un array...
            //in questo modo posso passare in questa var sia un array che una smplie email singola
            $S['to']=is_array($S['to'])?json_encode($S['to']):json_encode(array($S['to']));
            //Set from default from $SMTP_DATA['mittente']
            $S['from']=empty($S['from'])?$SMTP_DATA['mittente']:$S['from'];
                    
            //se $S['dont_send_before'] è vuoto lo rendo zero
            $S['dont_send_before']=empty($S['dont_send_before'])?"0":$S['dont_send_before'];
            
        $DATA=array(
        'destinatari'=>$S['to'],
        'mittente'=>$S['from'],
        'oggetto'=>$S['oggetto'],
        'testo'=>$S['testo'],
        'allegato'=>$S['allegato'],
        'inviato'=>'0',
        'codice_riferimento'=>$S['codice_riferimento'],
        'id_utenti'=>$S['id_utenti'],
        'data_creazione'=>time(),
        'dont_send_before'=>$S['dont_send_before'],
        
        );
            
        //Prima verifico che non sia già stata inserita in schedule
        $Ex = dbAction::_record(array('tab'=>$TAB_schedule_email,'set_w'=>" WHERE codice_riferimento='".$S['codice_riferimento']."' "));
        if(!$Ex){
            //pr("inserisco _schedule");
        $INS=dbAction::_insert(array('tab'=>$TAB_schedule_email,'data'=>$DATA,'echo'=>true));
        }
        
    }
    
    
    
    function _send_schedule($a,$settings=null)
    {
        //Gli schedule vengono inviati solo da questa funzione 
        //per ora...
        
        global $TAB_schedule_email;
        
        if(!empty($settings['id']) || !empty($settings['where']))
        {
            //Se c'è uno schedule id prelevo solo quello  e creo il loop
            //anche se c'è uno where
            if(!empty($settings['id']))
            {
                $settings['SendAgain_and_dont_copy']=true;
                
                //Se non viene invocata la funzione SendAgain_and_dont_copy
                if(empty($settings['SendAgain_and_dont_copy']))
                {
                    //Allora effettuo una copia dello schdule e lo invio subito!
                    
                    //1. prendo i dati dello schedule
                    $sc=dbAction::_record(array('tab'=>$TAB_schedule_email,'value'=>$settings['id']));
                    //2. creo la copia: inserisco gli stessi dati 
                    //(non aggiorno neanche inviato tanto viene aggiornato durante l'invio)
                    $ins_sc=dbAction::_insert(array('tab'=>$TAB_schedule_email,'data'=>$sc));
                    //3. imposto l'id con il nuovo creato.
                    $settings['id']=$ins_sc;
                }
                $W="WHERE id='".$settings['id']."'";
            }
            else if(!empty($settings['where']))
            {
                $W=$settings['where'];
            }
        }
        else
        {
            //
            //Se non ci sono impostazioni
            //invio solo quelli shcedulati ma non inviati
            //con l'impostazione anche 'dont_send_before'
            //
            
            $W="WHERE inviato='0' AND dont_send_before<'".time()."'";
        }
        $schedule=dbAction::_loop(array(
        'tab'=>$TAB_schedule_email,
        'where'=>$W,
        ));
        
        
        
        
        if($schedule){
            //se ci sono $schedule
            
            $this->eco('<h1 style="color:GREEN">SCEHDULE</h1>');
            //pr($schedule);
            
            foreach ($schedule AS $I)
            {
                $S=array();
                $S=array(
                    'og'=>$I['oggetto']." ".time(),
                    'msg'=>$I['testo'],
                    'destinatari'=>  json_decode($I['destinatari'],true),
                    
                );
                    
                $allegato=rootDOC.$I['allegato'];
                //pr($allegato);
                    if(file_exists($allegato))
                    {
                        $S['allegati']=array($allegato);
                        $this->eco('con allegato: '.$allegato);
                    }
                    
                //$this->eco($I['destinatari']);
                $this->eco('<p><b>INVIO EMAIL</b> il: '.date("d-m-Y H:i:s",$I['data_creazione']).'; oggetto: '.$I['oggetto'].'</p>');
                $INVIO_EMAIL=_EMAIL_ALERT::_send($a,$S);
                
                //$this->eco('<b style="color:RED;">RISULTATO INVIO</b>');
                //pr($INVIO_EMAIL);
                
                //Aggiorno il campo inviato=time();
                $UPD=dbAction::_update(array(
                    'tab'=>$TAB_schedule_email,
                    'only_this_fields'=>array('inviato'),
                    'data'=>array('inviato'=>time()),
                    'id'=>$I['id'],
                ));
            }
        }
        
        
    }
}