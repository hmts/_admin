<?php
CLASS _BONIFICI
{
    function _add($a,$settings=null)
    {
        //
        //
        //
        global $TAB_bonifici;
        global $TAB_doc_ordini_pagamento;
        //
        //
        //
        //
        //
        $DIR_ALLEGATI=$settings['DIR_ALLEGATI'];
            if(empty($DIR_ALLEGATI)){$DIR_ALLEGATI="_files/bonifici/";}
        $TAB_PAGAMENTI=$settings['TAB_PAGAMENTI'];
            if(empty($TAB_PAGAMENTI)){$TAB_PAGAMENTI=$TAB_doc_ordini_pagamento;}
        $CAMPO_ID_PAGAMENTO=$settings['CAMPO_ID_PAGAMENTO'];
           if(empty($CAMPO_ID_PAGAMENTO)){$CAMPO_ID_PAGAMENTO="id_doc_ordini_pagamento";}
        // 
        // 
        // 
        //Verifico prima di iniziare:
        //
        /*
         * 1. che l'utente id sia valido
         * 2. che l'importo non sia vuoto
         * 3. che il file sia allegato (si può togliere se serve...)
         * 
         */
        if(is_natural_number($a['USER']['id']) && !empty($a['POST_DATA']['importo']) && !empty($_FILES))
        {
            //
            //Recupero i dati del documento di pagamento
            //  
                //Se si trata di un utente unora cerco solo in base all'ID
                //Se si tratta di un re_azienda allora specifico che può lavorare
                //solo nella sua azienda
            $S=array('tab'=>$TAB_PAGAMENTI,'value'=>$a['GET_DATA']['upd']);
            if($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['ref_azienda']){
            $S=array('tab'=>$TAB_PAGAMENTI,'value'=>$a['GET_DATA']['upd'],'add_w'=>"AND id_aziende='".$a['USER']['id_aziende']."'");
            }
            $doc_pagamento=dbAction::_record($S);
            //pr($doc_pagamento);

            if($doc_pagamento)
            {
                // ^ !! IMPORTANTE: 
                // Continuo solo se ho trovato il riferimento 
                // del doc di pagamento.
                
            //pr($a);
                
                //pr($doc_pagamento);
                
                $timestamp=time();
                $NOME_FILE=$doc_pagamento['id']."_".$a['USER']['id']."_az".$doc_pagamento['id_aziende']."_".$timestamp;
                
                //
            if(empty($doc_pagamento['id_aziende'])){$doc_pagamento['id_aziende']="0";}
            if(empty($doc_pagamento['id_onlus'])){$doc_pagamento['id_onlus']="0";}
            
            $DATA=array(
            $CAMPO_ID_PAGAMENTO=>$doc_pagamento['id'],
            'id_utenti'=>$a['USER']['id'],
            'id_aziende'=>$doc_pagamento['id_aziende'],
            'id_onlus'=>$doc_pagamento['id_onlus'],
            'data_creazione'=>$timestamp,
            'data_aggiornamento'=>'0',
            'n_aggiornamenti'=>'0',
            'id_utente_creatore'=>$a['USER']['id'],
            'importo'=>$a['POST_DATA']['importo'],
            'allegato'=>"",
            'note'=>$a['POST_DATA']['note'],
            );
            //
            // creo il record di bonifico
            //
            
            $INS=dbAction::_insert(array(
                'tab'=>$TAB_bonifici,
                'data'=>$DATA,
                'echo'=>true,
                ));
            //
            // Se l'inserimento è riuscito 
            // Invio il file
            //
                if($INS){
                    //pr($a['MODEL_CLASS']);
                    $MC=$a['MODEL_CLASS'];
                $FILE_UPLOAD=$MC::uploadFile(array('nome_file'=>$NOME_FILE,'dir_file'=>$DIR_ALLEGATI));
                //pr($FILE_UPLOAD);
                
                    //Se il file è stato caricato
                    //con successo aggiorno il nme del file
                    //che altrimenti rimane vuoto
                    
                    if($FILE_UPLOAD)
                    {
                        /*
                         *  return and array() like this: 
                         *  [new_name] => ""
                         *  [old_name] => ""
                         */
                        
                        $UPD=dbAction::_update(array(
                        'tab'=>$TAB_bonifici,
                        'id'=>$INS,
                        'only_this_fields'=>array('allegato'),
                         'data'=>array('allegato'=>$FILE_UPLOAD['new_name']),
                        ));
                        
                        
                        
                    }
                    
                    
                    //
                    htmlHelper::setFlash("msg",__("Bonifico registrato",false));
                    _CORE::redirect();
                }
            }
        }
        return $a;
    }
    
    
    
    
    
    function _del($a, $settings=null)
    {
        global $TAB_bonifici;
        $DIR_ALLEGATI="_files/bonifici/";
        
        /*
         * Per ora, come per gli allegati
         * viene permessa la cancellazione 
         * solo agllostesso utente che lo ha creato
         * 
         * poi si potrebbe prevedere un "hook" speciale per gli utenti "unora"
         * 
         */
        
            //$ID_Bonifico=$a['GET_DATA']['bonifico_delete'];//JUST FOR TEST
            $ID_Bonifico=$a['POST_DATA']['bonifico_delete'];
        
            
        if(is_natural_number($a['USER']['id']) )
        {
            $R=dbAction::_record(array(
            'tab'=>$TAB_bonifici,
            'value'=>$ID_Bonifico,
            'add_w'=>" AND id_utenti='".$a['USER']['id']."'",
            ));
            
            if($R)
            {
                $file=rootDOC.$DIR_ALLEGATI.$R['allegato'];
                //pr($file);
                unlink($file);
                
                $del=dbAction::_delete(array(
                'tab'=>$TAB_bonifici,
                'id'=>$R['id'],
                ));
                
            }
            
        }
        return $a;
    }
}