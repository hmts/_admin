<?php
class _ALLEGATI
{
    function _add($a,$settings=null)
    {
        global $TAB_allegati;
        
        $DIR_ALLEGATI="_files/allegati/";
        
        
        if(is_natural_number($a['USER']['id']))
        {
            
            //pr($a);
            
            //Setto il campo tabella
            $TABELLA = empty($settings['tabella']) ? $TABELLA=$a['USE_TAB'] : $TABELLA=$settings['tabella'];
            $ID_TABELLA = empty($settings['id_tabella']) ? $ID_TABELLA=$a['GET_DATA']['upd'] : $TABELLA=$settings['id_tabella'];
            $ID_UTENTI=$a['USER']['id'];
                
            $TIMESTAMP=time();
                
                
                //1. eseguo upload
                //dopo se fallimento non faccio insert
                //mentre se fallimento insert dovrei cancellare il file...
                
            $NOME_FILE=$TIMESTAMP."_".$ID_UTENTI;
            $FILE_UPLOAD=$this->uploadFile(array('nome_file'=>$NOME_FILE,'dir_file'=>$DIR_ALLEGATI));
                
                $TITOLO = empty($settings['titolo']) ? $TITOLO=$FILE_UPLOAD['old_name'] : $TITOLO=$settings['titolo'];
            
                
            if($FILE_UPLOAD)
            {
                //2. eseguo insert
                
                $DATA=array(
                'titolo'=>$TITOLO,
                'id_utenti'=>$ID_UTENTI,
                'data'=>$TIMESTAMP,
                'tabella'=>$TABELLA,
                'id_tabella'=>$ID_TABELLA,
                'nome_file'=>$FILE_UPLOAD['new_name'],
                
                );
                
                //pr($DATA);
            
            $INS=dbAction::_insert(array(
                'tab'=>$TAB_allegati,
                'data'=>$DATA,
                
                ));
            }
            
            
            
        }
    }




    function _del($a,$settings=null)
    {
        //
        if(isset($a['POST_DATA']['allegato_delete']))
        {
        global $TAB_allegati;
        $DIR_ALLEGATI="_files/allegati/";
            if(is_natural_number($a['USER']['id']))
            {
                $R=dbAction::_record(array(
                'tab'=>$TAB_allegati,
                'value'=>$a['POST_DATA']['allegato_delete'],
                'add_w'=>" AND id_utenti='".$a['USER']['id']."'",
                ));

                if($R)
                {
                    unlink(rootDOC.$DIR_ALLEGATI.$R['nome_file']);

                    $del=dbAction::_delete(array(
                    'tab'=>$TAB_allegati,
                    'id'=>$R['id'],
                    ));

                }
            }
        }
        
        return $a;
    }    
    
    
    
    
        function _loop($a,$settings)
    {
    global $TAB_allegati;
    $DIR_ALLEGATI="_files/allegati/";
        
        $LOOP=dbAction::_loop(array(
            'tab'=>$TAB_allegati,
            'where'=>"WHERE tabella='".$settings['tabella']."' AND id_tabella='".$settings['id_tabella']."'",
            'orderby'=>'ORDER BY data DESC',
            ));
        
        $LOOP_NEW=$LOOP;
        /*
         // Loop preparato in caso sia necessario manipolare i dati
        if($LOOP){
            foreach($LOOP AS $I)
            {
                
                
                
            $LOOP_NEW[]=$I;
            }
        }
        */
        return $LOOP_NEW;
    }

}
