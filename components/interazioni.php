<?php
class _INTERAZIONI
{
    function _ascolta($a,$settings=null)
    {
        global $TAB_utenti;
        global $TAB_interazioni;
        
        //1. Creazione nuova interazione:
        //Controllo che sia settata unanuova interazione e che ci sia un utente autenticato
        if(isset($a['POST_DATA']['NUOVA_INTERAZIONE']) && isset($a['USER']) && is_natural_number($a['USER']['id']))
        {
            $a=_INTERAZIONI::_new($a);
        }
        
        if(isset($a['POST_DATA']['DEL_INTERAZIONE']) && isset($a['USER']) && is_natural_number($a['USER']['id']))
        {
            $a=_INTERAZIONI::_del($a);
        }
        
        return $a;
    }
    
    
    
    function _new($a,$settings=null)
    {
        global $TAB_utenti;
        global $TAB_interazioni;
        global $TAB_aziende;
        global $TAB_onlus;
        
            //verifico che i campi necessario ci siano: titolo, testo, e id_utenti
            if(!empty($a['POST_DATA']['int_tit']) && !empty($a['POST_DATA']['int_txt']) && 
                        (
                        is_natural_number($a['POST_DATA']['id_utenti'])
                        || is_natural_number($a['POST_DATA']['id_aziende'])
                        || is_natural_number($a['POST_DATA']['id_onlus'])
                        )
                    )
            {
                $int_tit=$a['POST_DATA']['int_tit'];
                $int_txt=$a['POST_DATA']['int_txt'];
                $id_utenti=$a['POST_DATA']['id_utenti'];
                $id_aziende=$a['POST_DATA']['id_aziende'];
                $id_onlus=$a['POST_DATA']['id_onlus'];
                if(empty($id_utenti)){$id_utenti="0";}
                if(empty($id_aziende)){$id_aziende="0";}
                if(empty($id_onlus)){$id_onlus="0";}
                
                
                //eseguo anche una verifica sulla reale esistenza dell'utente ricavato da $id_utenti 
                //ovviamente solo se $id_utenti è diverso da 0
                if($id_utenti!="0")
                {
                $U=dbAction::_record(array('tab'=>$TAB_utenti,'value'=>$id_utenti));
                }
                //stessa cosa per id_aziende
                elseif($id_aziende!="0")
                {
                $U=dbAction::_record(array('tab'=>$TAB_aziende,'value'=>$id_aziende));
                    //Lascio $U per semplicità
                }
                elseif($id_onlus!="0")
                {
                $U=dbAction::_record(array('tab'=>$TAB_onlus,'value'=>$id_onlus));
                    //Lascio $U per semplicità
                }
                
                
                if(!$U){
                    if(DEBUG_ON){pr("ID_UTENTE o ID_AZIENDE o ID_ONLUS inesistente");}
                    $a['STATUS']="404";return $a;
                    }
                
                //Creo l'interazione
                //ma prima verifico i dati
                    
                    //alcune var da istanziare:
                    $ID_USR=false;
                    $NOTA_PRIVATA="0"; //valido solo per gli utenti unora
                    
                    
                    
                //Si tratta di un utente UNORA o AZIENDA?
                if($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'])
                {
                    //USR: UNORA
                    $ID_USR="id_utente_unora";
                    $ID_USR_NONE="id_utente_azienda";
                    
                    //qui potrei veriricare che l'utente abbia i permessi sul utente corrente...
                    //per ora lasciamo così.
                    
                    //Valorizzo il campo nota_privata
                    //solo per gli utenti unora:
                    if($a['POST_DATA']['int_privata']=="1")
                    {
                        $NOTA_PRIVATA="1";
                    }
                    
                }
                else if($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['ref_azienda'])
                {
                    //USR: AZIENDA
                    $ID_USR="id_utente_azienda";
                    $ID_USR_NONE="id_utente_unora";
                    
                }
                
                
                if(!$ID_USR)
                {
                    //ATTENZIONE UTENTE SENZA PERMESSI CERCA DI ACCEDERE
                    //ALLE INTERAZIONI
                    if(DEBUG_ON){pr("UTENTE SENZA PERMESSI");}
                    $a['STATUS']="404";return $a;
                }
                else
                {
                    //$ID_USR
                    //$NOTA_PRIVATA
                    
                    
                    $DATA=array(
                        'titolo'        =>$int_tit,
                        'testo'         =>$int_txt,
                        'id_utenti'         =>$id_utenti,
                        'id_aziende'         =>$id_aziende,
                        'id_onlus'         =>$id_onlus,
                        'data'          =>time(),
                        $ID_USR        =>$a['USER']['id'],
                        $ID_USR_NONE    =>'0',
                        
                        'privata'=>$NOTA_PRIVATA,
                        
                        );
                        
                    //a questo punto sono pronto per salvare l'interazione
                        
                    $INS=dbAction::_insert(array(
                    'tab'=>$TAB_interazioni,
                    'data'=>$DATA,
                    'echo'=>true,
                    ));
                    
                    
                    if($INS)
                    {
                        htmlHelper::setFlash("msg",__("Nota salvata",false));   
                        _CORE::redirect();
                    }
                    {
                        htmlHelper::setFlash("err",__("Impossibile salvare la nota.",false));   
                    }
                }
                
                
            }
            else
            {
            htmlHelper::setFlash("err",__("Compilare tutti i campi",false));   
            }
        
        
        return $a;
    }
    
    
    
    
    function _del($a,$settings=null)
    {
        global $TAB_utenti;
        global $TAB_interazioni;
        
        //richiedo di cancellare una nota, prima vedo quale:
        //usando l'id passato: del_id
        
        //Anzi la cancello direttamente usando l'id da $a['USER']['id']
            
            $ID_USR=false;
            
            if($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'])
            {
                $ID_USR="id_utente_unora";
            }
            else if($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['ref_azienda'])
            {
                $ID_USR="id_utente_azienda";
            }
            
        if($ID_USR)
        {
        $nota=dbAction::_delete(array(
            'tab'=>$TAB_interazioni,
            'where'=>"WHERE ".$ID_USR."='".$a['USER']['id']."' AND id='".$a['POST_DATA']['del_id']."'",
            ));
            if($nota)
            {
            echo "ok";exit;
            }
            else
            {
            echo "no";exit;
            }
        }
        
        return $a;
    }


    function _loop($a,$settings)
    {
        global $TAB_utenti;
        global $TAB_interazioni;
        
        
        //Verifico che l'utente sia unora, o azienda
        if(
            (
                is_numeric($settings['id_utenti'])
                || is_numeric($settings['id_aziende'])
                || is_numeric($settings['id_onlus'])
            )
                
            &&
            (
            $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora']
            || $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['ref_azienda']
            )
           )
        {
            // controllo per note private
            // da far vedere solo a utenti unora
            
            
            //quindi se non si tratta di utenti unora faccio vedere solo le note non private
            if($a['USER']['id_utenti_cat']!=$a['_CAT_UTENTI_']['unora'])
            {
                $ADD_W=" AND privata='0' ";
            }
            
            if(is_numeric($settings['id_utenti'])){
            $W="WHERE id_utenti='".$settings['id_utenti']."' ".$ADD_W;
            }
            else if(is_numeric($settings['id_aziende'])){
            $W="WHERE id_aziende='".$settings['id_aziende']."' ".$ADD_W;
            }
            else if(is_numeric($settings['id_onlus'])){
            $W="WHERE id_onlus='".$settings['id_onlus']."' ".$ADD_W;
            }
            else
            {
            $W="WHERE id='_NONE_'";
            }
            //pr($W);
            
        $LOOP=dbAction::_loop(array(
            'tab'=>$TAB_interazioni,
            'where'=>$W,
            'orderby'=>'ORDER BY data DESC'
            ));
            
            
            //aggoiungo i dati sull'autore ['_AUTORE_']
            if($LOOP){
            foreach($LOOP AS $I)
            {
                $CAMPO='id_utente_unora';
                if($I['id_utente_azienda']!="0")
                {
                    $CAMPO='id_utente_azienda';
                }
                    
                
                $I['_AUTORE_']=dbAction::_record(array(
                    'tab'=>$TAB_utenti,
                    'value'=>$I[$CAMPO],
                ));
                
                
                $LOOP_NEW[]=$I;
                
            }
            }
        return $LOOP_NEW;
        }
        else
        {
            return FALSE;
        }
        
    }
}