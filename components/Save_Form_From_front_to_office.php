<?php
class Form_Front2Office
{
    //public $destinazione="";
    
    function sava_data($a)
    {
        
        
        $dest=$a['POST_DATA']['req_form'];
        //pr($dest);
        //pr($a['POST_DATA']);exit;
        
        if($dest=="azienda")
        {
            global $TAB_aziende;
            $TAB_ACTUAL_TAB_DEST=$TAB_aziende;
            
            
            $privacy="0";
            if($a['POST_DATA']['privacy']=="si"){$privacy="1";}
            
            $slug=slug($a['POST_DATA']['azienda']);
            
            $note = print_r($a['POST_DATA'], true);
            
            //
            $dati=array(
                'titolo'=>$a['POST_DATA']['azienda'],
                'testo'=>'',
                'url'=>'',
                'visibile'=>'0',
                'slug'=>$slug,
                'deduzione'=>'',
                'detrazione'=>'',
                'importi'=>'',
                'altro_importo'=>'',
                'importo_mensile'=>'',
                'privacy'=>$privacy,
                'ore_lavorative'=>'',
                'onlus_escluse'=>'',
                'sede_legale'=>$a['POST_DATA']['sede_legale'],
                'citta'=>$a['POST_DATA']['citta'],
                'provincia'=>'',
                'cap'=>$a['POST_DATA']['cap'],
                'cf'=>'',
                'piva'=>$a['POST_DATA']['piva'],
                'settore'=>$a['POST_DATA']['settore'],
                'note'=>$note,
                'stato'=>'0',
                'modalita_donazione'=>'',
                'importi_confermati'=>'',
                'importi_confermati_data'=>'',
                );
        }
        else if($dest=="onp")
        {
            global $TAB_onlus;
            $TAB_ACTUAL_TAB_DEST=$TAB_onlus;
            
            $bilanciosociale="0";
            if($a['POST_DATA']['bilanciosociale']=="si"){$bilanciosociale="1";}
            
            $apartitica="0";
            if($a['POST_DATA']['apartitica']=="si"){$apartitica="1";}
            
            $privacy="0";
            if($a['POST_DATA']['privacy']=="si"){$privacy="1";}
            
            $note = print_r($a['POST_DATA'], true);
            
            //
            $dati=array(
                'titolo'=>$a['POST_DATA']['associazione'],
                'testo'=>'',
                'url'=>'',
                'id_aziende'=>'0',
                'email'=>$a['POST_DATA']['associazione'],
                'telefono'=>$a['POST_DATA']['tel'],
                'cellulare'=>'',
                'sede_legale'=>$a['POST_DATA']['indirizzo'],
                'citta'=>$a['POST_DATA']['citta'],
                'provincia'=>'',
                'cap'=>$a['POST_DATA']['cap'],
                'cf'=>$a['POST_DATA']['cf'],
                'piva'=>$a['POST_DATA']['piva'],
                'note'=>'Iscritta dal web il '.date('d-m-Y')." - ".$note,
                'stato'=>'0',
                'bilancio_sociale'=>$bilanciosociale,
                'apartitica'=>$apartitica,
                'privacy'=>$privacy,
                );
            
            
            
           
            
        }
        else if($dest=="dipendente")
        {
            
            global $TAB_utenti;
            $TAB_ACTUAL_TAB_DEST=$TAB_utenti;
            
            
            $user_esiste=dbAction::_record(array(
                'tab'=>$TAB_progetti,'set_w'=>"WHERE email='".$a['POST_DATA']['email']."'",
                
                ));
            
            
            
            $privacy="0";
            if($a['POST_DATA']['privacy']=="si"){$privacy="1";}
            
            $date = new DateTime($a['POST_DATA']['data_nascita_aaaa']."-".$a['POST_DATA']['data_nascita_mm']."-".$a['POST_DATA']['data_nascita_gg']);
            $a['POST_DATA']['data_nascita']=$date->getTimestamp();
            
            $id_onlus="0";
            if(!empty($a['POST_DATA']['associazione']))
            {
                //$associazione
                $associazione=  explode("-", $associazione);
                $id_onlus=$associazione[0];
                if(!is_natural_number($id_onlus)){$id_onlus="0";}
            }
            
            $note = print_r($a['POST_DATA'], true);
            
            
            global $TAB_utenti_cat; 
            $a['LOOP'][$TAB_utenti_cat]=dbAction::_loop(array('tab'=>$TAB_utenti_cat));
            foreach($a['LOOP'][$TAB_utenti_cat] AS $I)
            {
                $a['_CAT_UTENTI_'][$I['nome_uri']]=$I['id'];
                $a['_UTENTI_CAT_'][$I['id']]=$I['nome'];
            }
            
            //pr($a['_CAT_UTENTI_']);
                
            
            
            
            $email_codice_verifica=generatePassword()."_".time();
            
            
            //$STOP_INSERT=false;
            
            if($user_esiste)
            {
                //Blocco l'inserimento
                $STOP_INSERT=true;
                //Utente esistente blocco la registrazione
                //e invio un messaggio
                if($user_esiste['attivo']=="1")
                {
                    //User attivo
                    htmlHelper::setFlash("msg",__("La tua donazione risulta nei nostri archivi!",false));
                }
                else
                {
                    //User non attivo
                    //Abilito l'inoltro della mail di attivazione
                    $ins=$user_esiste['id'];
                    //ripopolo il codice con quello attualmente registrato
                    $email_codice_verifica=$user_esiste['email_codice_verifica'];
                }
                return $a;
            }
            
            
            
            //
            $dati=array(
                'nome'=>$a['POST_DATA']['nome'],
                'cognome'=>$a['POST_DATA']['cognome'],
                'email'=>$a['POST_DATA']['email'],
                'email_verificata'=>'0',
                'email_codice_verifica'=>$email_codice_verifica,
                'password'=>'',
                'codice_recupero_psw'=>'',
                'codice_recupero_psw_scadenza'=>'',
                'data_iscrizione'=>time(),
                'attivo'=>'0', 
                'bannato'=>'0', 
                'data_nascita'=>$a['POST_DATA']['data_nascita'],
                'indirizzo'=>$a['POST_DATA']['indirizzo'],
                'citta'=>$a['POST_DATA']['citta'],
                'provincia'=>$a['POST_DATA']['provincia'],
                'regione'=>'',
                'paese'=>'',
                'cap'=>$a['POST_DATA']['cap'],
                'cf'=>$a['POST_DATA']['codice_fiscale'],
                'piva'=>'',
                'telefono'=>$a['POST_DATA']['tel'],
                'cellulare'=>'',
                'ruolo'=>'',
                'ufficio'=>$a['POST_DATA']['sede_lavoro'],
                'note'=>$note ,
                'id_utenti_cat'=>$a['_CAT_UTENTI_']['donatori'],
                'id_aziende'=>$a['POST_DATA']['id_aziende'],
                'id_onlus'=>$id_onlus,
                'referente_paghe'=>'',
                'privacy'=>$privacy,
                'sorgente_dati'=>'modulo online',
                'stato'=>'0',
                'importo_selezionato'=>'',
                'importo_selezionato_id_utenti'=>'',
                'importo_selezionato_data'=>'',
                'numero_ore_mese'=>'',
                'deduzione'=>'',
                'detrazione'=>'',
                'onp50x100'=>'',
                'valore_unora'=>'',
                
                );
            
            
            //pr($dati);exit;
        }
        else
        {
            
            if(DEBUG){pr($a);exit;}
            $a['STATUS']="404";
        }
        
        //
        //
        //  pr($TAB_ACTUAL_TAB_DEST);
        //
        //
        if(!$STOP_INSERT){
            
            
        $ins=dbAction::_insert(array(
            'tab'=>$TAB_ACTUAL_TAB_DEST,
            'data'=>$dati,
            //'echo'=>true
            ));
        }
        
        //exit;
            
            /*
             * 
             * Se l'iscrizione è 
             * andata a buon fine
             * e se si tratta di un donatore
             * mandao la mail di attivazione...
             * 
             */
        //$SMTP_DATA=save_array(SMTP_DATA,"get");
        global $SMTP_DATA;
        global $lang;
        //
        //
        //pr($SMTP_DATA);exit;
        
            if($dest=="dipendente" && $ins)
            {
                $ogetto=$lang['attiva-la-tua-registrazione']." ".time();
                
                $url_attivazione=rootWWW."?attiva_donazione=".$email_codice_verifica;
                        
                $destinatari=array($a['POST_DATA']['email']);
                //$destinatari=array("gabriele.marazzi@gmail.com");
                $msg =$lang['EMAIL']['ATT1'];
                $msg.='<br /><br /><div style="background:#ffffcc;padding:5px;border:1px orange solid;text-align:center;border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;">';
                $msg.='<b><a style="color:orange;text-decoration:none;font-size:15px;" href="'.$url_attivazione.'">'.$lang['EMAIL']['ATT2'].'</a></b></div><br /><br />';
                $msg.='<i style="font-size:11px;">'.$lang['EMAIL']['ATT3'].'</i>';
                $msg.='<br /><br /><br />';
                
                //$SMTP_DATA=save_array(SMTP_DATA,"get");
                
                $template='no-template';
                $template=  rootDOC.'_views/_email/default.php';
                $SETT=array(
                'SMTP_DATA'=>$SMTP_DATA,
                'destinatari'=>$destinatari,
                'tit'=>$ogetto,
                'txt'=>$msg,
                'template'=>rootDOC."_views/_email/default.php",
                //'allegati'=>false,
                );
                
                
                $em=_CORE::smtpEmail($SETT);
                //pr($em);exit;
                
                // Trattandosi di un donatore
                // e necessario inviare anche a tutti i referenti aziendali 
                // della sua azienda la mail con un riferimento riguardo la
                // nuova registrazione ed un link al profilo dell'utente
                // in modo da poter verificare i dati e riportare comunicazioni al riguardo
                // 
                
                //1. loop dei referenti aziendali per l'azienda dell'utente
                global $TAB_utenti;
                $referenti=dbAction::_loop(array(
                    'tab'=>$TAB_utenti,
                    'where'=>"WHERE id_utenti_cat='".$a['_CAT_UTENTI_']['ref_azienda']."' "
                            ." AND id_aziende='".$dati['id_aziende']."' AND attivo='1' ",
                    'echo'=>true
                    
                ));
                
                //pr(count($referenti));
                
                
                if($referenti){
                
                    foreach ($referenti AS $Ref){
                        $destinatari_ref[]=$Ref['email'];
                    }
                    
                    $msg ='<div style="text-align:center">';
                    $msg.="Un nuovo donatore si è registrato nel sistema<br><br>";
                    $msg.='<a href="'.URL_OFFICE.'tab/'.$TAB_utenti.'/upd/'.$ins.'/">Accedi per visualizzare la scheda relativa</a>.';
                    $msg.='</div>';
                    
                    $ogetto2="Nuova donatore in attivazione - ".time();
                    
                $template='no-template';
                $template=  rootDOC.'_views/_email/default.php';
                $SETT=array(
                'SMTP_DATA'=>$SMTP_DATA,
                'destinatari'=>$destinatari_ref,
                'tit'=>$ogetto2,
                'txt'=>$msg,
                'template'=>rootDOC."_views/_email/default.php",
                //'allegati'=>false,
                );
                
                
                $em2=_CORE::smtpEmail($SETT);
                    //if(DEBUG)pr($em2);exit;
                }
                
                
            }
            else if($ins)
            {
                //htmlHelper::setFlash("msg",__("Iscrizione ricevuta! Grazie!",false));
            }
        
        return $a;
    }
    
}
?>