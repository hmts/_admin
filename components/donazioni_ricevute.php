<?php
ini_set ( 'max_execution_time', 9000000); 

class ricevute{

    function get_total()
    {
        global $TAB_utenti;
        global $TAB_donazioni;
        global $TAB_donazioni_onp;
        global $TAB_donazioni_ricevute;
        
        $ANNO=$_SESSION[$TAB_donazioni_ricevute]['anno'];
        $DONATORI=dbAction::_loop(array('tab'=>$TAB_utenti,
                //'limit'=>"LIMIT 0,5"
                ));
        foreach ($DONATORI AS $DONATORE)
        {
            //Per ogni donatore prendo il loop delle donazioni nell'anno
            $donazioni=dbAction::_loop(array(
                    'tab'=>$TAB_donazioni,
                    'where'=>"WHERE id_utenti='".$DONATORE['id']."' AND anno='".$ANNO."'",
             ));
            
            //Ora per ogni donazione devo prendere gli importi delle singole onp
            //E sommarli assieme in un unico array diviso per ogni ONP
            //In modo da tenere tutto sottomano...
            $tot_onp="0";
            $TOT_Donazioni="0";
            if($donazioni)
            {
                //pr($donazioni);
                foreach ($donazioni AS $donazione)
                {
                    $TOT_DONAZIONE=$donazione['euro_mese'];
                    $ID="-".$DONATORE['id'];
                    if($donazione['euro_mese']=="" || $donazione['euro_mese']=="0.00")
                    {
                        $ID="@".$DONATORE['id'];
                        $TOT_DONAZIONE=format_euro($donazione['valore_unora']*$donazione['ore']);
                    }
                    //
                    //
                    //
                    $TOT_Donazioni=$TOT_Donazioni+$TOT_DONAZIONE;
                    $TOT_DONAZIONI[$DONATORE['id']]=$TOT_Donazioni;
                    //pr($ID." = ".$TOT_DONAZIONE);
                    //
                    //
                    //
                    //Qui calcolo le donazioni_onp
                    $donazioni_onp=dbAction::_loop(array(
                            'tab'=>$TAB_donazioni_onp,
                            'where'=>"WHERE id_donazioni='".$donazione['id']."'",
                    ));
                    foreach ($donazioni_onp AS $donazione_onp)
                    {
                        //Salvo un array con i totali divisi per 
                        //id utenti e id onp
                        $tot_onp=format_euro($tot_onp+$donazione_onp['importo']);
                        $ONP[$DONATORE['id']][$donazione_onp['id_onlus']]=$tot_onp;
                    }

                }
            }
        }
        
        return array($TOT_DONAZIONI,$ONP);
    }
    
    
    function save($data)
    {
        //pr($data);
        global $TAB_utenti;
        global $TAB_donazioni;
        global $TAB_donazioni_onp;
        global $TAB_donazioni_ricevute;
        
        $ANNO=$_SESSION[$TAB_donazioni_ricevute]['anno'];
        
        $totali=$data[0];
        $totali_onp=$data[1];
        //pr($totali);
        //pr($totali_onp);
        //Cancello tutti i dati dell'anno registrati
        $del =  dbAction::_delete(array(
            'tab'=>$TAB_donazioni_ricevute,
            'where'=>"WHERE anno='".$ANNO."'",
            //'echo'=>true,
            ));
        
        //Loop di insert
        
        //pr($data);
        
        if(!$totali)return false;
        
        foreach ($totali as $k=>$v)
        {
            //Preparo gli importi
            $importi_onp=array();
            foreach ($totali_onp[$k] as $k_onp=>$v_onp)
            {
                $importi_onp[]=array('id_onlus'=>$k_onp,'euro'=>$v_onp);
            }
            $data=array(
                'id_utenti'=>$k,
                'importo_totale'=>$v,
                'importi'=>  json_encode($importi_onp),
                'email_inviate'=>'0',
                'invii'=>'0',
                'anno'=>$ANNO,
                'data_creazione'=>time(),
                'data_modifica'=>time(),
            );
            
            $ins = dbAction::_insert(array('tab'=>$TAB_donazioni_ricevute,'data'=>$data));
            
            self::doPdf($data);
            
        }
    }
    
    
    
    function doPdf($data)
    {
        global $TAB_utenti;
        global $TAB_onlus;
        
        global $lang;
        //pr($lang);
        
        $UTENTE=dbAction::_record(array(
            'tab'=>$TAB_utenti,
            'value'=>$data['id_utenti']
                ));
        
        //$data
        //pr($data);
        $ANNO=$data['anno'];
        
        $DIRECTORY_DOC_ANNO=rootDOC."_files/doc_donazioni_ricevute/".$ANNO."/";
        
        
        if (!file_exists($DIRECTORY_DOC_ANNO)) {
            mkdir($DIRECTORY_DOC_ANNO, 0777, true);
        }
        
        $importi=json_decode($data['importi'],true);
        $template=rootDOC."_views/_DOC/donazioni_ricevute.php";
        if(empty($importi)){
        return false;}
        
        
        $template_base=  file_get_contents($template);
        
        
        //pr($importi);
        
        foreach ($importi AS $importo){
        //$importo['id_onlus'];
        //$importo['euro'];
        //pr($importo);
        $ONP=dbAction::_record(array(
        'tab'=>$TAB_onlus,
        'value'=>$importo['id_onlus']
            ));
        
        $url_firma=rootWWW."_files/img/onlus/firma_".$importo['id_onlus'].".png?".time().rand(0,9);
        $url_logo=rootWWW."_files/img/onlus/".$importo['id_onlus'].".png?".time().rand(0,9);
        $FIRMA_ONP='<img src="'.$url_firma.'">';
        $LOGO_ONP='<img src="'.$url_logo.'">';
        
        if(!file_exists(rootDOC."_files/img/onlus/firma_".$importo['id_onlus'].".png"))
        {$FIRMA_ONP=$ONP['intestazione_legale'];}
        if(!file_exists(rootDOC."_files/img/onlus/".$importo['id_onlus'].".png"))
        {$LOGO_ONP=$ONP['intestazione_legale'];}
        //$FIRMA_ONP=$ONP['intestazione_legale'];
        //$LOGO_ONP=$ONP['intestazione_legale'];
        
        $file_name=$data['id_utenti']."-".$importo['id_onlus'];
        
        $template=str_replace("#ROOT_WWW#",rootWWW,$template_base);
        $template=str_replace("#TITLE#",$settings['title'],$template);
        
        $template=str_replace("#INTESTAZIONE#",$settings['intestazione'],$template);
        $template=str_replace("#NOME-UTENTE#",$UTENTE['nome']." ".$UTENTE['cognome'],$template);
        $template=str_replace("#INDIRIZZO-UTENTE#",$UTENTE['indirizzo']
                ."<br>".$UTENTE['cap']
                ." (".$UTENTE['provicnia'].") ".$UTENTE['citta'],$template);
        $template=str_replace("#CODICEFISCALE-UTENTE#",$UTENTE['cf'],$template);
        
        $template=str_replace("#DATA-ODIERNA#",date('d')." ".$lang['mesi'][date('n')]." ".date('Y'),$template);
        $template=str_replace("#ANNO#",$ANNO,$template);
        $template=str_replace("#EURO#",$importo['euro'],$template);
        
        $template=str_replace("#LOGO-ONP#",$LOGO_ONP,$template);
        $template=str_replace("#FIRMA-ONP#",$FIRMA_ONP,$template);
        $template=str_replace("#INTESTAZIONE-ONP#",$ONP['intestazione_legale'],$template);
        $template=str_replace("#INDIRIZZO-ONP#",$ONP['sede_legale']."<br>"
                .$ONP['cap']." (".$ONP['provincia'].") ".$ONP['citta']
                ,$template);
        #INTESTAZIONE-ONP#
        
        $PDF_FILE=$DIRECTORY_DOC_ANNO.$file_name.".pdf";
            if(file_exists($PDF_FILE)){unlink($PDF_FILE);}

            ini_set('memory_limit', '-1');
            
            $html2pdf = new HTML2PDF('P','A4','it');
            $html2pdf->WriteHTML($template);
            $html2pdf->Output($PDF_FILE, 'F');
            
            
        }    
            
    }
    
    function sendAll()
    {
        global $TAB_donazioni_ricevute;
        $ANNO=$_SESSION[$TAB_donazioni_ricevute]['anno'];
        
        if(!is_natural_number($ANNO))return pr('Selezionare un anno');
        
        $donazioni=dbAction::_loop(array(
            'tab'=>$TAB_donazioni_ricevute,
            'where'=>"WHERE anno='".$ANNO."'",
        ));
        //pr($donazioni);
        foreach ($donazioni AS $D){
            self::send($D['id']);
        }
        exit;
    }
    
    function send($id)
    {
        global $SMTP_DATA;
        global $TAB_utenti;
        global $TAB_donazioni_ricevute;
        global $TAB_onlus; 
        
        $DATI_DONAZIONE=  dbAction::_record(
                array('tab'=>$TAB_donazioni_ricevute,'value'=>$id)
                );
        
        $ANNO=$DATI_DONAZIONE['anno'];
        $id_utente=$DATI_DONAZIONE['id_utenti'];
        
        $onlus=dbAction::_loop(array('tab'=>$TAB_onlus));

            foreach ($onlus as $onlus){
                $onlus_by_id[$onlus['id']]=$onlus['titolo'];
            }

        $destinatari=array('gabriele.marazzi@gmail.com');
        $oggetto="unora.org - ricevute donazioni - anno ".$ANNO." - ".time();
        
        
        //$datiD=print_r($DATI_DONAZIONE,true);
        
        //pr($DATI_DONAZIONE);
        
        $importi=  json_decode($DATI_DONAZIONE['importi'],true);
        $txt ="In allegato abbiamo inserito le ricevute delle sue donazioni per l'anno ";
        $txt.=" ".$ANNO.". <br><br>";
        
        $txt.='<div style="padding:9px;background-color:#ffffcc;">
            <table width="100%">';
        $allegati=array();
        foreach ($importi AS $imp)
        {
            $urlRicevuta=rootDOC."_files/doc_donazioni_ricevute/".$ANNO."/".$id_utente."-".$imp['id_onlus'].".pdf";
            $allegati[]=$urlRicevuta;
            
            $txt.='<tr>';
            $txt.="<td width='70%'><b>".$onlus_by_id[$imp['id_onlus']]."</b> </td>";
            $txt.='<td align="right">'.$imp['euro'].'</td>';
            //$txt.='<td><a href="'.$urlRicevuta.'">Scarica Ricevuta</a></td>';
            $txt.='</tr>';
        }
        $txt.="</table></div>";
        
            //pr($destinatari);
            //pr($allegati);
            
        $SETT=array(
                'SMTP_DATA'=>$SMTP_DATA,
                'destinatari'=>$destinatari,
                'tit'=>$oggetto,
                'txt'=>$txt,
                'template'=>rootDOC."_views/_email/default.html",
                //'allegati'=>false,
                );
        
            if(!empty($allegati))
            {
                   $SETT['allegati']=$allegati;
            }
        
                //pr($SETT);exit;
                //pr($SETT);
                $invio = _CORE::smtpEmail($SETT);
                echo 'Invio completato';
                return $invio;
    }
}