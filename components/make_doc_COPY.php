<?php
ini_set ( 'max_execution_time', 9000000); 


class _DOC
{
    function _make($a,$settings)
    {
        $TIPO_DI_DOC=$settings['doc_type'];
        /*
         * CASI:
         * _donazioni
         * 
         */
        //$a=_DOC::_donazioni($a,$settings=null);
        $a=_DOC::$TIPO_DI_DOC($a,$settings);
        
        //$a['_DOC']['file'];
        //$a['_DOC']['content'];
        //write($path_file,$content,$mode='w');
        //
            //Se il file esiste prima lo cancello
            //per fare spazio ad uno più aggiornato
            
        @unlink($a['_DOC']['file']);
        write($a['_DOC']['file'],$a['_DOC']['content']);
        
        
        
        return $a;
    }
    
    function _donazioni($a,$settings)
    {
        if(empty($settings['template'])){
            $settings['template']=rootDOC."_views/_DOC/default.php";
        }
        $template=  file_get_contents($settings['template']);
        //$settings['title'];
            
        
        
        $template=str_replace("#ROOT_WWW#",rootWWW,$template);
        $template=str_replace("#TITLE#",$settings['title'],$template);
        $template=str_replace("#INTESTAZIONE#",$settings['intestazione'],$template);
        $template=str_replace("#DATI#",$settings['dati'],$template);
        
        $a['_DOC']['file']=rootDOC."_files/doc_donazioni/".$settings['file_name'].".html";
        $a['_DOC']['content']=$template;
        
        
        //CREO IL PDF
        $PDF_FILE=rootDOC."_files/doc_donazioni/".$settings['file_name'].".pdf";
            //
            //SE IL FILE ESISTE LO CANCELLO per tenere solo quello aggiornato
            //
            if(file_exists($PDF_FILE))
            {
                unlink($PDF_FILE);
            }
            ini_set('memory_limit', '-1');
            $html2pdf = new HTML2PDF('P','A4','it');
            $html2pdf->WriteHTML($template);
            $html2pdf->Output($PDF_FILE, 'F');
                
            
            
        return $a;
    }
    
    
    function _ordine_pagamenti($a,$settings)
    {
        if(empty($settings['template'])){
            $settings['template']=rootDOC."_views/_DOC/default.php";
        }
        $template=  file_get_contents($settings['template']);
        //$settings['title'];
        
        //pr($settings);
        
        $template=str_replace("#ROOT_WWW#",rootWWW,$template);
        $template=str_replace("#TITLE#",$settings['title'],$template);
        $template=str_replace("#INTESTAZIONE#",$settings['intestazione'],$template);
        $template=str_replace("#DATI#",$settings['dati'],$template);
        
        $a['_DOC']['file']=rootDOC."_files/doc_ordini_pagamento/".$settings['file_name'].".html";
        $a['_DOC']['content']=$template;
        
        //CREO IL PDF
        $PDF_FILE=rootDOC."_files/doc_ordini_pagamento/".$settings['file_name'].".pdf";
            //
            //SE IL FILE ESISTE LO CANCELLO per tenere solo quello aggiornato
            //
            if(file_exists($PDF_FILE))
            {
                unlink($PDF_FILE);
            }
            ini_set('memory_limit', '-1');
            $html2pdf = new HTML2PDF('P','A4','it');
            $html2pdf->WriteHTML($template);
            $html2pdf->Output($PDF_FILE, 'F');
                
            
            
        return $a;
    }

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ### D O   I T ...
    
    
    
    function doit_donazioni($a)
    {
    global $lang;
        //pr($lang);
        global $TAB_doc_donazioni;
        
        global $TAB_donazioni;
        global $TAB_donazioni_info;
        global $TAB_donazioni_onp;
        
        global $TAB_utenti;
        global $TAB_onlus;
        global $TAB_aziende;
        
        //pr($a['USER']['id']);
        
        
        $settings=$a['SETTINGS'];
        
        $a['TEMPLATE_DIR_WWW']=rootDOC."_template/_office/";
        
        /*
         * 
         * Verifico sia che si tratti di un utente corente
         * e sia che appartenga solamente alle categorie unora e robot
         * 
         */
        if(is_natural_number($a['USER']['id']) && 
                (
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] || 
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']
                ) 
           )
        {
        /* * * * * * * * * * * * * * * * 
         *
         *
         *  Preparo tutte le variabili
         *  
         *
        * * * * * * * * * * * * * * * */
                
            //
            $anno=empty($settings['anno'])?date('Y'):$settings['anno'];
            $mese=empty($settings['mese'])?date('m'):$settings['mese'];
            $annomese=$anno.$mese;
            
            
            //pr(date('m'));
            //pr($annomese);exit;
            
            
                // Last Day of the Mont:
                // -- > date("t");
                
            //
            $id_admin=$a['USER']['id'];
                
                //Gli altri dati: (vengono creati nel loop Donatori)
                //$id_utenti
                //$id_aziende
                    $W = "WHERE ";
                    $W.= " id_utenti_cat='".$a['_CAT_UTENTI_']['donatori']."' ";
                    $W.= " AND stato='1' ";
                    
                $loop_donatori=dbAction::_loop(array(
                'tab'=>$TAB_utenti,
                'where'=>$W,
                'orderby'=>"ORDER BY id_aziende, cognome, nome",
                ));
                
                
                
                
                
                
                //Controllo le donazioni attuali e cancello quelle che non
                //sono piu attive
                //
                $checkDonazioni=dbAction::_loop(array(
                    'tab'=>$TAB_donazioni,
                    'where'=>"WHERE annomese='".$annomese."'",
                    
                ));
                
                foreach ($checkDonazioni AS $DON){
                    $checkUser=dbAction::_record(array('tab'=>$TAB_utenti,'value'=>$DON['id_utente']));
                    if($checkUser['stato']!="1")
                    {
                        $delDON=dbAction::_delete(array(
                            'tab'=>$TAB_donazioni,
                            'where'=>"WHERE id='".$DON['id']."'"
                            ));
                    }
                }
                
                
                
                //pr($loop_donatori);
                foreach ($loop_donatori AS $I)
                {
                    $ImportoSel=explode("|", $I['importo_selezionato']);
                    
                    /*
                     * Ricavo la modalita_donazione dall'importo
                     * dell'azienda.
                     * 
                     */
                    //$I['modalita_donazione']=$ImportoSel[1];
                    $I['modalita_donazione']=$I['REL'][$TAB_aziende]['modalita_donazione'];
                        if(empty($I['REL'][$TAB_aziende]['modalita_donazione']))
                        {
                            $I['modalita_donazione']="euro"; //valore default
                        }
                        
                        
                        
                        
                        if($I['modalita_donazione']=="euro")
                        {
                            $IMPORTO=$ImportoSel[2];
                        }
                        else
                        {
                            //ORE! devo moltiplicare il valore selezionato per il numero_ore_mese
                            $IMPORTO=  format_euro($I['valore_unora']*$I['numero_ore_mese']);
                        }
                    $I['importo']=$IMPORTO;
                    
                    $loop_donatori_NEW[]=$I;
                    
                        if(!$IMPORTO_TOT_azienda[$I['id_aziende']]){$IMPORTO_TOT_azienda[$I['id_aziende']]="0,00";}
                    $IMPORTO_TOT_donatori_azienda[$I['id_aziende']]++;
                    //...
                    
                    //pr("1] ".$I['id_aziende']." ".$I['importo']." ".$IMPORTO_TOT_azienda[$I['id_aziende']]);
                    
                    //pr("R: ".$IMPORTO_TOT_azienda[$I['id_aziende']]."+".$I['importo']."=".format_euro($IMPORTO_TOT_azienda[$I['id_aziende']]+$I['importo']));
                    $IMPORTO_TOT_azienda[$I['id_aziende']]=format_euro($IMPORTO_TOT_azienda[$I['id_aziende']]+$I['importo']);
                    //...
                    $IMPORTO_TOT=format_euro($IMPORTO_TOT+$I['importo']);
                    
                    //pr($I['importo']);
                    //pr("2] ".$I['id_aziende']." ".$I['importo']." ".$IMPORTO_TOT_azienda[$I['id_aziende']]);
                }
                
                //pr($IMPORTO_TOT_azienda);
            
            /* * * * * * * * * * * * * * * * * * * * 
             *
             * Lancio il loop dei donatori attivi e registro le donazioni per il mese corrente
             *
            * * * * * * * * * * * * * * * * * * * * */
                
                //Dovrei fare un "DOCUMENTO" di generazione DOnazioni
                //dove mettete la data di generazione, il periodo mese anno
                //e, tramite un ID nelle donazioni la lista delle donazioni ad esso abinate...
                //ovviamente il numero di donatori trovati...
                //ed altre info..
                //pr(count($loop_donatori));
                
            $TOT_DONATORI=count($loop_donatori);
            
                
                
                
               /*
                * 
                * Prima di entrare nel loop
                * delle donazioni 
                * 
                * Creo o Aggiorno il documento di donazione
                * del periodo attuale "annomese"
                * 
                * per ogni annomese
                * può essitere uno ed un solo documento di donazione
                * 
                * se esiste già il documento viene aggiornato
                * 
                * 
                */ 
                
                $DOC_DONAZIONE=dbAction::_record(array(
                        'tab'=>$TAB_doc_donazioni,
                        'set_w'=>"WHERE annomese='".$annomese."'",
                        //'echo'=>true
                        ));
                
                
                $DATI_AZIENDE_mod=dbAction::_loop(array('tab'=>$TAB_aziende));
                    foreach ($DATI_AZIENDE_mod AS $AZ)
                    {
                        if($IMPORTO_TOT_donatori_azienda[$AZ['id']]!="0" && !empty($IMPORTO_TOT_donatori_azienda[$AZ['id']]))
                        {
                        unset($AZ['testo']);
                        unset($AZ['privacy']);
                        $AZ['importo']=$IMPORTO_TOT_azienda[$AZ['id']];
                        $AZ['tot_donatori']=$IMPORTO_TOT_donatori_azienda[$AZ['id']];
                        
                        $DATI_AZIENDE[]=$AZ;
                        }
                    }
                    //pr($DATI_AZIENDE);
                    
                $TOT_AZIENDE=count($DATI_AZIENDE);
                
                //pr("CREAZIONE DOC donazioni");
                
                if($DOC_DONAZIONE)
                {
                    
                    /*
                     * FERMO TUTTO SE ESSITE UN ORDINE DI PAGAMENTO
                     * 
                     */
                    if($DOC_DONAZIONE['id_doc_ordini_pagamento']!='0')
                    {
                        htmlHelper::setFlash("err","Impossibile Aggiornare<br>Elenco Donazioni CHIUSO!");
                        return $a;
                    }
                    
                    
                    
                    
                    
                    /********************************
                     * 
                     * AGGIORNO IL DOC Delle Donazioni
                     */
                    
                    $n_aggiornamenti=$DOC_DONAZIONE['n_aggiornamenti']+1;
                    
                    $DATA_doc=array(
                        'data_creazione'=>$DOC_DONAZIONE['data_creazione'],
                        'data_aggiornamento'=>time(),
                        'n_aggiornamenti'=>$n_aggiornamenti,
                        'anno'=>$DOC_DONAZIONE['anno'],
                        'mese'=>$DOC_DONAZIONE['mese'],
                        'annomese'=>$DOC_DONAZIONE['annomese'],
                        'tot_donazioni'=>$TOT_DONATORI,
                        'tot_aziende'=>$TOT_AZIENDE,
                        'dati_aziende'=>  json_encode($DATI_AZIENDE),
                        'importo_totale'=>$IMPORTO_TOT,
                        );
                $UPD_doc=dbAction::_update(array(
                    'tab'=>$TAB_doc_donazioni,
                    'id'=>$DOC_DONAZIONE['id'],
                    'data'=>$DATA_doc,
                    //'echo'=>true,
                    ));
                $ID_DOC_Donazioni=$DOC_DONAZIONE['id'];
                }
                else
                {
                    /********************************
                     * 
                     * CREO IL DOC Delle Donazioni
                     */
                    $data_creazione=time();
                    
                    $DATA_doc=array(
                        'data_creazione'=>$data_creazione,
                        'data_aggiornamento'=>'0',
                        'n_aggiornamenti'=>'0',
                        'anno'=>$anno,
                        'mese'=>$mese,
                        'annomese'=>$annomese,
                        'tot_donazioni'=>$TOT_DONATORI,
                        'tot_aziende'=>$TOT_AZIENDE,
                        'dati_aziende'=>  json_encode($DATI_AZIENDE),
                        'importo_totale'=>$IMPORTO_TOT,
                        );
                        
                $INS_doc=dbAction::_insert(array(
                    'tab'=>$TAB_doc_donazioni,
                    'data'=>$DATA_doc,
                    //'echo'=>true,
                    ));  
                
                    if(!$INS_doc)
                    {echo'ERROR';}
                $ID_DOC_Donazioni=$INS_doc;
                }
                
                
                
                
                
                
                
                
                /*
                 * 
                 *  G E S T I O N E   D E L   D O C
                 * 
                 * 
                 * 
                 */
                $HTML1 ="<br><br>";
                $HTML1.="Documento creato il: ".date("d-m-Y H:i:s",$DATA_doc['data_creazione'])."<br>";
                if($DATA_doc['data_aggiornamento']!="0"){
                $HTML1.="Ultima modifica del: ".date("d-m-Y H:i:s",$DATA_doc['data_aggiornamento'])."<br>";
                $HTML1.="Numero di aggiornamenti: <b>".$DATA_doc['n_aggiornamenti']."</b><br>";
                }
                $HTML1.="<b>Periodo Donazioni</b><br>  ";
                $HTML1.="Anno: ".$DATA_doc['anno']." Mese: ".$DATA_doc['mese']."<br>";
                    $HTML1.='<table>';
                    $HTML1.='<tr><td><a href="'.rootWWW.'"><img src="'.rootWWW.'icona-mini.png" width="15" height="15" border="0"></a></td>';
                    $HTML1.='<td><b><a style="text-decoration:none;color:#941b20;" href="'.rootWWW.'">UNORA.ORG</a></b></td></tr></table>';
                $HTML1.="<br><br>  ";
                
                
                
                
                $HTML2 ="<br>";
                $HTML2.="<b>Importo Totale:</b> &euro;.".$IMPORTO_TOT."<br>  ";
                $HTML2.="<b>Totale Donatori:</b> ".$TOT_DONATORI."<br>  ";
                $HTML2.="<b>Totale Aziende:</b> ".$TOT_AZIENDE."<br>  ";
                $HTML2.="<br>  ";
                
                    $HTML2.='<table border="0" cellspacing="5" cellpadding="3">';
                    //INTESTAZIONE
                    $HTML2.='<tr>';
                    $HTML2.='<td>&nbsp;</td>';
                    $HTML2.='<td>Aziende</td>';
                    $HTML2.='<td>Totale</td>';
                    $HTML2.='<td>Modalit&agrave; Donazione</td>';
                    
                    $HTML2.='</tr>';
                    
                foreach ($DATI_AZIENDE as $ITEM) {
                if($ITEM['tot_donatori'])
                {
                    //pr($ITEM);
                    $HTML2.='<tr bgcolor="#ffffcc">';
                    //pr($ITEM);
                        $URLIMG=$ITEM['IMG2']['.png']['url'];
                        $URLIMG2=str_replace(rootWWW,rootDOC,$URLIMG);
                        $IMG="  ";
                        if(file_exists($URLIMG2)){
                        $IMG='<img src="'.$URLIMG.'" width="90">';
                        }
                    $HTML2.='<td bgcolor="#ffffff" style="width:90px;"><div>'.$IMG.'</div></td>';
                    $HTML2.='<td style="width:400px;"><div style="font-size:15px;padding:5px;">';
                    $HTML2.=''.$ITEM['titolo'].'<br><font style="font-size:12px;">Donatori: '.$ITEM['tot_donatori'].'</span></div></td>';
                    
                    //$HTML2.='<td style="width:15px;">';
                    //$HTML2.="";
                    //$HTML2.="</td>";
                    $HTML2.='<td style="width:80px;" align="right">';
                    $HTML2.="&euro;. ".$ITEM['importo']."&nbsp;";
                    $HTML2.="</td>";
                    
                    $HTML2.='<td style="width:180px;">';
                    $HTML2.='<img src="'.$a['TEMPLATE_DIR_WWW'].'_img/'.$ITEM['modalita_donazione'].'-32.png" width="25" height="25" border="0">';
                    $HTML2.=" ".$ITEM['modalita_donazione'];
                    $HTML2.="</td>";
                    $HTML2.="</tr>";
                }
                }
                
                    $HTML2.="<tr>";
                    $HTML2.="<td>&nbsp;</td>";
                    $HTML2.="<td>&nbsp;</td>";
                     $HTML2.='<td bgcolor="#ffffcc" style="width:15px;">';
                    $HTML2.="&euro;";
                    $HTML2.="</td>";
                    $HTML2.='<td bgcolor="#ffffcc" style="width:35px;" align="right">';
                    $HTML2.="".$IMPORTO_TOT."&nbsp;";
                    $HTML2.="</td>";
                   
                    $HTML2.="<td>&nbsp;</td>";
                    
                    $HTML2.="</tr>";
                    $HTML2.="</table>";
                
                //$HTML2.='<br><br><br><HR><br><br>';

                
                
                
                
                
                
            if($loop_donatori)
            {
                $loop_count="1";
                foreach($loop_donatori AS $I)
                {
                    
                    
                //1. Genero il record in donazione, ma solo se non esiste gi� un record per l'utente
                //se esiste lo aggiorno.
                
                $ID_UTENTI=$I['id'];
                $W="WHERE id_utenti='".$ID_UTENTI."' AND annomese='".$annomese."'";

                $esiste_donazione=dbAction::_record(array(
                'tab'=>$TAB_donazioni,
                'set_w'=>$W,
                //'echo'=>true
                ));
                    
                //Preparo gli importi
                //$I['importo_selezionato'];
                //pr($I);
                        
                    //NEL CASO IN CUI NON ESSITE UN IMPORTO SELEZIONATO
                    // da office, dovrei averlo impedito ma non si sa mai...
                    //Prelevo il primo valore dall'azienda:
                    if(empty($I['importo_selezionato']))
                    {
                        $importi_azienda=$I['REL'][$TAB_aziende]['importi'];
                        //controllo anche che gli importi dell'azienda non siano vuoti
                        //per evitare errori in fase di generazione degli importi
                        //
                        if(!empty($importi_azienda))
                        {
                        $importi_azienda=explode(",", $importi_azienda);
                        $I['importo_selezionato']=$importi_azienda[0];
                        }
                        else
                        {
                            //SE FOSSE VUOTO
                            //DOVREI INVIARE UN ALLERT()
                            //E COMUNQUE METTO UN VALORE DI DEFAULT:
                            $momento_presente=time();
                            $URL_AZIENDA=URL_OFFICE.'tab/aziende/upd/'.$I['REL'][$TAB_aziende]['id'];
                            
                            $msg ='ATTENZIONE:<br>non &egrave; stato indicato nessun importo per la azienda: ';
                            $msg.='<a href="'.$URL_AZIENDA.'/">';
                            $msg.=$I['REL'][$TAB_aziende]['titolo'].'</b> nella sezione OFFICE.<br><br>';
                            $msg.='Per questo motivo la funzione che crea le donazioni, ';
                            $msg.='lanciata il:'.date('d M Y',$momento_presente).' ';
                            $msg.='alle ore: '.date('H:i:s',$momento_presente).'<br> ';
                            $msg.='ha indicato per questa donazione:<br>';
                            $msg.='<div style="padding:5px;margin:3px;background-color:#efefef;">';
                            $msg.='Riferimento utente: ';
                            $msg.='<a href="'.URL_OFFICE.'tab/utenti/upd/'.$I['id'].'/">'.$I['nome'].' '.$I['cognome'].'</a>';
                            $msg.='<br>';
                            $msg.='Riferimento periodo: ';
                            $msg.='Anno: '.$anno.' - Mese: '.$mese;
                            $msg.='</div>';
                            $msg.='<br><br>';
                            $msg.='Un importo di default di 10 euro al mese.';
                            $msg.='<br><br>';
                            $msg.='Si richiede una verifica ed uncontrollo.';
                            $msg.='<br><br>';
                            $msg.='Se necessario, dopo la verifica della corretta assegnazione degli ';
                            $msg.='importi nella relativa <a href="'.$URL_AZIENDA.'">scheda azienda</a> ';
                            $msg.='Effettuare una riassegnazione degli importi delle donazioni.<br><br>';
                            
                            
                            $DATI_4SEND_EMAIL=array(
                                'id_utenti_cat'=>$a['_CAT_UTENTI_']['unora'],
                                'msg'=>$msg,
                                ); 
                            _EMAIL_ALERT::_send($a,$DATI_4SEND_EMAIL);
                            $importi_azienda="IMPORTO DEFAULT di 10 euro|euro|10";
                        }
                    }
                
                $I['importo_selezionato']=explode("|",$I['importo_selezionato']);
                //pr($I['importo_selezionato']);
                    if($I['importo_selezionato'][1]=="euro")
                    {
                        //Importo in euro
                        $euro=$I['importo_selezionato'][2];
                        $ore="0";
                        $valore_unora="0";
                        $euro_mese=format_euro($I['importo_selezionato'][2]);
                    }
                    else
                    {
                        //Importo in ore
                        $euro="0";
                        $ore=$I['numero_ore_mese'];
                        $valore_unora=$I['importo_selezionato'][2];
                        $euro_mese=  format_euro($I['numero_ore_mese']*$I['valore_unora']);
                        
                        
                    }
                    
                    $DATI_donazione=array(
                    'annomese'=>$annomese,
                    'mese'=>$mese,
                    'anno'=>$anno,
                    'id_doc_donazioni'=>$ID_DOC_Donazioni,
                    'id_utenti'=>$I['id'],
                    'id_aziende'=>$I['id_aziende'],
                    'id_admin'=>$id_admin,
                    
                    'euro'=>$euro,
                    'ore'=>$ore,
                    'valore_unora'=>$valore_unora,
                    'deduzione'=>$I['deduzione'],
                    'detrazione'=>$I['detrazione'],
                    
                    'id_onp50x100'=>$I['onp50x100'],
                    'euro_mese'=>$euro_mese,
                            
                    'data'=>time()
                    );
                        
                    
                    
                        
                    
                        
                    //pr($esiste_donazione);
                        
                        
                    /*
                     * 
                     * C R E A Z I O N E  
                     * 
                     * i n s e r i m e n t o / m o d i f ic a  
                     * 
                     * D A T I donazioni
                     * 
                     */
                    
                    if($esiste_donazione)
                    {
                    //DONAZIONE GIA' PRESENTE ----> UPDATE
                       
                        $UPD=dbAction::_update(array('tab'=>$TAB_donazioni,'data'=>$DATI_donazione,'id'=>$esiste_donazione['id'],'echo'=>false)); 
                        /*
                            In questo caso prima cancello tutti i record relativi
                        */
                        
                        $del2=dbAction::_delete(array('tab'=>$TAB_donazioni_onp,'where'=>"WHERE id_donazioni='".$esiste_donazione['id']."'"));
                        
                    $ID_DONAZIONE=$esiste_donazione['id'];
                    }
                    else
                    {
                    //DONAZIONE NON PRESENTE  ----> INSERT
                        
                        $INS=dbAction::_insert(array('tab'=>$TAB_donazioni,'data'=>$DATI_donazione,'echo'=>false));
                        
                    $ID_DONAZIONE=$INS;
                    }
                    
                    
                    //$HTML3='';
                    
                    if($loop_count=="1")
                        {
                            $HTML3='<page><div style="background-color:#FF9900;"><h1 style="color:#ffffff;"> &nbsp; Lista Donazioni - '.$lang['mesi'][$DATA_doc['mese']].' '.$DATA_doc['anno'].' </h1></div><br>';
                            $HTML3.='<table cellspacing="2" cellpadding="5">';
                        }
                        
                    
                    
                    if($loop_count=="1" || (is_natural_number($MEM_AZIENDA['id']) && $MEM_AZIENDA['id']!=$I['REL'][$TAB_aziende]['id']) )
                    {
                        if($loop_count!="1")
                        {
                            $HTML3.='</table>';
                            $HTML3.='</page>';
                            $HTML3.='<page>';
                            $HTML3.='<table cellspacing="2" cellpadding="5">';
                            
                        }
                        
                        
                        $count_donazioni_azienda='0';
                        
                        
                        //Anno: 
                
                        //Se non sono alla prima azienda inserisco un 
                        //interruzione di pagina in modo da ordinare 
                        //meglio i dati delle donazioni per azienda
                        
                        
                    $HTML3.='<tr bgcolor="#ffffff">';
                    $HTML3.='<td colspan="4">';
                    $HTML3.='<div style="padding:5px;font-size:18px;">';
                    $HTML3.='<b>'.$I['REL'][$TAB_aziende]['titolo'].'</b>';
                    $HTML3.='</div>';
                    
                    
                    $HTML3.='</td>';
                    $HTML3.='</tr>';
                    
                $HTML3.='<tr bgcolor="#afafaf">';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;">Donatore</div></td>';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;">Importo Mese</div></td>';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;text-align:center;">Deduzione</div></td>';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;text-align:center;">Detrazione</div></td>';
                $HTML3.='</tr>';
                
                    }
                    $count_donazioni_azienda++;
                    $MEM_AZIENDA=$I['REL'][$TAB_aziende];
                    
                    
                    
                    
                    
                        if($I['tr_class']=="uno")
                        {
                        $bg1="#ffffcc";
                        $bg2="#f0f0d3";
                        }
                        else
                        {
                        $bg1="#fff0b8";
                        $bg2="#ebddb5";
                        } 
                    
                    //DOC donatori
                    $HTML3.='<tr bgcolor="'.$bg1.'">';
                    /*
                    $HTML3.='<td style="width:130px;" valign="top">';
                    $HTML3.='<div style="background-color:#ffffcc;padding:5px;">';
                    $HTML3.="Donatore:<br><b>".$I['nome']." ".$I['cognome'].'</b>';
                    $HTML3.="</div>";
                    $HTML3.="</td>";
                    $HTML3.='<td style="width:150px;" valign="top">';
                    $HTML3.='<div style="background-color:#ffffcc;padding:5px;">';
                    $HTML3.="Azienda:<br>".$I['REL'][$TAB_aziende]['titolo'];
                    $HTML3.="</div>";
                    $HTML3.="</td>";
                    */
                    
                $HTML3.='<td style="width:440px;">';
                //$HTML3.='<div style="color:#941b20;padding:5px;font-size:11px;">';
                $HTML3.='<div style="padding:5px;">';
                $HTML3.=$count_donazioni_azienda.') <img src="'.rootWWW.'_template/_office/_img/stato-utente-1.png" width="18" height="18" >';
                $HTML3.='&nbsp;';
                
                $HTML3.='<a style="color:#941b20;text-decoration:none;" href="'.URL_OFFICE.'tab/'.$TAB_utenti.'/upd/'.$I['REL'][$TAB_utenti]['id'].'/">';
                $HTML3.='<b>'.$I['nome'].'</b> ';
                $HTML3.=' <b>'.$I['cognome'].'</b>';
                $HTML3.='</a>';
                //$HTML3.='<br>'.$I['REL'][$TAB_aziende]['titolo'];
                $HTML3.='</div>';
                //$HTML3.='</div>';
                $HTML3.='</td>';
                $HTML3.='<td style="width:130px;">';
                $HTML3.='<img src="'.rootWWW.'_template/_office/_img/euro-21.png">';
                $HTML3.=' &nbsp;'.$euro_mese.' Euro';
                $HTML3.='</td>';

                    /*
                    $HTML3.='<td style="width:110px;" valign="top">';
                    $HTML3.='<div style="background-color:#ffffcc;padding:5px;">';
                    $HTML3.="Importo:<br>Euro/Mese:".$euro_mese."";
                    $HTML3.="</div>";
                    $HTML3.="</td>";
                    */
                
                
                
                                    //Set off:
                    $img_deduzione=rootWWW.'_template/_office/_img/sel-off-16.png';
                    $img_detrazione=rootWWW.'_template/_office/_img/sel-off-16.png';
                    //Set on, if active:
                    if($I['deduzione']=="1"){$img_deduzione=rootWWW.'_template/_office/_img/sel-16.png';}
                    if($I['detrazione']=="1"){$img_detrazione=rootWWW.'_template/_office/_img/sel-16.png';}
                    
                $HTML3.='<td align="center" style="width:80px;">';
                $HTML3.='<img src="'.$img_deduzione.'">';
                $HTML3.='</td>';
                $HTML3.='<td align="center" style="width:80px;">';
                $HTML3.='<img src="'.$img_detrazione.'">';
                $HTML3.='</td>';

                
                
                $HTML3.="</tr>";
                    
                    
                    
                    
                    
                    
                    
                    
                    //2.  $TAB_donazioni_onp
                    //Duplicato per ognuna delle ONP assegnate all'utente
                    //
                    /*
                     *
                     *  Qui vanno chieirte delle cose
                     *  con unora:
                     *
                     *  ad esempio:
                     *
                     *  qual'� il criterio con cui una ONP
                     *  viene inclusa nella donazione?
                     *
                     *  Ci sono delle modalit� di adesione della ONP che
                     *  vanno registrate, come la data di iscrizione
                     *  che in alcune modalit�
                     *  dovrebbe prevedere l'accesso alle donazioni
                     *  dio tutte le aziende che aderiscono
                     *  dopo la data di iscrizione della ONP
                     *
                     *  questo per i profili FREE
                     *
                     *  e per i profili a SPOT?
                     *  rimane visibile sempre e solo
                     *  dentro una singola AZIENDA
                     *
                     *  Di fatto il profilo SPOT e
                     *  quello free sono la stessa cosa?
                     *
                     *  Si direbbe di si
                     *
                     *  perch� per entrare una ONP che non vuole pagare la FEE di 5000Euro
                     *  deve portare una azienda e quindi di fatto � come
                     *  quando l'azienda presenta una sua ONP preferita?
                     *
                     *  Anche se l'origne � un po' diversa..
                     *
                     *  Approfondire...
                     *
                     *
                     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                     * 
                     *  S I T U A Z I O N E   A T T U A L E 
                     *
                     *  Pewr il momento ho solo la differenza tra ONP normale e ONP Spot
                     *  se � spot viene aggiunta solo agli utenti della relativa azienda 
                     *
                     *
                     */
                    /**/
                
                
                        /*
                         * AGGIUNTA DEL CAMPO 'onlus_escluse'
                         * da verificare se ci sono 
                         * altre parti che chiamano questa funzione
                         * intanto, in attesa del restyling di queste funzioni
                         * metto un controllo qui:
                         * $I['REL'][$TAB_aziende]
                         */
                        $ARRAY_ONLUS_ESCLUSE=array();
                        if($I['REL'][$TAB_aziende]['onlus_escluse']){
                        $ARRAY_ONLUS_ESCLUSE=explode(",", $I['REL'][$TAB_aziende]['onlus_escluse']);}
                         
                         
                    $OPN=dbAction::_loop(array(
                        'tab'=>$TAB_onlus,
                        'where'=>"WHERE (id_aziende='0' OR id_aziende='".$I['id_aziende']."') AND visibile='1' AND stato='1' ",
                        //'orderby'=>"ORDER BY ",
                        ));
                    
                    if($OPN)
                    {
                        $tot_OPN=count($OPN);
                        $HTML3.='<tr bgcolor="'.$bg1.'">';
                        $HTML3.='<td colspan="4">';
                        $HTML3.="Onp (".$tot_OPN."):<br><table>";
                        /*
                         * Totale delle onp che hanno
                         * diritto alla donazione di questo utente
                         */
                        //pr("TOTALE ONP: ".$tot_OPN);
                        //SE IL TOTALE E' MAGGIORE DI UNO
                        if($tot_OPN>1)
                        {
                        //verifico se esiste una ONP al 50%
                            if($I['onp50x100']!="0" && !empty($I['onp50x100']))
                            {
                                //SI ESISTE! -> Calcolo la percentuale sul totale meno UNO.
                                //E sul rimanente 50% ...
                                //$PERCENTUALE_FAIR=round( (100 / ($tot_OPN-1)) / 2 ,5);
                                $PERCENTUALE_FAIR=round(100 / ($tot_OPN-1),5);
                            }
                            else
                            {
                                //NO niente 50% -> Calcolo la percentuale sul totale.
                                $PERCENTUALE_FAIR=round(100 / ($tot_OPN),5);
                            }
                        }
                        else
                        {
                            //SOLO UNA ONP DISPONIBILE
                            //NON IMPORTA QUINDI IL VALORE PERCENTUALE è sempre 100%
                            $PERCENTUALE_FAIR="100";
                        }
                        
                    $DATI_onp=array();
                    $conta_onp="1";
                    
                    
                    
                    foreach($OPN AS $onp)
                    {
                    //Aggiungo controllo se presente in array $I['REL'][$TAB_aziende]
                    if(!in_array($onp['id'], $ARRAY_ONLUS_ESCLUSE)){
                        
                        //Setto i dati sulla donazione tutti a fari
                        //ovvero distribuzione in parti uguali della donazione
                        //
                        $fair=$PERCENTUALE_FAIR;
                        $p50x100="0";
                            
                            //MA SE TROVO UNA ONP al 50% cambio il valore per quella ONP
                            //E solo nel caso ci sia piu di una ONP
                            //
                            if($I['onp50x100']>0 && !empty($I['onp50x100']))
                            {
                                if($I['onp50x100']==$onp['id'] && $tot_OPN>1)
                                {
                                    $fair="50";
                                    $p50x100=$onp['id'];
                                    
                                    
                                    $euro_mese_diviso_cento=$euro_mese/100;
                                    //floor($some_number*100)/100
                                    $importo_onp=$euro_mese_diviso_cento*$fair;
                                    $importo_onp=floor($importo_onp*100)/100;
                                    $importo_onp=number_format($importo_onp,2);
                                    //number_format(98,2);
                            
                                }
                                else
                                {
                                    //DIVIDO PER DUE GLI ALTRI PER TOGLIERE IL VALORE ONP a 50%
                                    $euro_mese_diviso_cento=$euro_mese/100/2;
                                    //floor($some_number*100)/100
                                    $importo_onp=$euro_mese_diviso_cento*$fair;
                                    $importo_onp=floor($importo_onp*100)/100;
                                    $importo_onp=number_format($importo_onp,2);
                                    //number_format(98,2);
                            
                                }
                            
                            }
                            else
                            {
                            $euro_mese_diviso_cento=$euro_mese/100;
                            //floor($some_number*100)/100
                            $importo_onp=$euro_mese_diviso_cento*$fair;
                            $importo_onp=floor($importo_onp*100)/100;
                            $importo_onp=number_format($importo_onp,2);
                            //number_format(98,2);
                            }
                            
                            
                            $DATI_onp[]=array(
                                'id_donazioni'=>$ID_DONAZIONE,
                                'id_onlus'=>$onp['id'],
                                'fair'=>$fair,
                                '50x100'=>$p50x100,
                                'annomese'=>$annomese,
                                'importo'=>$importo_onp
                                );
                            
                            
                            
                    //DOC info
                    $bgcolor="#efefef";$color="#000000";
                        //controllo se il contaONP è paro:
                        if($conta_onp % 2=='0'){$bgcolor="#ffffcc";$color="#000000";}
                        elseif($fair=="50"){$bgcolor="#FF9900";$color="#ffffff";}
                    $HTML3.='<tr bgcolor="'.$bgcolor.'">';
                    /*
                    $HTML3.='<td>';
                    $HTML3.='<span style="color:'.$color.';">'.$conta_onp.'</span>';
                    $HTML3.='</td>';
                    */
                    $HTML3.='<td style="width:210px;">';
                    $HTML3.='<span style="color:'.$color.';">'.$onp['titolo'].'</span>';
                    $HTML3.='</td>';
                    
                    $HTML3.='<td>';
                    $HTML3.='<span style="color:'.$color.';">'.$fair.'%</span>';;
                    $HTML3.='</td>';
                    
                    $HTML3.='<td align="right">';
                    $HTML3.='<span style="color:'.$color.';">&euro;.'.$importo_onp.' </span>';;
                    $HTML3.='</td>';
                    
                    $HTML3.="</tr>";
                    $HTML3.="";
                    
                            $tot_importo_onp=  format_euro($tot_importo_onp+$importo_onp);
                    $conta_onp++;        
                    }
                    }
                    
                    //Onp Totale Importi:$tot_importo_onp
                    $HTML3.='<tr><td>&nbsp;</td><td>&nbsp;</td>';
                    $HTML3.='<td align="right">&euro;.'.$tot_importo_onp.'</td>';
                    $HTML3.='</tr>';
                    $HTML3.="</table></td></tr>";
                    }
                    //pr($DATI_onp);
                    
                    /*
                     *
                     *  Prima ho settato i dati adesso li inserisco
                     *
                    */
                    
                    foreach($DATI_onp AS $D)
                    {
                        /*
                         * Ricreo direttamente senza il controllo di esistenza perch�
                         * prima le ho cancellate tutte durante l'update 
                         * in questo modo ad ogni aggiornamento li ricreo.
                         */
                        
                        $INS_onp=dbAction::_insert(array('tab'=>$TAB_donazioni_onp,'data'=>$D));
                        
                    }
                    
                    
                    /**/
                    
                    
                $loop_count++;
                }
                $HTML3.="</table></page>";
            }
            
            
            
            
            
            
            
                //COMPLETO IL SALVATAGGIO DEL DOC
                $settings['doc_type']="_donazioni";
                $settings['file_name']=$ID_DOC_Donazioni;
                $settings['intestazione']=$HTML1;
                $settings['dati']=$HTML2.$HTML3;
                $a=_DOC::_make($a,$settings);
                
        htmlHelper::setFlash("msg","Elenco Donazioni Aggiornato");
        }
    
        
    return $a;    
    }
    
    
    
    
    function doit_ordini_pagamento($a)
    {
        global $lang;
        //pr($lang);
        global $TAB_doc_ordini_pagamento;
        
        global $TAB_doc_donazioni;
        
        global $TAB_donazioni;
        global $TAB_donazioni_info;
        global $TAB_donazioni_onp;
        
        global $TAB_utenti;
        global $TAB_utenti_cat;
        
        global $TAB_onlus;
        global $TAB_aziende;
        
        //pr($a['USER']['id']);
        $settings=$a['SETTINGS'];
        
        
        
        //if(is_natural_number($a['USER']['id']))
        if(is_natural_number($a['USER']['id']) && 
                (
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] || 
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']
                ) 
           )
        {
        /* * * * * * * * * * * * * * * * 
         *
         *
         *  Preparo tutte le variabili
         *  
         *
        * * * * * * * * * * * * * * * */
                
            //
            $anno=empty($settings['anno'])?date('Y'):$settings['anno'];
            $mese=empty($settings['mese'])?date('m'):$settings['mese'];
            $annomese=$anno.$mese;
              
            
            
            $anno_e_mese[0]=substr($annomese,0,4);
            $anno_e_mese[1]=$lang['mesi'][(int)substr($annomese,4,2)];
                        
            
            
                // Last Day of the Mont:
                // -- > date("t");
                
            //
            $id_admin=$a['USER']['id'];
            /*
             * 1. Cercare ordine di pagamento già presente, ed in caso aggironarlo.
             * 2. Cercare Doc_Donazione attivo e lavorare su quello
             *      Diviso per ogni azienda: 
             *      2.1 quindi prendere tutte le donazioni
             *      2.2 salvarle in un array per "congelare" una foto della situazione
             *      2.3 calcolare i totali degli importi da bonificare
             *      2.4 prepararee, report, doc e inviarli via email ai ref_azienda
             *      2.5 conlcusione... si rimane in attesa delle comunicazioni di bonifici.
             */
            // 1. Verifico se esiste già un ordine di pagamento
            /*
             * Prelevo il documento di donazioni corrente annomese
             * 
             */
            $DOC_Donazioni=dbAction::_record(array('tab'=>$TAB_doc_donazioni,'value'=>$annomese,'field'=>'annomese'));
            //pr($DOC_Donazioni);
            
            //Fermo tutto se non c'è un DOC di Donazioni
            if(!$DOC_Donazioni)
            {
                htmlHelper::setFlash("err","Errore:<br>non &egrave; stato possibile effettuare la<br>Creazione del documento di Ordine di Pagamento<br>Non &grave presente un documento di Donazioni");
                return $a;
            }
            
            
            //Esiste un documento di donazione
                //Eseguo import anche delle relative donazioni
                //e delle informazioni sulle onlus.
                $loop_donazioni=dbAction::_loop(array('tab'=>$TAB_donazioni,'where'=>"WHERE id_doc_donazioni='".$DOC_Donazioni['id']."'"));
                foreach ($loop_donazioni AS $I)
                {
                    $I['REL'][$TAB_donazioni_onp]=dbAction::_loop(array('tab'=>$TAB_donazioni_onp,'where'=>"WHERE id_donazioni='".$I['id']."'"));
                    $loop_donazioni_new[]=$I;
                    
                    unset($I['REL'][$TAB_aziende]);
                    $loop_donazioni_aziende[$I['id_aziende']][]=$I;
                }
                $loop_donazioni=$loop_donazioni_new;
                $TOT_DONAZIONI=count($loop_donazioni);
                //pr($loop_donazioni);
            
                //A fronte di un Doc_donazioni ci sono 'n' doc_ordine_pagamenti 
                //dove 'n' è il numero delle aziende attive 
                ////verso le quali si deve effettuare un ordine di pagamento 
                //Adesso verifico se i doc_ordine_pagamenti esiste 
                //e quindi se è da creare o da aggiornare.
                
                // Le aziende attive vengono prese dal DOC_DONAZIONI e non dall'elenco 
                // delle aziende del db perchè questo potrebbe nel frattempo essere cambiato
                
                $ELENCO_AZIENDE=json_decode($DOC_Donazioni['dati_aziende'],true);
                //pr($ELENCO_AZIENDE);
                
                
                //Seguo il loop delle aziende per verificare 
                //e poi creare o modificare il doc_ordine _pagamenti
                foreach ($ELENCO_AZIENDE AS $I)
                {
                    
                    $DONAZIONI_LOOP_AZIENDA=$loop_donazioni_aziende[$I['id']];
                    
                    //pr($I['id']);
                    //pr($I);
                    $DOC_ordine_pagamenti=dbAction::_record(array(
                    'tab'=>$TAB_doc_ordini_pagamento,
                    'set_w'=>"WHERE id_aziende='".$I['id']."' AND annomese='".$annomese."' ",
                    //'echo'=>true
                    ));
                    
                    
                    //unset($DONAZIONI_LOOP_AZIENDA['REL'][$TAB_aziende]);
                    //pr($DONAZIONI_LOOP_AZIENDA['REL'][$TAB_aziende]);
                    $DATI_DOC_ordine_pagamenti=array(
                    'id_utenti'=>$id_admin,
                    'id_aziende'=>$I['id'],
                    'data_creazione'=>time(),
                    'data_aggiornamento'=>'0',
                    'n_aggiornamenti'=>'0',
                    'anno'=>$DOC_Donazioni['anno'],
                    'mese'=>$DOC_Donazioni['mese'],
                    'annomese'=>$DOC_Donazioni['annomese'],
                    'donazioni'=>  json_encode($DONAZIONI_LOOP_AZIENDA),
                    'tot_donazioni'=>$I['tot_donatori'],
                    'importo'=>$I['importo'],
                    
                    );
                        //pr($DATI_DOC_ordine_pagamenti);
                    if($DOC_ordine_pagamenti)
                    {
                        //Esiste, devo aggiornare
                        //aggiorno alcuni dati
                        $DATI_DOC_ordine_pagamenti['data_creazione']=$DOC_ordine_pagamenti['data_creazione'];
                        $DATI_DOC_ordine_pagamenti['data_aggiornamento']=time();
                        $DATI_DOC_ordine_pagamenti['n_aggiornamenti']=$DOC_ordine_pagamenti['n_aggiornamenti']+1;
                        
                        $UPD_Doc=dbAction::_update(array(
                        'tab'=>$TAB_doc_ordini_pagamento,
                        'data'=>$DATI_DOC_ordine_pagamenti,
                        'id'=>$DOC_ordine_pagamenti['id'],
                        //'echo'=>true
                        ));
                        
                        
                        $ID_DOC_Ordine_Pagamenti=$DOC_ordine_pagamenti['id'];
                    }
                    else
                    {
                        //Non essite devo creare
                        
                        $INS_Doc=dbAction::_insert(array(
                        'tab'=>$TAB_doc_ordini_pagamento,
                        'data'=>$DATI_DOC_ordine_pagamenti,
                        
                        ));
                        
                        
                        $ID_DOC_Ordine_Pagamenti=$INS_Doc;
                    }
                
                
            
                
                $HTML1 ="<br><br>";
                
                $HTML1.="Documento creato il: ";
                //$HTML1.=date("d-m-Y H:i:s",$DATI_DOC_ordine_pagamenti['data_creazione'])."<br>";
                
                $HTML1.=date("d",$DATI_DOC_ordine_pagamenti['data_creazione'])." ";
                $HTML1.=substr($lang['mesi'][date("n",$DATI_DOC_ordine_pagamenti['data_creazione'])],0,3)." ";
                $HTML1.=date("Y",$DATI_DOC_ordine_pagamenti['data_creazione'])." ";
                $HTML1.=' alle ore: '.date("H:i s",$DATI_DOC_ordine_pagamenti['data_creazione'])." ";
                
                
                
                if($DATI_DOC_ordine_pagamenti['data_aggiornamento']!="0"){
                $HTML1.="<br>";
                $HTML1.="Ultima modifica del: ";
                //.date("d-m-Y H:i:s",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])."<br>";
                $HTML1.=date("d",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])." ";
                $HTML1.=substr($lang['mesi'][date("n",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])],0,3)." ";
                $HTML1.=date("Y",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])." ";
                $HTML1.=' alle ore: '.date("H:i s",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])." ";
                
                
                $HTML1.="<br>";
                $HTML1.="Numero di aggiornamenti: <b>".$DATI_DOC_ordine_pagamenti['n_aggiornamenti']."</b><br>";
                }
                $HTML1.="<b>Periodo Donazioni</b><br>  ";
                $HTML1.="Anno: ".$DATI_DOC_ordine_pagamenti['anno']." Mese: ".$DATI_DOC_ordine_pagamenti['mese']."<br>";
                    $HTML1.='<table>';
                    $HTML1.='<tr><td><a href="'.rootWWW.'"><img src="'.rootWWW.'icona-mini.png" width="15" height="15" border="0"></a></td>';
                    $HTML1.='<td><b><a style="text-decoration:none;color:#941b20;" href="'.rootWWW.'">UNORA.ORG</a></b></td></tr></table>';
                $HTML1.="<br><br>  ";
                
                
                
                
                
                /* * * * * * * * * * * * * * * * * * * * * * * * *
                 *
                 * Prima pagina con i dati di riepilogo
                 *  
                 * * * * * * * * * * * * * * * * * * * * * * * * */
                
                $HTML2 ='';
                
                $IMPORTO=$DATI_DOC_ordine_pagamenti['importo'];
                
                $HTML2.='<div style="font-size:28px;">';
                $HTML2.='<b>Ordine di Pagamento</b> <br>';
                $HTML2.='Per le Donazioni del Periodo: ';
                $HTML2.='<b style="color:#941b20">'.$anno_e_mese[1].' '.$anno_e_mese[0].'</b><br>';
                $HTML2.='</div>';
                $HTML2.='<br><br>';
                $HTML2.='<div style="width:690px;padding:20px;background-color:#ffffcc;border:3px solid orange;border-radius:9px;-moz-border-radius:9px;-webkit-border-radius:9px;">';
                $HTML2.='<table>';
                $HTML2.='<tr>';
                $HTML2.='<td>';
                $HTML2.='<img src="'.rootWWW.'_template/_office/_img/euro-128.png" width="90" height="90" >';
                $HTML2.='</td>';
                
                $HTML2.='<td>';
                $HTML2.='<div style="color:orange;font-size:65px;padding:20px;">';
                $HTML2.='<b>';
                $HTML2.='&euro;.'.number_format($IMPORTO,'2',',','.');
                $HTML2.='</b>';
                $HTML2.='</div>';
                
                $HTML2.='</td>';
                $HTML2.='</tr>';
                $HTML2.='</table>';
                $HTML2.='</div>';
                
                $HTML2.='<br><br>';
                
                $HTML2.='<div style="font-size:18px;padding:20px 20px 0px 20px;">';
                $HTML2.='Totale donazioni: '.$DATI_DOC_ordine_pagamenti['tot_donazioni'];
                $HTML2.='</div>';
                $HTML2.='<div style="font-size:18px;padding:0px 20px 0px 20px;">';
                $HTML2.='Ultma modifica di: '.$a['USER']['nome'];
                $HTML2.=' '.$a['USER']['cognome'].' <i style="color:#909090;">('.$a['USER']['REL'][$TAB_utenti_cat]['nome'].')</i>';
                $HTML2.='</div>';
                
                
                
                //$HTML2.='<pre>'.print_r($DATI_DOC_ordine_pagamenti, true).'</pre>';
                
                
                
                
                
                
                
                
                
                
                
                
                /* * * * * * * * * * * * * * * * * * * * * * * * *
                 *
                 * Seconda pagina con i dati delle donazioni
                 *  
                 * * * * * * * * * * * * * * * * * * * * * * * * */
                
                
                $HTML3='<page>';
                //pr($DONAZIONI_LOOP_AZIENDA);
                if($DONAZIONI_LOOP_AZIENDA)
                {
                //$HTML3.='<table cellspacing="0" cellpadding="1"><tr><td>';
                $HTML3.='<table cellspacing="2" cellpadding="5">';
                $HTML3.='<tr bgcolor="#afafaf">';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;">Donatore</div></td>';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;">Importo Mese</div></td>';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;text-align:center;">Deduzione</div></td>';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;text-align:center;">Detrazione</div></td>';
                $HTML3.='</tr>';
                
                
                foreach ($DONAZIONI_LOOP_AZIENDA AS $Idonazioni)
                {
                    
                    if($Idonazioni['tr_class']=="uno")
                    {
                    $bg1="#ffffcc";
                    $bg2="#f0f0d3";
                    }
                    else
                    {
                    $bg1="#fff0b8";
                    $bg2="#ebddb5";
                    }
                    
                    
                $HTML3.='<tr bgcolor="'.$bg1.'">';
                $HTML3.='<td style="width:440px;">';
                //$HTML3.='<div style="color:#941b20;padding:5px;font-size:11px;">';
                $HTML3.='<div style="padding:5px;">';
                
                $HTML3.='<img src="'.rootWWW.'_template/_office/_img/stato-utente-1.png" width="18" height="18" >';
                $HTML3.='&nbsp;';
                
                $HTML3.='<a style="color:#941b20;text-decoration:none;" href="'.URL_OFFICE.'tab/'.$TAB_utenti.'/upd/'.$Idonazioni['REL'][$TAB_utenti]['id'].'/">';
                $HTML3.='<b>'.$Idonazioni['REL'][$TAB_utenti]['nome'].'</b> ';
                $HTML3.=' <b>'.$Idonazioni['REL'][$TAB_utenti]['cognome'].'</b>';
                $HTML3.='</a>';
                $HTML3.='</div>';
                //$HTML3.='</div>';
                $HTML3.='</td>';
                $HTML3.='<td style="width:130px;">';
                $HTML3.='<img src="'.rootWWW.'_template/_office/_img/euro-21.png">';
                $HTML3.=' &nbsp;'.$Idonazioni['euro_mese'].' Euro';
                $HTML3.='</td>';
                    
                    //Set off:
                    $img_deduzione=rootWWW.'_template/_office/_img/sel-off-16.png';
                    $img_detrazione=rootWWW.'_template/_office/_img/sel-off-16.png';
                    //Set on, if active:
                    if($Idonazioni['deduzione']=="1"){$img_deduzione=rootWWW.'_template/_office/_img/sel-16.png';}
                    if($Idonazioni['detrazione']=="1"){$img_detrazione=rootWWW.'_template/_office/_img/sel-16.png';}
                    
                $HTML3.='<td align="center" style="width:80px;">';
                $HTML3.='<img src="'.$img_deduzione.'">';
                $HTML3.='</td>';
                $HTML3.='<td align="center" style="width:80px;">';
                $HTML3.='<img src="'.$img_detrazione.'">';
                $HTML3.='</td>';
                
                
                $HTML3.='</tr>';
                $HTML3.='<tr bgcolor="'.$bg2.'">';
                $HTML3.='<td colspan="4">';
                    $imp_sel=$Idonazioni['REL'][$TAB_utenti]['importo_selezionato'];
                    $imp_sel=explode("|",$imp_sel);
                    
                
                    //Tipo di importo selezionato
                    $HTML3.='<div style="padding:5px;color:#22a12c;">';
                    if($imp_sel[1]=="euro")
                    {
                    $HTML3.='<img src="'.rootWWW.'_template/_office/_img/euro-32.png" width="20" height="20">';
                    $HTML3.='&nbsp;';
                    $HTML3.='Importo Mensile selezionato: '.$imp_sel[0];
                    $HTML3.=' (Valore in euro: '.$imp_sel[2].')'; 
                    }
                    else
                    {
                    $HTML3.='<img src="'.rootWWW.'_template/_office/_img/ore-32.png" width="20" height="20">';
                    $HTML3.='&nbsp;';
                    //$HTML3.='Valore Euro di Un Ora: '.$imp_sel[2].' - ';
                    $HTML3.='Un Ora: &euro;.'.$Idonazioni['REL'][$TAB_utenti]['valore_unora'].' - ';
                    $HTML3.=' Ore/Mese donate: ';
                    $HTML3.=$Idonazioni['REL'][$TAB_utenti]['numero_ore_mese']; 
                    }
                    $HTML3.='</div>';
                    
                //$HTML3.=''.$Idonazioni['REL'][$TAB_utenti]['importo_selezionato'];
                
                $HTML3.='</td>';
                $HTML3.='</tr>';
                
                $HTML3.='<tr bgcolor="'.$bg1.'">';
                $HTML3.='<td colspan="4">';
                    
                    /* * * * * * * * * * * * * * * * * * * * * * * * * 
                     *  
                     *   L O O P   O N P 
                     *    --> start()
                     *  
                     * * * * * * * * * * * * * * * * * * * * * * * * */
                //$HTML3.='<pre>'.print_r($Idonazioni, true).'</pre>';
                if($Idonazioni['REL']['donazioni_onp']){
                    
                    $HTML3.='<div style="padding:3px;color:#afafaf;">';
                    $HTML3.='<b>ONP Selezionate</b>';
                    $HTML3.='</div>';
                    
                    $HTML3.='<table>';
                    //pr($Idonazioni['REL']['donazioni_onp']);exit;
                    foreach ($Idonazioni['REL']['donazioni_onp'] AS $ONP)
                    {
                        
                        if($ONP['tr_class']=="uno"){$BGColor="#FFECE3";}else{$BGColor="#F4E4DD";}
                        
                        $HTML3.='<tr bgcolor="'.$BGColor.'"';
                            if($ONP['50x100']!="0"){$HTML3.=' bgcolor="orange" style="font-size:18px;color:#ffffff;" ';}
                        $HTML3.='>';
                        $HTML3.='<td style="width:650px;">';
                        $HTML3.='<div style="padding:3px;">';
                        $HTML3.=''.$a['ONP_BY_ID'][$ONP['id_onlus']]['titolo'].'';
                        $HTML3.='</div>';
                        $HTML3.='</td>';
                        $HTML3.='<td style="width:90px;">';
                        $HTML3.='<div style="padding:3px;">';
                        $HTML3.=''.(float)$ONP['fair'].'%';
                        $HTML3.='</div>';
                        $HTML3.='</td>';
                        $HTML3.='</tr>';
                    }
                    $HTML3.='</table>';
                }
                $HTML3.='</td>';
                $HTML3.='</tr>';
                
                }
                $HTML3.='</table>';
                $HTML3.='</page>';
                
                //$HTML3.='</td></tr></table>';
                }
                
                //COMPLETO IL SALVATAGGIO DEL DOC
                $settings['doc_type']="_ordine_pagamenti";
                $settings['file_name']=$ID_DOC_Ordine_Pagamenti;
                $settings['intestazione']=$HTML1;
                $settings['dati']=$HTML2.$HTML3;
                $a=_DOC::_make($a,$settings);
                
                
                // Adesso, dopo che il doc è ronto
                // deve essere inviato via email ad ogni singolo
                // Utente Azienda.
                // 
                // Per evitare però che vengano effettuati
                // innumerevoli invii ogni vlta che viene 
                // aggiornato il file
                // Metto in schedule le email da inviare...
                // usando il metodo _schedule di class _EMAIL_ALERT 
                    
                //Loop dei refernti azienda della azienda attuale
                //$I['id']
                //pr($I);
                $ref_azienda=dbAction::_loop(array('tab'=>$TAB_utenti,'where'=>"WHERE id_aziende='".$I['id']."' AND id_utenti_cat='".$a['_CAT_UTENTI_']['ref_azienda']."'"));
                //pr($a['_CAT_UTENTI_']);
                
                    if($ref_azienda){
                        $TO=array();
                    foreach ($ref_azienda AS $rA)
                    {
                        $TO[]=$rA['email'];
                    }
                    //
                            
                        $ALLEGATO="_files/doc_ordini_pagamento/".$ID_DOC_Ordine_Pagamenti.".pdf";
                        
                        $CODICE_RIFERIMENTO="OrP_".$ID_DOC_Ordine_Pagamenti."_".$annomese;
                        
                        $URL_AT_OFFICE=URL_OFFICE."tab/".$TAB_doc_ordini_pagamento."/upd/".$ID_DOC_Ordine_Pagamenti."/";
                            
                        $TXT ='';
                        
                        $TXT.='<div class="'.time().'">';
                        $TXT.='<b>Ordine di pagamento</b><br>';
                        $TXT.='Il documento in allegato riporta il riepilogo ';
                        $TXT.=' completo dei dati relativi alle donazioni registrate ';
                        $TXT.='nel periodo '.$anno_e_mese[1].' '.$anno_e_mese[0].".";
                        $TXT.='<br>Puoi accedere subito alla <a href="'.$URL_AT_OFFICE.'">pagina relativa a questo conto</a><br>';
                        $TXT.='';
                        $TXT.='<br><br>';
                        $TXT.='</div>';
                        
                        $TXT.='<div style="border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;margin:5px;border:2px orange solid;background-color:#ffffcc;padding:15px;">';
                            
                            $TXT.='<div style="float:left;width:70px;" class="'.time().'">';
                            $TXT.='<img src="'.rootWWW.'_template/_office/_img/euro-64.png" border="0">';
                            $TXT.='</div>';
                            $TXT.='<div style="float:left;text-align:left;font-size:15px;" class="'.time().'">';
                            $TXT.='Totale dell\'importo da bonificare:<br>'; 
                            $TXT.='<b style="color:orange;font-size:25px;">'.$I['importo'].' Euro</b>';
                            $TXT.='</div>';
                            $TXT.='<div style="clear:both;"></div>';
                            
                        $TXT.='</div>';
                        
                        $TXT.='<br>';
                        $TXT.='';
                        $TXT.='';
                        //pr($DATI_DOC_ordine_pagamenti['importo']);
                        //pr($DONAZIONI_LOOP_AZIENDA);
                        
                        
                    $S=array(
                    'to'=>$TO,
                    //'from'=>'', //Se vuoto viene preso da $SMTP_DATA['mittente']
                    'oggetto'=>'Donazioni '.$anno_e_mese[1].' '.$anno_e_mese[0],
                    'testo'=>$TXT,
                    'allegato'=>$ALLEGATO,
                    'inviato'=>'0',
                    'codice_riferimento'=>$CODICE_RIFERIMENTO,
                    'id_utenti'=>$a['USER']['id'],
                    'data_creazione'=>time(),
                    //'dont_send_before'=>'',
                    );
                        
                        //pr($S);
                    
                    _EMAIL_ALERT::_schedule($a,$S);
                    }

                }//foreach ($ELENCO_AZIENDE AS $I)
                
                
                
                
                
                
        htmlHelper::setFlash("msg","Ordine di pagamento Aggiornato.");
        _CORE::redirect(array());
        exit;
        }
        
        
        return $a;
    }
    
    
    
    
    
    function doit_donazioni_onp($a)
    {
        
        
        
        global $lang;
        //pr($lang);
        global $TAB_doc_ordini_pagamento;
        global $TAB_doc_ordini_pagamento_onp;
        
        global $TAB_doc_donazioni;
        
        global $TAB_donazioni;
        global $TAB_donazioni_info;
        global $TAB_donazioni_onp;
        
        global $TAB_utenti;
        global $TAB_utenti_cat;
        
        global $TAB_onlus;
        global $TAB_aziende;
        
        //pr($a['USER']['id']);
        $settings=$a['SETTINGS'];
        
        
        
        //if(is_natural_number($a['USER']['id']))
        if(is_natural_number($a['USER']['id']) && 
                (
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] || 
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']
                ) 
           )
        {
            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * 
             *  Creo i record ed i documenti
             * 
             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
            $anno=empty($settings['anno'])?date('Y'):$settings['anno'];
            $mese=empty($settings['mese'])?date('m'):$settings['mese'];
            $annomese=$anno.$mese;
              
            
            
            $anno_e_mese[0]=substr($annomese,0,4);
            $anno_e_mese[1]=$lang['mesi'][(int)substr($annomese,4,2)];
                        
            
            
                // Last Day of the Mont:
                // -- > date("t");
                
            //
            $id_admin=$a['USER']['id'];
         
            
            if(isset($_GET['force']))
            {
                $del2=dbAction::_delete(array(
                    'tab'=>$TAB_donazioni_onp,
                    'where'=>"WHERE annomese='".$annomese."'"
                    
                    
                    ));
                
                
                
                
                
            }
            
            /*
             * 
             * PRIMA CONTROLLO CHE SIA STATO FATTO UN DOCUMENTO DI DONAZIONI
             * 
             */
            $DOC_Donazioni=dbAction::_record(array('tab'=>$TAB_doc_donazioni,'value'=>$annomese,'field'=>'annomese'));
            //pr($DOC_Donazioni);
            
            //Fermo tutto se non c'è un DOC di Donazioni
            if(!$DOC_Donazioni)
            {
                htmlHelper::setFlash("err","Errore:<br>non &egrave; stato possibile effettuare la<br>Creazione del documento di Ordine di Pagamento<br>Non &grave presente un documento di Donazioni");
                return $a;
            }
            
            
            
            
            
            
            //LOOP ONLUS
            $loop_onlus=dbAction::_loop(array('tab'=>$TAB_onlus,
                'where'=>"WHERE visibile='1' AND stato='1'" //LO STATO è ANCPRA DA INTEGRARE per le ONP e le AZIENDE
                ));
                foreach ($loop_onlus AS $onp_item)
                {
                    //
                    //Per ogni onlus dove trovo donazioni_onp registrate per il periodo dato
                    //creo / aggiorno il documento
                    //
                    $donazioni_onp=dbAction::_loop(array('tab'=>$TAB_donazioni_onp,'where'=>"WHERE annomese='".$annomese."' AND id_onlus='".$onp_item['id']."'"));
                    
                    $mysql_access = connect();
                    $ww=" WHERE annomese='".$annomese."' AND id_onlus='".$onp_item['id']."' ";
                    $s="SELECT SUM(importo) FROM ".$TAB_donazioni_onp." ".$ww;
                    $count=mysql_query($s,$mysql_access);
                    $totale_donaizoni_euro=mysql_fetch_array($count);
                    
                    pr($s);
                    pr("ONP --> ".$onp_item['titolo']." TOT: ".$totale_donaizoni_euro[0]);
                    pr(count($donazioni_onp));
                    //    
                    //
                    //
                    if($donazioni_onp)
                    {
                        /*
                         * OK DONAZIONI TROVATE
                         * devo creare il DOC
                         * 
                         */
                        
                        /*
                         * DEVO FARE IL LOOP delle donazioni_onp e ricavare l'importo totale
                         */
                        $TOT_DONAZIONI_ONP="0";
                        //pr($donazioni_onp);
                        foreach ($donazioni_onp AS $donazioni_onp_item)
                        {
                            //pr($donazioni_onp_item['importo']);
                            $TOT_DONAZIONI_ONP=  format_euro($TOT_DONAZIONI_ONP+$donazioni_onp_item['importo']);
                        }
                        
                        
                        
                        
                        
                            //
                        //
                        // Inserimento in TAB doc_ordini_pagamento_onp
                        // $TAB_doc_ordini_pagamento_onp
                        //
                            //
                        $DOC_ordini_pagamento_onp=dbAction::_record(array(
                            'tab'=>$TAB_doc_ordini_pagamento_onp,
                            'set_w'=>"WHERE annomese='".$annomese."' AND id_onlus='".$onp_item['id']."'",
                            ));
                        
                        /* * * * * * * * * * * * * * * * * * * * * *
                         * CASO 1. IL DOCUMENTO ESISTE
                         */
                            
                            //PREPARO I DATI
                            $DOC_DATA=array(
                                'id_utenti'=>$a['USER']['id'],
                                'id_onlus'=>$onp_item['id'],
                                'data_creazione'=>time(),
                                'anno'=>$anno,
                                'mese'=>$mese,
                                'annomese'=>$annomese,
                                'importo'=>$TOT_DONAZIONI_ONP,
                                
                                
                            );
                            
                        if($DOC_ordini_pagamento_onp)
                        {
                            /*
                             * AGGIORNO DATE (creazione e modifica) E N° AGGIORNAMENTI
                             */
                            //1. lascio la data di creazione come era
                            $DOC_DATA['data_creazione']=$DOC_ordini_pagamento_onp['data_creazione'];
                            //Aggiorno la data di modifica
                            $DOC_DATA['data_aggiornamento']=time();
                            //Incremento il numero di modifiche del documento
                            $DOC_DATA['n_aggiornamenti']=$DOC_ordini_pagamento_onp['n_aggiornamenti']+1;
                            
                            $update=dbAction::_update(array(
                                'tab'=>$TAB_doc_ordini_pagamento_onp,
                                'id'=>$DOC_ordini_pagamento_onp['id'],
                                'data'=>$DOC_DATA,
                                    //'echo'=>true,
                            ));
                        }
                        /* * * * * * * * * * * * * * * * * * * * * *
                         * CASO 2. IL DOCUMENTO - N O N - ESISTE 
                         */
                        else
                        {
                            $insert=dbAction::_insert(array(
                                'tab'=>$TAB_doc_ordini_pagamento_onp,
                                'data'=>$DOC_DATA,
                                    'echo'=>true,
                            ));
                        }
                    }
                    else
                    {
                        //donazioni_onp non trovate non accio nulla...
                    }
                    
                    
                }
                
            
            
            
            
        }
        return $a;
    }
}