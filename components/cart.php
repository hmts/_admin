<?php
class _CART
{
    function setLoop($a,$settings=null)
    {
        global $TAB_autori;
        global $TAB_editori;
        global $TAB_sot;
        global $TAB_ordini_spedizioni;
            
            
            
        if(!empty($settings['LOOP_CART']))
        {
            $L=$settings['LOOP_CART'];
        }
        else
        {
            pr("ERROR: set settings['LOOP_CART'] VAR ");
        }
            
            
            
        //pr($L);
            
            /*
                init vars
            */
                
            $TOT_NO_DISCOUNT="0.00";
            $TOT_TO_PAY="0.00";
                
            $TOT_QTA="0";
                
                
                
        for($i=0;$i<count($L);$i++)
        {
                
            //$a['LOOP']['CART']
            //pr($L);
            $add_w="";
            $set_w="";
            $echo="0";
            $settings=array(
                'tab'=>$L[$i]['t'],
                'field'=>'id',
                'value'=>$L[$i]['id'],
                'add_w'=>$add_w,
                'set_w'=>$set_w,
                'echo'=>$echo
                );
                    
            $r=dbAction::_record($settings);
            //pr($r);
                $r['qta']=$L[$i]['qta'];
                
                $AUTH=AUTH::isLogin("TAB_utenti");
                if($AUTH['b2b']=="1"){
                    $r['costo']=$r['costoB2B'];
                    $r['sconto']=$r['scontoB2B'];
                    $r['percentuale']=$r['percB2B'];
                }
                else
                {
                    $r['costo']=$r['costoB2C'];
                    $r['sconto']=$r['scontoB2C'];
                    $r['percentuale']=$r['percB2C'];
                
                }
                    
                    $r['risparmio']=number_format($r['costo']/100*$r['percentuale'], 2, '.', '');
                    if(empty($r['sconto']))$r['sconto']=number_format(($r['costo']-$r['risparmio']), 2, '.', '');
                    //echo '<p>sconto: '.$r['costo'].' - '.$r['risparmio'].'='.$r['sconto'].'</p>';
                    
                
                if($r['percentuale']!="0" && $r['percentuale']!="")
                {
                    $r['prezzo']=$r['sconto'];
                    $r['costo_barrato']='<span style="text-decoration:line-through">&euro;. '.$r['costo'].'</span>';
                    $r['prezzo_scrivi']=$r['costo_barrato'].'<br />';
                    $r['prezzo_scrivi'].='<span class="sconto">&euro;. '.$r['prezzo'].'</span>';
                    $r['perc']=$r['percentuale']."%";
                    $r['scrivi_costo_mini']=$r['qta']." x ".$r['costo_barrato']." ".'<span class="sconto">&euro;. '.$r['prezzo'].'</span>';
                }
                else
                {
                    $r['prezzo']=$r['costo'];
                    $r['prezzo_scrivi']='<span class="prezzo">&euro;. '.$r['prezzo'].'</span>';
                    $r['perc']="";
                    $r['scrivi_costo_mini']=$r['qta']." x ".'<span class="prezzo">&euro;. '.$r['prezzo'].'</span>';
                }
                    
                    /***********************************
                        
                        CALCOLO COSTI
                        
                    ***********************************/
                    
                    
                $costo_per_qta=number_format($r['costo']*$r['qta'], 2, '.', '');
                $TOT_NO_DISCOUNT=number_format($TOT_NO_DISCOUNT+$costo_per_qta, 2, '.', '');    
                    
                    
                $prezzo_per_qta=number_format($r['prezzo']*$r['qta'], 2, '.', '');    
                $TOT_TO_PAY=number_format($TOT_TO_PAY+$prezzo_per_qta, 2, '.', '');  
                    
                    
                    
                    
                $TOT_QTA=$TOT_QTA+$r['qta'];
                $TOT_PESO=$TOT_PESO+($r['peso']*$r['qta']);
                    
                    
                    
                $r['autori']=listAutoriLinks($r['REL'][$TAB_autori],array('divide'=>',','a_attribute'=>'class="autore"'));
                $r['autori_txt']=listAutoriLinks($r['REL'][$TAB_autori],array('divide'=>',','a_attribute'=>'class="autore"','only_txt'=>true));
                
                $r['editore']=$r['REL'][$TAB_editori];
                $r['editore_url']=MakeURL_editori($r['editore']);
                $r['editore_scrivi']='<a href="'.$r['editore_url'].'" class="editore">'.MakeNOME_editori($r['editore']).'</a>';
                    
                    
                    
                    
                $r['delQta']=URL_BUY."?id=".$L[$i]['id']."&t=".$L[$i]['t']."&delQta";
                $r['addQta']=URL_BUY."?id=".$L[$i]['id']."&t=".$L[$i]['t']."&addQta";
                    
                    
                $r['TABLE']=$L[$i]['t'];
                
            $a['LOOP']['CART'][]=$r;
            
        }
            //echo '<p>'.$TOT_NO_DISCOUNT.'</p>';
                
                /********************************
                    
                    SHIPPING
                    
                *********************************/
                $W_shipping=" WHERE peso_min<='".$TOT_PESO."' AND peso_max>='".$TOT_PESO."' ";
                $orderby=' ORDER BY posizione,nome';
                $SETT=array('tab'=>$TAB_ordini_spedizioni,'where'=>$W_shipping,'orderby'=>$orderby,'echo'=>'0');
                //$shipping=dbAction::_record($SETT);
                $shipping=dbAction::_loop($SETT);
                    
                $a['LOOP']['shipping']=$shipping;
                    
                //pr($shipping);
                       //pr($_COOKIE); 
                $a['R']['shipping']=json_decode(stripslashes($_COOKIE['SHIPPING']),true);
                    if(!isset($_COOKIE['SHIPPING']))
                    {
                        $value=json_encode($shipping[0]);
                        $date_exp=time()+80000000;
                        setcookie("SHIPPING",$value,$date_exp,"/");
                    $a['R']['shipping']=$shipping[0];
                    }
                        
                //pr($shipping_FIRST);
                $COSTO_SH=$a['R']['shipping']['costo'];
                //pr($TOT_NO_DISCOUNT);
                if($a['R']['shipping']['target']<=$TOT_NO_DISCOUNT){$COSTO_SH=$a['R']['shipping']['sconto'];}
                    
                    
            $a['CART_INFO']['TOT_NO_DISCOUNT']=number_format($TOT_NO_DISCOUNT, 2, '.', ' ');
            $a['CART_INFO']['TOT_TO_PAY']=number_format($TOT_TO_PAY, 2, '.', ' ');
                
            
                
            $a['CART_INFO']['SHIPPING_PRICE']=$COSTO_SH;
            $a['CART_INFO']['SHIPPING_PRICE_WRITE']="&euro;. ".$COSTO_SH;
                if($COSTO_SH=='0.00')
                {
                    $a['CART_INFO']['SHIPPING_PRICE_WRITE']='<span class="free">GRATIS</span>';
                }
            
            $a['CART_INFO']['PAYMENT_TYPE']="---";
                
            $a['CART_INFO']['TOT_DISCOUNT_ONLY']=number_format(($TOT_NO_DISCOUNT-$TOT_TO_PAY), 2, '.', ' ');
                
            $a['CART_INFO']['TOT_QTA']=$TOT_QTA;
            $a['CART_INFO']['TOT_PESO']=$TOT_PESO;
                
            $a['CART_INFO']['TOT_TO_PAY_PLUS']=number_format(($TOT_TO_PAY+$COSTO_SH), 2, '.', ' ');
            
            
        return $a;
    }
}