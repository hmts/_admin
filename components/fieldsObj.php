<?php
class outputfObj 
{
    function dateCalendar($timestamp)
    {
        $mmaa=date('M',$timestamp)." ".date('y',$timestamp);
        $dd = date('d',$timestamp);
        $hh = date('H:i:s',$timestamp);
    $HTML = ''    
          . '<div class="date">'
          . '<span class="mm">'.$mmaa.'</span>'
          . '<span class="dd">'.$dd.'</span>'
          . '<span class="hh">'.$hh.'</span>'
          . '</div>'
          . '';
    return $HTML; 
        }
}
class fieldsObj
{
    
    function range($name,$record,$min,$max,$step,$label=NULL,$style=NULL,$printMax=FALSE)
    {
        $label=$this->setLabel($label,$name);
        $style=$this->setStyle($style,"width:480px;padding:5px;");
        
        
        $HTML ='<script type="text/javascript">'.PHP_EOL;
        $HTML.='function updateTextInput_'.$name.'(val) {'.PHP_EOL;
        $HTML.='  document.getElementById(\'textInput_'.$name.'\').value=val; '.PHP_EOL;
        $HTML.='}'.PHP_EOL;
        $HTML.='</script>'.PHP_EOL;
        
        $HTML.='<li onMouseOver="toolTip(\''.$label.'\')" onMouseOut="toolTip()" class="editor range" '.$style.'>';
        $HTML.=' '.$label.' ';
        $HTML.='<input type="text" name="'.$name.'" id="textInput_'.$name.'" value="'.$record[$name].'" style="width:50px;padding:5px;text-align:center;" >';
        $HTML.='<input type="range" value="'.$record[$name].'" '
             . ' min="'.$min.'" max="'.$max.'" step="'.$step.'"  '
             . ' oninput="updateTextInput_'.$name.'(this.value);" '
             . ' onchange="updateTextInput_'.$name.'(this.value);" '
             . 'style="width:auto;border:3px solid; height: 10px;width:250px;margin:0 0 0 5px;padding:0px 0px 0px 0px;" />';
             
             //$HTML.=" Link Top";
        $HTML.='</li>';
        
        return $HTML;
    }
    
    
    
    function selFromTab($name,$record,$loopSettings,$label=NULL,$style=NULL,$note=NULL)
    {
        
        $records=dbAction::_loop($loopSettings);
        if($records)
        {
            $label=$this->setLabel($label,$name);
            $style=$this->setStyle($style,"width:473px;");
        
            if(!empty($note))
                $note='<div style="padding:5px;margin:5px;background:#ffffff;color:red;">'.$note.'</div>';
            
            $HTML ='
            <li onMouseOver="toolTip(\''.$label.'\')" onMouseOut="toolTip()" class="editor" style="padding:5px;margin:10px 0 5px 0;">
            <select name="'.$name.'" '.$style.' >
            <option value="">nessuno</option>';
            
            foreach ($records AS $I)
            {
                $sel_id = $I[$loopSettings['_INPUT_SELECT_']['id']];
                $sel_name =$I[$loopSettings['_INPUT_SELECT_']['name']];
                
                
                $HTML.='<option value="'.$sel_id.'" ';
                if($record[$name]==$sel_id) 
                    $HTML.=' selected ';
                $HTML.=' >'.$sel_name.' - '.$record[$name].'</option>';
            }
            $HTML.='
            </select>
            </li>
            '.$note;
            
        }
        return $HTML;
    }
    
    function selView($name,$record,$label=NULL,$style=NULL)
    {
        global $viewTypeArr;
        global $categoryFixedViewOnAreaSlug;
        global $TAB_Area;
        
        
        $showDefault = true;
        $SET_categoryFixedViewOnAreaSlug = $viewTypeArr[$record['TAB_NAME']];
        if( !empty($categoryFixedViewOnAreaSlug[$record['REL'][$TAB_Area]['slug']]) )
        {
            $showDefault = false;
            $SET_categoryFixedViewOnAreaSlug = $categoryFixedViewOnAreaSlug[$record['REL'][$TAB_Area]['slug']];
        }
        
        //pr($viewTypeArr[$record['TAB_NAME']]);
        //pr($categoryFixedViewOnAreaSlug[$record['REL'][$TAB_Area]['slug']]);
        //pr($SET_categoryFixedViewOnAreaSlug);
        
        if(isset($viewTypeArr[$record['TAB_NAME']]) && !empty($viewTypeArr[$record['TAB_NAME']]) && is_array($viewTypeArr[$record['TAB_NAME']]))
        {
            $label=$this->setLabel($label,$name);
            $style=$this->setStyle($style,"width:490px;");
        
            
            
            $HTML ='
            <li onMouseOver="toolTip(\''.$label.'\')" onMouseOut="toolTip()" class="" style="">
                <label class="aSide">'.$label.'</label>
            <select name="'.$name.'" '.$style.' >';
            
            if($showDefault)
                $HTML.='<option value="default">default</option>';
            
            if($record['slug']!="home")
            {
                foreach ($viewTypeArr[$record['TAB_NAME']] AS $v)
                {
                    if(in_array($v, $SET_categoryFixedViewOnAreaSlug))
                    {
                        $HTML.='<option value="'.$v.'" ';
                        if($record[$name]==$v) 
                            $HTML.=' selected ';
                        $HTML.=' >'.$v.'</option>';
                    }
                }
            }
            $HTML.='
            </select>
            </li>
            ';
            
        }
        return $HTML;
    }
    
    function siNo($name,$record,$label=NULL,$style=NULL,$disabled=NULL)
    {
        
        $label=$this->setLabel($label,$name);
        $style=$this->setStyle($style,"width:490px;");
        
        $HTML ='
        <li onMouseOver="toolTip(\''.$label.'\')" onMouseOut="toolTip()">
            <label class="aSide">'.$label.'</label>
        <select '.$disabled.' name="'.$name.'" '.$style.' >
        <option value="0">'.$label.' --> NO</option>
        <option value="1" ';
        if($record[$name]=='1') 
            $HTML.=' selected ';
        $HTML.=' >'.$label.'  --> SI</option>

        </select>
        </li>
        ';
        return $HTML;
    }
    
    function hidden($name,$value)
    {
        if($name=="lang" && empty($value))
        {
            $value='it';
        }
        return '<input type="hidden" name="'.$name.'" value="'.$value.'">';
    }
    
    function opt($name,$record,$values,$label=NULL,$style=NULL)
    {
        
        $label=$this->setLabel($label,$name);
        $style=$this->setStyle($style,"width:490px;");
        
        
        $HTML = '
        <li onMouseOver="toolTip(\''.$label.'\')" onMouseOut="toolTip()">
        <label class="aSide">'.$label.'</label>
        <select name="'.$name.'" '.$style.' >';
        
        
        if(is_array($values[0]) || getLanguages()==$values )
        {
            foreach ($values AS $k=>$v)
            {
                $HTML.='<option value="'.$k.'" ';
                if($record[$name]==$k) 
                    $HTML.=' selected ';
                $HTML.=' >'.$v.'</option>';
            }
        }
        else
        {
            foreach ($values AS $I)
            {
                $HTML.='<option value="'.$I.'" ';
                if($record[$name]==$I) 
                    $HTML.=' selected ';
                $HTML.=' >'.$I.'</option>';
            }        
        }
        
        
        $HTML.='
        </select>
        </li>
        ';
        return $HTML;
    }
    
    function extraFields($name,$record)
    {
        global $a;
        $label=$this->setLabel($label,$name);
        $style=$this->setStyle($style,"width:490px;");
        $HTML ='
        <li class="'.errClassSet($a['err_fields'],$name).'">
        <label class="aSide '.$labelClass.'" for="'.$name.'">'.$label.'</label>
        <input '.$readonly.' type="text" name="extrafields['.$name.']" value="'.$record['value'].'" id="'.$name.'" '.$style.'>
        </li>';
        
        
        return $HTML;
    }
    function text($name,$record,$label=NULL,$style=NULL,$id=true,$labelClass=NULL,$readonly=NULL)
    {
        
        global $a;
        
        $label=$this->setLabel($label,$name);
        $style=$this->setStyle($style,"width:490px;");
        
        $a['err_fields']=empty($a['err_fields'])?array():$a['err_fields'];
        $HTML ='
        <li class="'.errClassSet($a['err_fields'],$name).'">';
        
        $HTML.='<label class="aSide '.$labelClass.'" for="'.$name.'">'.$label.'</label>';
        $HTML.='
        <input '.$readonly.' type="text" name="'.$name.'" value="'.$record[$name].'" id="'.$name.'" '.$style.'>
        </li>';
        
        if($name=='slug' && empty($record[$name]))
        {
            $HTML=$this->hidden($name,$value);
        }
        
        
        return $HTML;
    }
    
    function number($name,$record,$label=NULL,$style=NULL,$id=true,$decimal=false)
    {
        $label=$this->setLabel($label,$name);
        $style=$this->setStyle($style,"width:490px;");
        
        global $a;
        
        $a['err_fields']=empty($a['err_fields'])?array():$a['err_fields'];
        $HTML ='
        <li class="'.errClassSet($a['err_fields'],$name).'">
        <label class="aSide" for="'.$name.'">'.$label.'</label>
        <input type="number" step="0.01" name="'.$name.'" value="'.$record[$name].'" id="'.$name.'" '.$style.'>
        </li>';
        
        if($name=='slug' && empty($record[$name]))
        {
            $HTML=$this->hidden($name,$value);
        }
        
        
        return $HTML;
    }
    
    function textEditor($name,$record,$label=NULL,$w=NULL,$h=NULL,$noLi=false,$mini=false)
    {
        $label=$this->setLabel($label,$name);
        
        $w=empty($w)?"488px":$w;
        $h=empty($h)?"130px":$h;
        
        global $a;
        
        if(!$noLi)
            echo '<li class="editor">';    
        //echo '<span class="txt">'.$label.'</span>';
        $editor_textarea['id']=$name;
        $editor_textarea['name']=$name;
        $editor_textarea['value']=$record[$name];
        $editor_textarea['w']=$w;
        $editor_textarea['h']=$h;
        
        
        
        if($mini)
        {
            include((_CORE::addView("admin/_elements/txt_editor_miny")));
        }
        else
        {
            include((_CORE::addView("admin/_elements/txt_editor")));
        }
        
        if(!$noLi)
            echo '</li>';
    }
        
    
    function setStyle($style,$default="width:490px;")
    {
        $style = empty($style)?$default:$style;
        $style = ' style="'.$style.'" ';
        return $style;
    }
    function setLabel($label,$name)
    {
        return empty($label)?$name:$label;
    }
    
}