<?php
class _CHRONOLOGY
{
    function rec($a)
    {
        //_CHRONOLOGY::clear($a); //Add a page
        _CHRONOLOGY::add($a); //Add a page
    }
    
    function add($a)
    {
            
        $CHRONOLOGY=_CHRONOLOGY::read($a);
        if(empty($CHRONOLOGY)) {$CHRONOLOGY=array();}
            
            
        /**************
            
            Add Page
            
            only if
            _STOP_CHRONOLOGY_
            is not true!
            
        ***************/
        if(!$a['_STOP_CHRONOLOGY_'])
        {
                
            //$p=_CHRONOLOGY::PARSE($_SESSION['CHRONOLOGY']['HTTP_REFERER'],'p');
            $p=_CHRONOLOGY::PARSE(rootWWW.$a['__URL'],'p');
                
            //if($p && $p!=_CHRONOLOGY::last_p($a))
            //pr($CHRONOLOGY['p']);
            $pExists=search_in_array($CHRONOLOGY['p'],"url",$p);
            //pr($pExists);
            if($p && empty($pExists))
            {
                if(!$CHRONOLOGY['p']) {$CHRONOLOGY['p']=array();}
                else{$CHRONOLOGY['p']=array_slice($CHRONOLOGY['p'],-9,9);}
                $CHRONOLOGY['p']=array_merge($CHRONOLOGY['p'],array(array('title'=>$a['title'],'url'=>$p)));
            }
        }        
            
            
            
            
            
        /**************
            
            Add Card Pro
            
        **************/
        if(isset($a['_CHRONOLOGY_PRODUCT_CARD_']))
        {
            //pr($a['_CHRONOLOGY_PRODUCT_CARD_']);
            $cardExists1=search_in_array($CHRONOLOGY['cards'],"id",$a['_CHRONOLOGY_PRODUCT_CARD_']['id']);
            if(empty($cardExists1))
            {
                    $CARDS_ADD=true;
            }
            else
            {
                $cardExists2=search_in_array($cardExists1,"table",$a['_CHRONOLOGY_PRODUCT_CARD_']['TAB_NAME']);
                if(empty($cardExists2))
                {
                    $CARDS_ADD=true;
                }
            }
            
            if($CARDS_ADD)
            {
                $CARDS_ADD=array(
                    'id'=>$a['_CHRONOLOGY_PRODUCT_CARD_']['id'],
                    'slug'=>$a['_CHRONOLOGY_PRODUCT_CARD_']['slug'],
                    'title'=>$a['_CHRONOLOGY_PRODUCT_CARD_']['titolo'],
                    'table'=>$a['_CHRONOLOGY_PRODUCT_CARD_']['TAB_NAME'],    
                    );
                
                //pr($a['_CHRONOLOGY_PRODUCT_CARD_']);
                if(!$CHRONOLOGY['cards']) {$CHRONOLOGY['cards']=array();}
                else{$CHRONOLOGY['cards']=array_slice($CHRONOLOGY['cards'],-9,9);}
                $CHRONOLOGY['cards']=array_merge($CHRONOLOGY['cards'],array($CARDS_ADD));
                
            }
        }
            
            
            
            
        /**************
            
            Add Query
                
        ***************/
        //pr($CHRONOLOGY);
        //$q=_CHRONOLOGY::PARSE($_SESSION['CHRONOLOGY']['HTTP_REFERER'],'q');
        $q=$a['QS'];
        //if($_GET['term']=="")echo '<p>'.$q.'</p>';
        $qExists=search_in_array($CHRONOLOGY['q'],"all",$q);
            
        if($q && isset($_GET['q']) && empty($qExists))
        {
            if(!$CHRONOLOGY['q']) {$CHRONOLOGY['q']=array();}
            else{$CHRONOLOGY['q']=array_slice($CHRONOLOGY['q'],-9,9);}
            $CHRONOLOGY['q']=array_merge($CHRONOLOGY['q'],array(array('all'=>$q,'q'=>$_GET['q'])));
        }
            
        //SAVE
        //pr($CHRONOLOGY);
        _CHRONOLOGY::save($CHRONOLOGY);
    }
    
    
    function PARSE($referer,$v)
    {
        /****************************************
            Return false if is not in rootWWW
        ****************************************/
        if(strpos($referer,rootWWW)===false)
        {
            return false;
        }
        /***************************************/
            
        //echo '<p>'.$referer.'</p>';
        $P=substr($referer, 0, strpos($referer, '?'));
            $QuestionMark_POS=strpos($referer, '?');
            if($QuestionMark_POS===false)
            {
                $Q=false;
            }
            else
            {
                $Q=substr($referer, $QuestionMark_POS); 
            }
           //echo '<p>'.$Q.'</p>';
        if(!$P && $v=='p'){return $referer;} 
        elseif($v=='p'){return $P;}
        elseif($Q && $v=='q'){return $Q;}
        return false;
    }
    
    function read($a,$settings=null)
    {
        if(isset($_COOKIE['CHRONOLOGY']))
        {
            $CHRONOLOGY=json_decode(stripslashes($_COOKIE['CHRONOLOGY']),true);    
                
                if(isset($settings['only_cards']))
                {
                    /*
                        No ID of this Card:$settings['only_cards']
                    */
                    for($i=0;$i<count($CHRONOLOGY['cards']);$i++)
                    {
                        if($CHRONOLOGY['cards'][$i]['id']!=$settings['only_cards'])
                        {
                            $CARDS[]=$CHRONOLOGY['cards'][$i];
                        }
                    }
                    //pr($CARDS);
                    return $CARDS;
                }
                
            return $CHRONOLOGY;   
        }
        return false;
    }
    
    function last_p($a)
    {
        $CHRONOLOGY=_CHRONOLOGY::read($a);
        //echo count($CHRONOLOGY['p']);
        $I=count($CHRONOLOGY['p'])-1;
        if($CHRONOLOGY)
        {
            return $CHRONOLOGY['p'][$I];
        }
        return fasle;
        
    }

    function last_q($a)
    {
        $CHRONOLOGY=_CHRONOLOGY::read($a);
        //echo count($CHRONOLOGY['p']);
        $I=count($CHRONOLOGY['q'])-1;
        if($CHRONOLOGY)
        {
            return $CHRONOLOGY['q'][$I];
        }
        return fasle;
        
    }

    
    function clear($a)
    {
        setcookie("CHRONOLOGY", '', 1,"/");
    }
    
    function save($value)
    {
        //pr($value);
        $value=json_encode($value);
        //pr($value);
        $date_exp=time()+80000000;
        setcookie("CHRONOLOGY", $value, $date_exp,"/");
    }
}