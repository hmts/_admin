<?php
ini_set ( 'max_execution_time', 9000000); 


class _DOC
{
    function _make($a,$settings)
    {
        
        $TIPO_DI_DOC=$settings['doc_type'];
        /*
         * CASI:
         * _donazioni
         * 
         */
        //$a=_DOC::_donazioni($a,$settings=null);
        
        $a=_DOC::$TIPO_DI_DOC($a,$settings);
        
        //$a['_DOC']['file'];
        //$a['_DOC']['content'];
        //write($path_file,$content,$mode='w');
        //
            //Se il file esiste prima lo cancello
            //per fare spazio ad uno più aggiornato
            
        @unlink($a['_DOC']['file']);
        write($a['_DOC']['file'],$a['_DOC']['content']);
        
        
        
        return $a;
    }
    
    function _donazioni($a,$settings)
    {
        
        if(empty($settings['template'])){
            $settings['template']=rootDOC."_views/_DOC/default.php";
        }
        $template=  file_get_contents($settings['template']);
        //$settings['title'];
            
        
        
        $template=str_replace("#ROOT_WWW#",rootWWW,$template);
        $template=str_replace("#TITLE#",$settings['title'],$template);
        $template=str_replace("#INTESTAZIONE#",$settings['intestazione'],$template);
        $template=str_replace("#DATI#",$settings['dati'],$template);
        
        $a['_DOC']['file']=rootDOC."_files/doc_donazioni/".$settings['file_name'].".html";
        $a['_DOC']['content']=$template;
        
        
        //CREO IL PDF
        $PDF_FILE=rootDOC."_files/doc_donazioni/".$settings['file_name'].".pdf";
            //
            //SE IL FILE ESISTE LO CANCELLO per tenere solo quello aggiornato
            //
            if(file_exists($PDF_FILE))
            {
                unlink($PDF_FILE);
            }
            
            ini_set('memory_limit', '-1');
            ini_set('display_errors',1);
            ini_set('display_startup_errors',1);
            error_reporting(-1);
            //
            
            $html2pdf = new HTML2PDF('P','A4','it');
            $html2pdf->WriteHTML($template);
            $html2pdf->Output($PDF_FILE, 'F');
                
            
            
        return $a;
    }
    
    
    function _ordine_pagamenti($a,$settings)
    {
        if(empty($settings['template'])){
            $settings['template']=rootDOC."_views/_DOC/default.php";
        }
        $template=  file_get_contents($settings['template']);
        //$settings['title'];
        
        //pr($settings);
        
        $template=str_replace("#ROOT_WWW#",rootWWW,$template);
        $template=str_replace("#TITLE#",$settings['title'],$template);
        $template=str_replace("#INTESTAZIONE#",$settings['intestazione'],$template);
        $template=str_replace("#DATI#",$settings['dati'],$template);
        
        $a['_DOC']['file']=rootDOC."_files/doc_ordini_pagamento/".$settings['file_name'].".html";
        $a['_DOC']['content']=$template;
        
        //CREO IL PDF
        $PDF_FILE=rootDOC."_files/doc_ordini_pagamento/".$settings['file_name'].".pdf";
            //
            //SE IL FILE ESISTE LO CANCELLO per tenere solo quello aggiornato
            //
            if(file_exists($PDF_FILE))
            {
                unlink($PDF_FILE);
            }
            ini_set('memory_limit', '-1');
            $html2pdf = new HTML2PDF('P','A4','it');
            $html2pdf->WriteHTML($template);
            $html2pdf->Output($PDF_FILE, 'F');
                
            
            
        return $a;
    }

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ### D O   I T ...
    function insertDonazioni($I,$annomese,$anno,$mese,$id_admin)
    {
        global $TAB_donazioni; 
        //$I['modalita_donazione'] "ore" "euro"
        //$I['importo']
        $ImportoSel=array(0,0,0,0);
        if(!empty($I['importo_selezionato']))
        {
            $ImportoSel=explode("|", $I['importo_selezionato']);
            $I['numero_ore_mese']="0";
        }
        
        //$I['valore_unora']*$I['numero_ore_mese']
        
        $ID_DOC_Donazioni=0;
        $DATI_donazione=array(
        'annomese'=>$annomese, 'mese'=>$mese, 'anno'=>$anno,
        'id_doc_donazioni'=>$ID_DOC_Donazioni, 'id_utenti'=>$I['id'], 'id_aziende'=>$I['id_aziende'], 'id_admin'=>$id_admin,
        'euro'=>$ImportoSel[2], 'ore'=>$I['numero_ore_mese'], 'valore_unora'=>$I['valore_unora'],
        'deduzione'=>$I['deduzione'], 'detrazione'=>$I['detrazione'],
        'id_onp50x100'=>$I['onp50x100'], 'euro_mese'=>$I['importo'], 'data'=>time()
        );
        
        $INS=dbAction::_insert(array('tab'=>$TAB_donazioni,'data'=>$DATI_donazione,'echo'=>false));
    }
    
    function getDonatoriList($a,$annomese,$anno,$mese,$id_admin,$insertDonazioni=true)
    {
        
        global $TAB_aziende;
        
        global $TAB_utenti; 
        $W = "WHERE ";
                    $W.= " id_utenti_cat='".$a['_CAT_UTENTI_']['donatori']."' ";
                    $W.= " AND stato='1' ";
                    
        $loop_donatori= dbAction::_loop(array(
            'tab'=>$TAB_utenti,
            'where'=>$W,
            'orderby'=>"ORDER BY id_aziende, cognome, nome",
        ));
        
        global $TAB_onlus;
        $loop_ONP= dbAction::_loop(array(
            'tab'=>$TAB_onlus,
            //'where'=>$W,
            //'orderby'=>"ORDER BY id_aziende, cognome, nome",
        ));
        if($loop_ONP)
            foreach ($loop_ONP AS $Onp){$ONP_A[$Onp['id']]=$Onp;}
            
        foreach ($loop_donatori AS $I)
        {
            $array_onlus_escluse =  explode(",", $I['REL'][$TAB_aziende]['onlus_escluse']);
            //Sistemo il campo onp50x100 in caso sia presente una ONP non attiva o esclusa
            if($I['onp50x100']!=0 && $I['onp50x100']!="0" && !empty($I['onp50x100']) )
            {
                $DatiOnp=$ONP_A[$I['onp50x100']];
                if(
                        !$DatiOnp || 
                        $DatiOnp['stato']!="1" || 
                        $DatiOnp['visibile']!="1" || 
                        in_array($array_onlus_escluse, $I['onp50x100'])
                        )
                {
                    $I['onp50x100']="0";
                }
            }
            
            
            
            //$I['modalita_donazione']="euro";
            if(!empty($I['importo_selezionato']))
            {
                $I['modalita_donazione']="euro"; //valore default
                $ImportoSel=explode("|", $I['importo_selezionato']);
                $I['importo']=$ImportoSel[2];
            }else{ //ORE! devo moltiplicare il valore selezionato per il numero_ore_mese
                $I['modalita_donazione']="ore"; //valore default
                $I['importo']=format_euro($I['valore_unora']*$I['numero_ore_mese']);
            }
            
            
            
            if($insertDonazioni)
                _DOC::insertDonazioni($I,$annomese,$anno,$mese,$id_admin);
            
            
            $loop_donatori_NEW[]=$I;
            
            //Get totals 
                if(!$IMPORTO_TOT_azienda[$I['id_aziende']]){$IMPORTO_TOT_azienda[$I['id_aziende']]="0,00";}
            $IMPORTO_TOT_donatori_azienda[$I['id_aziende']]++;
            $IMPORTO_TOT_azienda[$I['id_aziende']]=format_euro($IMPORTO_TOT_azienda[$I['id_aziende']]+$I['importo']);
            $IMPORTO_TOT=format_euro($IMPORTO_TOT+$I['importo']);
            
        }
        
        if($loop_donatori_NEW){
            $loop_donatori_NEW[0]['IMPORTI']=array(
                'IMPORTO_TOT'=>$IMPORTO_TOT,
                'IMPORTO_TOT_donatori_azienda'=>$IMPORTO_TOT_donatori_azienda,
                'IMPORTO_TOT_azienda'=>$IMPORTO_TOT_azienda
                );
            return $loop_donatori_NEW;
        }
    }
    
    function doit_donazioni($a)
    {
        
        global $TAB_doc_donazioni,$TAB_donazioni,
               $TAB_donazioni_info,$TAB_donazioni_onp,
               $TAB_utenti,$TAB_onlus,$TAB_aziende;
        
        $settings=$a['SETTINGS']; $a['TEMPLATE_DIR_WWW']=rootDOC."_template/_office/";
        
        $continua=false;
        if(is_natural_number($a['USER']['id']) && ($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] ||  $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']) )
        {$continua=true;}
        if(!$continua){return false;}
        
        $anno=empty($settings['anno'])?date('Y'):$settings['anno'];
        $mese=empty($settings['mese'])?date('m'):$settings['mese'];
        $annomese=empty($settings['annomese'])?date('Ym'):$settings['annomese'];
        $id_admin=$a['USER']['id'];
        
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        
        
        //Delete all Donation for this month/year
        dbAction::_delete(array('tab'=>$TAB_donazioni,'where'=>"WHERE annomese='".$annomese."'"));
        
        //Delete PDF 1. Recovering ID 2. Delete the file
        $docDonazioni=dbAction::_record(array('tab'=>$TAB_doc_donazioni,'set_w'=>"WHERE annomese='".$annomese."'"));
        unlink(rootDOC."_files/doc_donazioni/".$docDonazioni['id'].".pdf");
        
        //Delete doc donazioni
        dbAction::_delete(array('tab'=>$TAB_doc_donazioni,'where'=>"WHERE annomese='".$annomese."'"));
        
        //Delete donazioni_onp
        dbAction::_delete(array('tab'=>$TAB_donazioni_onp,'where'=>"WHERE annomese='".$annomese."'"));
        
        //Get List of "Donatori" & insertDonazioni
        if(!isset($a['GET_DATA']['delete'])){ 
            $loop_donatori=_DOC::getDonatoriList($a,$annomese,$anno,$mese,$id_admin);
        }
        //pr($loop_donatori[0]);
        
        //$loop_donatori[0]['IMPORTI']['IMPORTO_TOT'];
        //$loop_donatori[0]['IMPORTO_TOT_donatori_azienda']['IMPORTO_TOT'];
        //$loop_donatori[0]['IMPORTO_TOT_azienda']['IMPORTO_TOT'];
        
        $TOT_DONATORI=count($loop_donatori);
        
        htmlHelper::setFlash("msg","Aggiornamento Completato!");
        _CORE::redirect(array('location'=>URL_OFFICE."tab/".$TAB_donazioni."/"));
        
        return $a;
    }
    
    
    
    
    
    function doit_onp($a)
    {
        //pr($a['SETTINGS']);
        global $TAB_doc_donazioni,$TAB_donazioni,
               $TAB_donazioni_info,$TAB_donazioni_onp,
               $TAB_utenti,$TAB_onlus,$TAB_aziende;
        
        $settings=$a['SETTINGS']; 
        $donazioni=  dbAction::_loop(array(
            'tab'=>$TAB_donazioni,
            'where'=>"WHERE annomese='".$settings['annomese']."'"
            ));
        //pr($donazioni);
        $continua=false;
        if(is_natural_number($a['USER']['id']) && ($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] ||  $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']) )
        {$continua=true;}
        if(!$continua){return false;}
        
        $anno=empty($settings['anno'])?date('Y'):$settings['anno'];
        $mese=empty($settings['mese'])?date('m'):$settings['mese'];
        $annomese=empty($settings['annomese'])?date('Ym'):$settings['annomese'];
        $id_admin=$a['USER']['id'];
        
        //Delete donazioni_onp
        dbAction::_delete(array('tab'=>$TAB_donazioni_onp,'where'=>"WHERE annomese='".$annomese."'"));
        
        //pr($donazioni);
        
        
        if($donazioni)
        {
            foreach ($donazioni AS $I)
            {
                //if($MEM_AZIENDA_ID!=$I['REL'][$TAB_aziende]['id'])
                //{
                $ARRAY_ONLUS_ESCLUSE=array();
                if($I['REL'][$TAB_aziende]['onlus_escluse']){
                $ARRAY_ONLUS_ESCLUSE=explode(",", $I['REL'][$TAB_aziende]['onlus_escluse']);}
                    $ADDW="";
                    foreach ($ARRAY_ONLUS_ESCLUSE AS $ID_NO_ONP)
                    {
                        $ADDW.=" id!='".$ID_NO_ONP."' AND ";
                    }
                $OPN=dbAction::_loop(array(
                        'tab'=>$TAB_onlus,
                        'where'=>"WHERE ".$ADDW." (id_aziende='0' OR id_aziende='".$I['id_aziende']."') AND visibile='1' AND stato='1' ",
                        //'orderby'=>"ORDER BY ",
                        //'echo'=>true,
                        ));
                $TOT_ONP=count($OPN);
                $fair="100";
                //}
                
                if($TOT_ONP>1)
                {
                    if($I['id_onp50x100']!="0")
                    {
                        $TOT_ONP=$TOT_ONP-1;
                        $fair=round(100 / ($TOT_ONP) / 2 , 5);
                        
                        //$IMPORTO_singolaOnp=round(number_format($I['euro_mese'] / 2 / $TOT_ONP  , 3), 2,PHP_ROUND_HALF_DOWN);
                        $IMPORTO_singolaOnp=number_format($I['euro_mese'] / 2   , 3);
                        $IMPORTO_singolaOnp=my_round(number_format($IMPORTO_singolaOnp / $TOT_ONP  , 3));
                        
                    }
                    else
                    {
                        $fair=round(100 / ($TOT_ONP) , 5);
                        //$IMPORTO_singolaOnp=round(number_format($I['euro_mese'] / $TOT_ONP , 3), 2,PHP_ROUND_HALF_DOWN);
                        $IMPORTO_singolaOnp=my_round(number_format($I['euro_mese'] / $TOT_ONP , 3));
                    }
                }  
                
                foreach ($OPN AS $O)
                {
                    //$Scrivi_IMPORTO=round(number_format($IMPORTO_singolaOnp , 3) , 2,PHP_ROUND_HALF_DOWN);
                    //$Scrivi_IMPORTO=my_round(number_format($IMPORTO_singolaOnp , 2));
                    $Scrivi_IMPORTO=number_format($IMPORTO_singolaOnp , 2);
                    $Scrivi_fair=$fair;
                    $Scrivi_50x100=0;
                    
                    if($O['id']==$I['id_onp50x100'] && $I['id_onp50x100']!="0")
                    {
                        //$Scrivi_IMPORTO=round(number_format($I['euro_mese'] / 2 , 3), 2,PHP_ROUND_HALF_DOWN);
                        //$Scrivi_IMPORTO=my_round(number_format($I['euro_mese'] / 2 , 2));
                        $Scrivi_IMPORTO=my_round(number_format($I['euro_mese'] / 2 , 3));
                        $Scrivi_fair=50;
                        $Scrivi_50x100=$I['id_onp50x100'];
                    }
                    
                //$donazioni
                $ins=dbAction::_insert(array(
                    'tab'=>$TAB_donazioni_onp,
                    'data'=>array(
                        'id_donazioni'=>$I['id'],
                        'id_onlus'=>$O['id'],
                        'fair'=>$Scrivi_fair,
                        '50x100'=>$Scrivi_50x100,
                        'annomese'=>$annomese,
                        'importo'=>$Scrivi_IMPORTO,
                        
                    ),
                ));
                
                }
                $MEM_AZIENDA_ID=$I['REL'][$TAB_aziende]['id'];
            }
        }
        
        
        htmlHelper::setFlash("msg","Aggiornamento Completato!");
        _CORE::redirect(array('location'=>URL_OFFICE."tab/".$TAB_donazioni."/"));
        return $a;
    }
    
    
    function loopAziende($a,$IMPORTI)
    {
        global $TAB_aziende;
        
        $IMPORTO_TOT_donatori_azienda=$IMPORTI['IMPORTO_TOT_donatori_azienda'];
        $IMPORTO_TOT_azienda=$IMPORTI['IMPORTO_TOT_azienda'];
        
        $DATI_AZIENDE_mod=dbAction::_loop(array('tab'=>$TAB_aziende));
                    foreach ($DATI_AZIENDE_mod AS $AZ)
                    {
                        if($IMPORTO_TOT_donatori_azienda[$AZ['id']]!="0" && !empty($IMPORTO_TOT_donatori_azienda[$AZ['id']]))
                        {
                        unset($AZ['testo']);
                        unset($AZ['privacy']);
                        $AZ['importo']=$IMPORTO_TOT_azienda[$AZ['id']];
                        $AZ['tot_donatori']=$IMPORTO_TOT_donatori_azienda[$AZ['id']];
                        
                        $DATI_AZIENDE[]=$AZ;
                        }
                    }
                    //pr($DATI_AZIENDE);
                    
                return $DATI_AZIENDE;
    }
    
    
    
    function doit_doc_donazioni($a)
    {
        //pr($a['SETTINGS']);
        global $TAB_doc_donazioni,$TAB_donazioni,
               $TAB_donazioni_info,$TAB_donazioni_onp,
               $TAB_utenti,$TAB_onlus,$TAB_aziende;
        
        $settings=$a['SETTINGS']; 
        $donazioni=  dbAction::_loop(array(
            'tab'=>$TAB_donazioni,
            'where'=>"WHERE annomese='".$settings['annomese']."'"
            ));
        //pr($donazioni);
        $continua=false;
        if(is_natural_number($a['USER']['id']) && ($a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] ||  $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']) )
        {$continua=true;}
        if(!$continua){return false;}
        
        $anno=empty($settings['anno'])?date('Y'):$settings['anno'];
        $mese=empty($settings['mese'])?date('m'):$settings['mese'];
        $annomese=empty($settings['annomese'])?date('Ym'):$settings['annomese'];
        
        $id_admin=$a['USER']['id'];
        
        $a['R'][$TAB_doc_donazioni]=dbAction::_record(array(
            'tab'=>$TAB_doc_donazioni,
            'value'=>$settings['annomese'],
            'field'=>'annomese',
            //'echo'=>true
                    ));
        //
        $loop_donazioni=_DOC::getDonatoriList($a,$annomese,$anno,$mese,$id_admin,false);
        $DATI_AZIENDE=_DOC::loopAziende($a,$loop_donazioni[0]['IMPORTI'],$annomese);
        $TOT_AZIENDE=count($DATI_AZIENDE);
        $IMPORTO_TOT=$loop_donazioni[0]['IMPORTI']['IMPORTO_TOT'];
        $TOT_DONATORI=count($loop_donazioni);
        //
        
        $dati_doc=array(
            'id_utenti'=>$id_admin,
            'data_creazione'=>time(),
            'data_aggiornamento'=>time(),
            'n_aggiornamenti'=>1,
            'anno'=>$anno,'mese'=>$mese,'annomese'=>$annomese,
            'tot_donazioni'=>$TOT_DONATORI,
            'tot_aziende'=>$TOT_AZIENDE,
            'dati_aziende'=>  json_encode($DATI_AZIENDE),
            'importo_totale'=>$IMPORTO_TOT,
        );
        
        $ID_DOC=false;
        
        if(!$a['R'][$TAB_doc_donazioni])
        {
            //Doc creation
            $INS_doc=dbAction::_insert(array(
                    'tab'=>$TAB_doc_donazioni,
                    'data'=>$dati_doc,
                    //'echo'=>true,
                    )); 
            
            $ID_DOC=$INS_doc;
        }
        else
        {
            //Doc exists so update it
            $dati_doc['data_creazione']=$a['R'][$TAB_doc_donazioni]['data_creazione'];
            $dati_doc['n_aggiornamenti']=$a['R'][$TAB_doc_donazioni]['n_aggiornamenti']+1;
            
            $UPD_doc=dbAction::_update(array(
                    'tab'=>$TAB_doc_donazioni,
                    'id'=>$a['R'][$TAB_doc_donazioni]['id'],
                    'data'=>$dati_doc,
                    //'echo'=>true,
                    ));
            if($UPD_doc)
                $ID_DOC=$a['R'][$TAB_doc_donazioni]['id'];
        }
        
        //pr($ID_DOC);
        if($ID_DOC){
            $sql="UPDATE ".$TAB_donazioni." SET id_doc_donazioni='".$ID_DOC."' WHERE annomese='".$annomese."'";
            $eseguiSql=dbAction::_exec(array('return_result'=>true,'sql'=>$sql));
            //pr($sql);
        }
            
        
        //Crea Doc PDF...
        $a=_DOC::creaDocDonazioni($a,$ID_DOC,$dati_doc,$DATI_AZIENDE,$loop_donatori,$annomese,$anno,$mese);
        
        htmlHelper::setFlash("msg","Aggiornamento Completato!");
        _CORE::redirect(array('location'=>URL_OFFICE."tab/".$TAB_donazioni."/"));
        
        return $a;
    }
    

    
    function creaDocDonazioni($a,$ID_DOC,$DATA_doc,$DATI_AZIENDE,$loop_donatori,$annomese,$anno,$mese)
    {
        global $lang,$TAB_donazioni,$TAB_aziende,$TAB_utenti,$TAB_donazioni_onp;
        
                $HTML1 ="<br><br>";
                $HTML1.="Documento creato il: ".date("d-m-Y H:i:s",$DATA_doc['data_creazione'])."<br>";
                    if($DATA_doc['data_aggiornamento']!="0"){
                        $HTML1.="Ultima modifica del: ".date("d-m-Y H:i:s",$DATA_doc['data_aggiornamento'])."<br>";
                        $HTML1.="Numero di aggiornamenti: <b>".$DATA_doc['n_aggiornamenti']."</b><br>";
                    }
                $HTML1.="<b>Periodo Donazioni</b><br>  ";
                $HTML1.="Anno: ".$DATA_doc['anno']." Mese: ".$DATA_doc['mese']."<br>";
                    $HTML1.='<table>';
                    $HTML1.='<tr><td><a href="'.rootWWW.'"><img src="'.rootWWW.'icona-mini.png" width="15" height="15" border="0"></a></td>';
                    $HTML1.='<td><b><a style="text-decoration:none;color:#941b20;" href="'.rootWWW.'">UNORA.ORG</a></b></td></tr></table>';
                $HTML1.="<br><br>  ";
                
                $HTML2 ="<br>";
                $HTML2.="<b>Importo Totale:</b> &euro;.".$DATA_doc['importo_totale']."<br>  ";
                $HTML2.="<b>Totale Donatori:</b> ".$DATA_doc['tot_donazioni']."<br>  ";
                $HTML2.="<b>Totale Aziende:</b> ".$DATA_doc['tot_aziende']."<br>  ";
                $HTML2.="<br>  ";
                
                    $HTML2.='<table border="0" cellspacing="5" cellpadding="3">';
                    //INTESTAZIONE
                    $HTML2.='<tr>';
                    $HTML2.='<td>&nbsp;</td>';
                    $HTML2.='<td>Aziende</td>';
                    $HTML2.='<td>Totale</td>';
                    $HTML2.='<td>Modalit&agrave; Donazione</td>';
                    
                    $HTML2.='</tr>';
                    
                    
                //unset($DATI_AZIENDE[0]);
                    
                foreach ($DATI_AZIENDE as $ITEM) {
                    if($ITEM['tot_donatori'])
                    {
                    //pr($ITEM);
                    $HTML2.='<tr bgcolor="#ffffcc">';
                    //pr($ITEM);
                        $URLIMG=$ITEM['IMG2']['.png']['url'];
                        $URLIMG2=str_replace(rootWWW,rootDOC,$URLIMG);
                        $IMG="  ";
                        if(file_exists($URLIMG2)){
                        $IMG='<img src="'.$URLIMG.'" width="90">';
                        }
                    $HTML2.='<td bgcolor="#ffffff" style="width:90px;"><div>'.$IMG.'</div></td>';
                    $HTML2.='<td style="width:400px;"><div style="font-size:15px;padding:5px;">';
                    $HTML2.=''.$ITEM['titolo'].'<br><font style="font-size:12px;">Donatori: '.$ITEM['tot_donatori'].'</span></div></td>';
                    
                    //$HTML2.='<td style="width:15px;">';
                    //$HTML2.="";
                    //$HTML2.="</td>";
                    $HTML2.='<td style="width:80px;" align="right">';
                    $HTML2.="&euro;. ".$ITEM['importo']."&nbsp;";
                    $HTML2.="</td>";
                    
                    $HTML2.='<td style="width:180px;">';
                    $HTML2.='<img src="'.$a['TEMPLATE_DIR_WWW'].'_img/'.$ITEM['modalita_donazione'].'-32.png" width="25" height="25" border="0">';
                    $HTML2.=" ".$ITEM['modalita_donazione'];
                    $HTML2.="</td>";
                    $HTML2.="</tr>";
                    
                    
                        //Memorizzo donazioni divise per azienda
                        $donazioni[$ITEM['id']]['REL'][$TAB_aziende]=$ITEM;
                        $donazioni[$ITEM['id']]['DONAZIONI']=  dbAction::_loop(array(
                            'tab'=>$TAB_donazioni,
                            'where'=>"WHERE annomese='".$annomese."' AND id_aziende='".$ITEM['id']."'"
                        ));  
                    
                    }
                }
                
                    $HTML2.="<tr>";
                    $HTML2.="<td>&nbsp;</td>";
                    $HTML2.="<td>&nbsp;</td>";
                    $HTML2.='<td bgcolor="#ffffcc" style="width:80px;" align="right">';
                    $HTML2.="&euro;. ";
                    $HTML2.="".$DATA_doc['importo_totale']."&nbsp;";
                    $HTML2.="</td>";
                    $HTML2.="<td>&nbsp;</td>";
                    $HTML2.="</tr>";
                    $HTML2.="</table>";    
                    $HTML2.="<br><br>";    
                    $HTML2.='<div style="background-color:#FF9900;"><h1 style="color:#ffffff;"> &nbsp; Lista Donazioni - '.$lang['mesi'][$DATA_doc['mese']].' '.$DATA_doc['anno'].' </h1></div><br>';
                    
                    
                  
                    
                 
                    //pr($donazioni);
                    if($donazioni)
                    {
                        $loop_count="1";
                        $HTML3="";
                        
                            $count_righe=0;
                        foreach($donazioni AS $Iaziende)    
                        {
                            
                            $HTML3.='<page>';
                            
                            $HTML3.='<table cellspacing="2" cellpadding="5">';
                            
                            $HTML3.='<tr bgcolor="#ffffff">';
                            $HTML3.='<td colspan="4">';
                            $HTML3.='<div style="padding:5px;font-size:18px;">';
                            $HTML3.='<b>'.$Iaziende['REL'][$TAB_aziende]['titolo'].'</b>';
                            $HTML3.='</div>';


                            $HTML3.='</td>';
                            $HTML3.='</tr>';
                            $count_righe=$count_righe+3;
                            
                            //Intestazioni
                            $HTML3.='<tr bgcolor="#afafaf">';
                            $HTML3.='<td style="width:440px;"><div style="color:#ffffff;padding:5px;font-size:11px;">Donatore</div></td>';
                            $HTML3.='<td style="width:130px;"><div style="color:#ffffff;padding:5px;font-size:11px;">Importo Mese</div></td>';
                            $HTML3.='<td align="center" style="width:80px;"><div style="color:#ffffff;padding:5px;font-size:11px;text-align:left;">Deduzione</div></td>';
                            $HTML3.='<td align="center" style="width:80px;"><div style="color:#ffffff;padding:5px;font-size:11px;text-align:left;">Detrazione</div></td>';
                            $HTML3.='</tr>';
                            $HTML3.='</table>';
                            $count_righe=$count_righe+2;
                            
                            $countDonazioni=1;
                            foreach($Iaziende['DONAZIONI'] AS $I)    
                            {
                            $HTML3.='<table cellspacing="2" cellpadding="5">';
                            
                            $bg1="#fff0b8"; $bg2="#ebddb5";if($I['tr_class']=="uno"){$bg1="#ffffcc"; $bg2="#f0f0d3";}
                            $HTML3.='<tr bgcolor="'.$bg1.'">';    
                            $HTML3.='<td style="width:440px;">';
                            $HTML3.='<div style="padding:5px;">';
                            $HTML3.=$countDonazioni.') <img src="'.rootWWW.'_template/_office/_img/stato-utente-1.png" width="18" height="18" >';
                            $HTML3.='&nbsp;';

                            $HTML3.='<a style="color:#941b20;text-decoration:none;" href="'.URL_OFFICE.'tab/'.$TAB_utenti.'/upd/'.$I['REL'][$TAB_utenti]['id'].'/">';
                            $HTML3.='<b>'.$I['REL'][$TAB_utenti]['nome'].'</b> ';
                            $HTML3.=' <b>'.$I['REL'][$TAB_utenti]['cognome'].'</b>';
                            $HTML3.='</a>';
                            //$HTML3.='<br>'.$I['REL'][$TAB_aziende]['titolo'];
                            $HTML3.='</div>';
                            //$HTML3.='</div>';
                            $HTML3.='</td>';
                            $HTML3.='<td style="width:130px;">';
                            $HTML3.='<img src="'.rootWWW.'_template/_office/_img/euro-21.png">';
                            $HTML3.=' &nbsp;&euro;. '.$I['euro_mese'].'';
                            $HTML3.='</td>';
                                $img_deduzione=rootWWW.'_template/_office/_img/sel-off-16.png';
                                $img_detrazione=rootWWW.'_template/_office/_img/sel-off-16.png';
                                //Set on, if active:
                                if($I['deduzione']=="1"){$img_deduzione=rootWWW.'_template/_office/_img/sel-16.png';}
                                if($I['detrazione']=="1"){$img_detrazione=rootWWW.'_template/_office/_img/sel-16.png';}
                            $HTML3.='<td align="center" style="width:80px;">';
                            $HTML3.='<img src="'.$img_deduzione.'">';
                            $HTML3.='</td>';
                            $HTML3.='<td align="center" style="width:80px;">';
                            $HTML3.='<img src="'.$img_detrazione.'">';
                            $HTML3.='</td>';
                            $HTML3.="</tr>";
                            $count_righe=$count_righe+2;
                            
                                //For each donation print out onlus fair
                                $ONP=dbAction::_loop(array('tab'=>$TAB_donazioni_onp,
                                    'where'=>"WHERE annomese='".$annomese."' AND id_donazioni='".$I['id']."'"));
                                if($ONP)
                                {
                                    $totaleOnp=count($ONP);
                                    
                                    $HTML3.='<tr bgcolor="'.$bg1.'">';
                                    $HTML3.='<td colspan="4">';
                                    $HTML3.="<div style='padding:3px;'>Onp (".$totaleOnp."):</div><table>";
                                        
                                    $count_righe=$count_righe+1;
                                    
                                    $conta_onp="1";
                                    $tot_importo_onp_singolo=0;
                                    foreach ($ONP AS $onp)
                                    {
                                        $bgcolor="#efefef";$color="#000000";
                                        if($conta_onp % 2=='0'){$bgcolor="#eeeeee";$color="#000000";}
                                        elseif($onp['fair']=="50"){$bgcolor="#FF9900";$color="#ffffff";}
                                    $HTML3.='<tr bgcolor="'.$bgcolor.'">';
                                    $HTML3.='<td style="width:210px;">';
                                    $HTML3.='<span style="color:'.$color.';">'.$a['_ONLUS_'][$onp['id_onlus']].'</span>';
                                    $HTML3.='</td>';
                                    $HTML3.='<td>';
                                    $HTML3.='<span style="color:'.$color.';">'.$onp['fair'].'%</span>';;
                                    $HTML3.='</td>';
                                    $HTML3.='<td align="right">';
                                    $HTML3.='<span style="color:'.$color.';">&euro;.'.$onp['importo'].' </span>';;
                                    $HTML3.='</td>';
                                    $HTML3.="</tr>";
                                    $HTML3.="";
                                    
                                    $count_righe=$count_righe+1;
                                    
                                    $tot_importo_onp_singolo=  format_euro($tot_importo_onp_singolo+$onp['importo']);
                                    $tot_importo_onp=  format_euro($tot_importo_onp+$onp['importo']);
                                    $conta_onp++;
                                    }
                                    $HTML3.='<tr><td>&nbsp;</td><td>&nbsp;</td>';
                                    $HTML3.='<td align="right">&euro;. '.$tot_importo_onp_singolo.'</td>';
                                    $HTML3.='</tr>';
                                    
                                    $HTML3.='<tr><td>&nbsp;</td><td>&nbsp;</td>';
                                    $HTML3.='<td align="right">&euro;. '.$tot_importo_onp.'</td>';
                                    $HTML3.='</tr>';
                                    
                                    $count_righe=$count_righe+1;
                                    
                                    $HTML3.="</table></td></tr>";
                                }
                                $HTML3.='</table>';
                                
                                
                                
                            $countDonazioni++;
                            }
                            
                            $HTML3.='</page>';
                            
                               
                        $loop_count++;
                        }
                        
                        $HTML3.='<div style="padding:5px;font-size:18px;">';
                        $HTML3.='<br><br>Totale donazioni divise per ONP: <br>&euro;. '.$tot_importo_onp.'<br><br>';
                        $HTML3.='</div>';
                    }
                    
            
                    /*
                     * Echo Per il debug
                    echo'<table align=center><tr><td bgcolor=#ffffff>';
                    echo $HTML1.$HTML2.$HTML3;
                    echo'</td></tr></table>';
                    */
                    
                    //SAVE DOC
                    $settings['doc_type']="_donazioni";
                    $settings['file_name']=$ID_DOC;
                    $settings['intestazione']=$HTML1;
                    $settings['dati']=$HTML2.$HTML3;
                    $a=_DOC::_make($a,$settings);
                    
    }
    
    
    
    
    function doit_ordini_pagamento($a)
    {
        global $lang;
        //pr($lang);
        global $TAB_doc_ordini_pagamento;
        
        global $TAB_doc_donazioni;
        
        global $TAB_donazioni;
        global $TAB_donazioni_info;
        global $TAB_donazioni_onp;
        
        global $TAB_utenti;
        global $TAB_utenti_cat;
        
        global $TAB_onlus;
        global $TAB_aziende;
        
        //pr($a['USER']['id']);
        $settings=$a['SETTINGS'];
        
        
        
        //if(is_natural_number($a['USER']['id']))
        if(is_natural_number($a['USER']['id']) && 
                (
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] || 
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']
                ) 
           )
        {
        /* * * * * * * * * * * * * * * * 
         *
         *
         *  Preparo tutte le variabili
         *  
         *
        * * * * * * * * * * * * * * * */
                
            //
            $anno=empty($settings['anno'])?date('Y'):$settings['anno'];
            $mese=empty($settings['mese'])?date('m'):$settings['mese'];
            $annomese=$anno.$mese;
              
            
            
            $anno_e_mese[0]=substr($annomese,0,4);
            $anno_e_mese[1]=$lang['mesi'][(int)substr($annomese,4,2)];
                        
            
            
                // Last Day of the Mont:
                // -- > date("t");
                
            //
            $id_admin=$a['USER']['id'];
            /*
             * 1. Cercare ordine di pagamento già presente, ed in caso aggironarlo.
             * 2. Cercare Doc_Donazione attivo e lavorare su quello
             *      Diviso per ogni azienda: 
             *      2.1 quindi prendere tutte le donazioni
             *      2.2 salvarle in un array per "congelare" una foto della situazione
             *      2.3 calcolare i totali degli importi da bonificare
             *      2.4 prepararee, report, doc e inviarli via email ai ref_azienda
             *      2.5 conlcusione... si rimane in attesa delle comunicazioni di bonifici.
             */
            // 1. Verifico se esiste già un ordine di pagamento
            /*
             * Prelevo il documento di donazioni corrente annomese
             * 
             */
            $DOC_Donazioni=dbAction::_record(array('tab'=>$TAB_doc_donazioni,'value'=>$annomese,'field'=>'annomese'));
            //pr($DOC_Donazioni);
            
            //Fermo tutto se non c'è un DOC di Donazioni
            if(!$DOC_Donazioni)
            {
                htmlHelper::setFlash("err","Errore:<br>non &egrave; stato possibile effettuare la<br>Creazione del documento di Ordine di Pagamento<br>Non &grave presente un documento di Donazioni");
                return $a;
            }
            
            
            //Esiste un documento di donazione
                //Eseguo import anche delle relative donazioni
                //e delle informazioni sulle onlus.
                $loop_donazioni=dbAction::_loop(array('tab'=>$TAB_donazioni,'where'=>"WHERE id_doc_donazioni='".$DOC_Donazioni['id']."'"));
                foreach ($loop_donazioni AS $I)
                {
                    $I['REL'][$TAB_donazioni_onp]=dbAction::_loop(array('tab'=>$TAB_donazioni_onp,'where'=>"WHERE id_donazioni='".$I['id']."'"));
                    $loop_donazioni_new[]=$I;
                    
                    unset($I['REL'][$TAB_aziende]);
                    $loop_donazioni_aziende[$I['id_aziende']][]=$I;
                }
                $loop_donazioni=$loop_donazioni_new;
                $TOT_DONAZIONI=count($loop_donazioni);
                //pr($loop_donazioni);
            
                //A fronte di un Doc_donazioni ci sono 'n' doc_ordine_pagamenti 
                //dove 'n' è il numero delle aziende attive 
                ////verso le quali si deve effettuare un ordine di pagamento 
                //Adesso verifico se i doc_ordine_pagamenti esiste 
                //e quindi se è da creare o da aggiornare.
                
                // Le aziende attive vengono prese dal DOC_DONAZIONI e non dall'elenco 
                // delle aziende del db perchè questo potrebbe nel frattempo essere cambiato
                
                $ELENCO_AZIENDE=json_decode($DOC_Donazioni['dati_aziende'],true);
                //pr($ELENCO_AZIENDE);exit;
                
                
                $WHERE_Docs="WHERE annomese='".$annomese."' ";
                //Cancello tutti i doc presenti
                $DOCs_ordine_pagamenti=dbAction::_loop(array(
                    'tab'=>$TAB_doc_ordini_pagamento,
                    'where'=>$WHERE_Docs,
                    //'echo'=>true
                    ));
                if($DOCs_ordine_pagamenti)
                {
                    foreach ($DOC_ordine_pagamenti AS $docs)
                    {
                        //http://www.unora.org/_files/doc_ordini_pagamento/14.pdf
                        @unlink(rootDOC."_files/doc_ordini_pagamento/".$docs['id'].".pdf");
                    }
                    
                    $DELETE_DOCs_ordine_pagamenti=  dbAction::_delete(array(
                        'tab'=>$TAB_doc_ordini_pagamento,
                        'where'=>$WHERE_Docs,
                    ));
                }
                
                
                //Seguo il loop delle aziende per verificare 
                //e poi creare o modificare il doc_ordine _pagamenti
                foreach ($ELENCO_AZIENDE AS $I)
                {
                    
                    $DONAZIONI_LOOP_AZIENDA=$loop_donazioni_aziende[$I['id']];
                    
                    //pr($I['id']);
                    //pr($I);
                    
                    
                    //unset($DONAZIONI_LOOP_AZIENDA['REL'][$TAB_aziende]);
                    //pr($DONAZIONI_LOOP_AZIENDA['REL'][$TAB_aziende]);
                    $DATI_DOC_ordine_pagamenti=array(
                    'id_utenti'=>$id_admin,
                    'id_aziende'=>$I['id'],
                    'data_creazione'=>time(),
                    'data_aggiornamento'=>'0',
                    'n_aggiornamenti'=>'0',
                    'anno'=>$DOC_Donazioni['anno'],
                    'mese'=>$DOC_Donazioni['mese'],
                    'annomese'=>$DOC_Donazioni['annomese'],
                    'donazioni'=>  json_encode($DONAZIONI_LOOP_AZIENDA),
                    'tot_donazioni'=>$I['tot_donatori'],
                    'importo'=>$I['importo'],
                    
                    );
                    
                    
                    //pr($DATI_DOC_ordine_pagamenti);
                    
                    //Non essite devo creare
                    
                    $INS_Doc=dbAction::_insert(array(
                    'tab'=>$TAB_doc_ordini_pagamento,
                    'data'=>$DATI_DOC_ordine_pagamenti,
                    
                    ));
                    
                    
                    $ID_DOC_Ordine_Pagamenti=$INS_Doc;
                    
                
                
            
                
                $HTML1 ="<br><br>";
                
                $HTML1.="Documento creato il: ";
                //$HTML1.=date("d-m-Y H:i:s",$DATI_DOC_ordine_pagamenti['data_creazione'])."<br>";
                
                $HTML1.=date("d",$DATI_DOC_ordine_pagamenti['data_creazione'])." ";
                $HTML1.=substr($lang['mesi'][date("n",$DATI_DOC_ordine_pagamenti['data_creazione'])],0,3)." ";
                $HTML1.=date("Y",$DATI_DOC_ordine_pagamenti['data_creazione'])." ";
                $HTML1.=' alle ore: '.date("H:i s",$DATI_DOC_ordine_pagamenti['data_creazione'])." ";
                
                
                
                if($DATI_DOC_ordine_pagamenti['data_aggiornamento']!="0"){
                $HTML1.="<br>";
                $HTML1.="Ultima modifica del: ";
                //.date("d-m-Y H:i:s",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])."<br>";
                $HTML1.=date("d",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])." ";
                $HTML1.=substr($lang['mesi'][date("n",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])],0,3)." ";
                $HTML1.=date("Y",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])." ";
                $HTML1.=' alle ore: '.date("H:i s",$DATI_DOC_ordine_pagamenti['data_aggiornamento'])." ";
                
                
                $HTML1.="<br>";
                $HTML1.="Numero di aggiornamenti: <b>".$DATI_DOC_ordine_pagamenti['n_aggiornamenti']."</b><br>";
                }
                $HTML1.="<b>Periodo Donazioni</b><br>  ";
                $HTML1.="Anno: ".$DATI_DOC_ordine_pagamenti['anno']." Mese: ".$DATI_DOC_ordine_pagamenti['mese']."<br>";
                    $HTML1.='<table>';
                    $HTML1.='<tr><td><a href="'.rootWWW.'"><img src="'.rootWWW.'icona-mini.png" width="15" height="15" border="0"></a></td>';
                    $HTML1.='<td><b><a style="text-decoration:none;color:#941b20;" href="'.rootWWW.'">UNORA.ORG</a></b></td></tr></table>';
                $HTML1.="<br><br>  ";
                
                
                
                
                
                /* * * * * * * * * * * * * * * * * * * * * * * * *
                 *
                 * Prima pagina con i dati di riepilogo
                 *  
                 * * * * * * * * * * * * * * * * * * * * * * * * */
                
                $HTML2 ='';
                
                $IMPORTO=$DATI_DOC_ordine_pagamenti['importo'];
                
                $HTML2.='<div style="font-size:28px;">';
                $HTML2.='<b>Ordine di Pagamento</b> <br>';
                $HTML2.='Per le Donazioni del Periodo: ';
                $HTML2.='<b style="color:#941b20">'.$anno_e_mese[1].' '.$anno_e_mese[0].'</b><br>';
                $HTML2.='</div>';
                $HTML2.='<br><br>';
                $HTML2.='<div style="width:690px;padding:20px;background-color:#ffffcc;border:3px solid orange;border-radius:9px;-moz-border-radius:9px;-webkit-border-radius:9px;">';
                $HTML2.='<table>';
                $HTML2.='<tr>';
                $HTML2.='<td>';
                $HTML2.='<img src="'.rootWWW.'_template/_office/_img/euro-128.png" width="90" height="90" >';
                $HTML2.='</td>';
                
                $HTML2.='<td>';
                $HTML2.='<div style="color:orange;font-size:65px;padding:20px;">';
                $HTML2.='<b>';
                $HTML2.='&euro;.'.number_format($IMPORTO,'2',',','.');
                $HTML2.='</b>';
                $HTML2.='</div>';
                
                $HTML2.='</td>';
                $HTML2.='</tr>';
                $HTML2.='</table>';
                $HTML2.='</div>';
                
                $HTML2.='<br><br>';
                
                $HTML2.='<div style="font-size:18px;padding:20px 20px 0px 20px;">';
                $HTML2.='Totale donazioni: '.$DATI_DOC_ordine_pagamenti['tot_donazioni'];
                $HTML2.='</div>';
                $HTML2.='<div style="font-size:18px;padding:0px 20px 0px 20px;">';
                $HTML2.='Ultma modifica di: '.$a['USER']['nome'];
                $HTML2.=' '.$a['USER']['cognome'].' <i style="color:#909090;">('.$a['USER']['REL'][$TAB_utenti_cat]['nome'].')</i>';
                $HTML2.='</div>';
                
                
                
                //$HTML2.='<pre>'.print_r($DATI_DOC_ordine_pagamenti, true).'</pre>';
                
                
                
                
                
                
                
                
                
                
                
                
                /* * * * * * * * * * * * * * * * * * * * * * * * *
                 *
                 * Seconda pagina con i dati delle donazioni
                 *  
                 * * * * * * * * * * * * * * * * * * * * * * * * */
                
                
                $HTML3='<page>';
                //pr($DONAZIONI_LOOP_AZIENDA);
                if($DONAZIONI_LOOP_AZIENDA)
                {
                //$HTML3.='<table cellspacing="0" cellpadding="1"><tr><td>';
                $HTML3.='<table cellspacing="2" cellpadding="5">';
                $HTML3.='<tr bgcolor="#afafaf">';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;">Donatore</div></td>';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;">Importo Mese</div></td>';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;text-align:center;">Deduzione</div></td>';
                $HTML3.='<td><div style="color:#ffffff;padding:5px;font-size:11px;text-align:center;">Detrazione</div></td>';
                $HTML3.='</tr>';
                
                
                foreach ($DONAZIONI_LOOP_AZIENDA AS $Idonazioni)
                {
                    
                    if($Idonazioni['tr_class']=="uno")
                    {
                    $bg1="#ffffcc";
                    $bg2="#f0f0d3";
                    }
                    else
                    {
                    $bg1="#fff0b8";
                    $bg2="#ebddb5";
                    }
                    
                    
                $HTML3.='<tr bgcolor="'.$bg1.'">';
                $HTML3.='<td style="width:440px;">';
                //$HTML3.='<div style="color:#941b20;padding:5px;font-size:11px;">';
                $HTML3.='<div style="padding:5px;">';
                
                $HTML3.='<img src="'.rootWWW.'_template/_office/_img/stato-utente-1.png" width="18" height="18" >';
                $HTML3.='&nbsp;';
                
                $HTML3.='<a style="color:#941b20;text-decoration:none;" href="'.URL_OFFICE.'tab/'.$TAB_utenti.'/upd/'.$Idonazioni['REL'][$TAB_utenti]['id'].'/">';
                $HTML3.='<b>'.$Idonazioni['REL'][$TAB_utenti]['nome'].'</b> ';
                $HTML3.=' <b>'.$Idonazioni['REL'][$TAB_utenti]['cognome'].'</b>';
                $HTML3.='</a>';
                $HTML3.='</div>';
                //$HTML3.='</div>';
                $HTML3.='</td>';
                $HTML3.='<td style="width:130px;">';
                $HTML3.='<img src="'.rootWWW.'_template/_office/_img/euro-21.png">';
                $HTML3.=' &nbsp;'.$Idonazioni['euro_mese'].' Euro';
                $HTML3.='</td>';
                    
                    //Set off:
                    $img_deduzione=rootWWW.'_template/_office/_img/sel-off-16.png';
                    $img_detrazione=rootWWW.'_template/_office/_img/sel-off-16.png';
                    //Set on, if active:
                    if($Idonazioni['deduzione']=="1"){$img_deduzione=rootWWW.'_template/_office/_img/sel-16.png';}
                    if($Idonazioni['detrazione']=="1"){$img_detrazione=rootWWW.'_template/_office/_img/sel-16.png';}
                    
                $HTML3.='<td align="center" style="width:80px;">';
                $HTML3.='<img src="'.$img_deduzione.'">';
                $HTML3.='</td>';
                $HTML3.='<td align="center" style="width:80px;">';
                $HTML3.='<img src="'.$img_detrazione.'">';
                $HTML3.='</td>';
                
                
                $HTML3.='</tr>';
                $HTML3.='<tr bgcolor="'.$bg2.'">';
                $HTML3.='<td colspan="4">';
                    $imp_sel=$Idonazioni['REL'][$TAB_utenti]['importo_selezionato'];
                    $imp_sel=explode("|",$imp_sel);
                    
                
                    //Tipo di importo selezionato
                    $HTML3.='<div style="padding:5px;color:#22a12c;">';
                    if($imp_sel[1]=="euro")
                    {
                    $HTML3.='<img src="'.rootWWW.'_template/_office/_img/euro-32.png" width="20" height="20">';
                    $HTML3.='&nbsp;';
                    $HTML3.='Importo Mensile selezionato: '.$imp_sel[0];
                    $HTML3.=' (Valore in euro: '.$imp_sel[2].')'; 
                    }
                    else
                    {
                    $HTML3.='<img src="'.rootWWW.'_template/_office/_img/ore-32.png" width="20" height="20">';
                    $HTML3.='&nbsp;';
                    //$HTML3.='Valore Euro di Un Ora: '.$imp_sel[2].' - ';
                    $HTML3.='Un Ora: &euro;.'.$Idonazioni['REL'][$TAB_utenti]['valore_unora'].' - ';
                    $HTML3.=' Ore/Mese donate: ';
                    $HTML3.=$Idonazioni['REL'][$TAB_utenti]['numero_ore_mese']; 
                    }
                    $HTML3.='</div>';
                    
                //$HTML3.=''.$Idonazioni['REL'][$TAB_utenti]['importo_selezionato'];
                
                $HTML3.='</td>';
                $HTML3.='</tr>';
                
                $HTML3.='<tr bgcolor="'.$bg1.'">';
                $HTML3.='<td colspan="4">';
                    
                    /* * * * * * * * * * * * * * * * * * * * * * * * * 
                     *  
                     *   L O O P   O N P 
                     *    --> start()
                     *  
                     * * * * * * * * * * * * * * * * * * * * * * * * */
                //$HTML3.='<pre>'.print_r($Idonazioni, true).'</pre>';
                if($Idonazioni['REL']['donazioni_onp']){
                    
                    $HTML3.='<div style="padding:3px;color:#afafaf;">';
                    $HTML3.='<b>ONP Selezionate</b>';
                    $HTML3.='</div>';
                    
                    $HTML3.='<table>';
                    //pr($Idonazioni['REL']['donazioni_onp']);exit;
                    foreach ($Idonazioni['REL']['donazioni_onp'] AS $ONP)
                    {
                        
                        if($ONP['tr_class']=="uno"){$BGColor="#FFECE3";}else{$BGColor="#F4E4DD";}
                        
                        $HTML3.='<tr bgcolor="'.$BGColor.'"';
                            if($ONP['50x100']!="0"){$HTML3.=' bgcolor="orange" style="font-size:18px;color:#ffffff;" ';}
                        $HTML3.='>';
                        $HTML3.='<td style="width:650px;">';
                        $HTML3.='<div style="padding:3px;">';
                        $HTML3.=''.$a['ONP_BY_ID'][$ONP['id_onlus']]['titolo'].'';
                        $HTML3.='</div>';
                        $HTML3.='</td>';
                        $HTML3.='<td style="width:90px;">';
                        $HTML3.='<div style="padding:3px;">';
                        $HTML3.=''.(float)$ONP['fair'].'%';
                        $HTML3.='</div>';
                        $HTML3.='</td>';
                        $HTML3.='</tr>';
                    }
                    $HTML3.='</table>';
                }
                $HTML3.='</td>';
                $HTML3.='</tr>';
                
                }
                $HTML3.='</table>';
                $HTML3.='</page>';
                
                //$HTML3.='</td></tr></table>';
                }
                
                //COMPLETO IL SALVATAGGIO DEL DOC
                $settings['doc_type']="_ordine_pagamenti";
                $settings['file_name']=$ID_DOC_Ordine_Pagamenti;
                $settings['intestazione']=$HTML1;
                $settings['dati']=$HTML2.$HTML3;
                $a=_DOC::_make($a,$settings);
                
                
                // Adesso, dopo che il doc è ronto
                // deve essere inviato via email ad ogni singolo
                // Utente Azienda.
                // 
                // Per evitare però che vengano effettuati
                // innumerevoli invii ogni vlta che viene 
                // aggiornato il file
                // Metto in schedule le email da inviare...
                // usando il metodo _schedule di class _EMAIL_ALERT 
                    
                //Loop dei refernti azienda della azienda attuale
                //$I['id']
                //pr($I);
                $ref_azienda=dbAction::_loop(array('tab'=>$TAB_utenti,'where'=>"WHERE id_aziende='".$I['id']."' AND id_utenti_cat='".$a['_CAT_UTENTI_']['ref_azienda']."'"));
                //pr($a['_CAT_UTENTI_']);
                
                    if($ref_azienda){
                        $TO=array();
                    foreach ($ref_azienda AS $rA)
                    {
                        $TO[]=$rA['email'];
                    }
                    //
                            
                        $ALLEGATO="_files/doc_ordini_pagamento/".$ID_DOC_Ordine_Pagamenti.".pdf";
                        
                        $CODICE_RIFERIMENTO="OrP_".$ID_DOC_Ordine_Pagamenti."_".$annomese;
                        
                        $URL_AT_OFFICE=URL_OFFICE."tab/".$TAB_doc_ordini_pagamento."/upd/".$ID_DOC_Ordine_Pagamenti."/";
                            
                        $TXT ='';
                        
                        $TXT.='<div class="'.time().'">';
                        $TXT.='<b>Ordine di pagamento</b><br>';
                        $TXT.='Il documento in allegato riporta il riepilogo ';
                        $TXT.=' completo dei dati relativi alle donazioni registrate ';
                        $TXT.='nel periodo '.$anno_e_mese[1].' '.$anno_e_mese[0].".";
                        $TXT.='<br>Puoi accedere subito alla <a href="'.$URL_AT_OFFICE.'">pagina relativa a questo conto</a><br>';
                        $TXT.='';
                        $TXT.='<br><br>';
                        $TXT.='</div>';
                        
                        $TXT.='<div style="border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;margin:5px;border:2px orange solid;background-color:#ffffcc;padding:15px;">';
                            
                            $TXT.='<div style="float:left;width:70px;" class="'.time().'">';
                            $TXT.='<img src="'.rootWWW.'_template/_office/_img/euro-64.png" border="0">';
                            $TXT.='</div>';
                            $TXT.='<div style="float:left;text-align:left;font-size:15px;" class="'.time().'">';
                            $TXT.='Totale dell\'importo da bonificare:<br>'; 
                            $TXT.='<b style="color:orange;font-size:25px;">'.$I['importo'].' Euro</b>';
                            $TXT.='</div>';
                            $TXT.='<div style="clear:both;"></div>';
                            
                        $TXT.='</div>';
                        
                        $TXT.='<br>';
                        $TXT.='';
                        $TXT.='';
                        //pr($DATI_DOC_ordine_pagamenti['importo']);
                        //pr($DONAZIONI_LOOP_AZIENDA);
                        
                        
                    $S=array(
                    'to'=>$TO,
                    //'from'=>'', //Se vuoto viene preso da $SMTP_DATA['mittente']
                    'oggetto'=>'Donazioni '.$anno_e_mese[1].' '.$anno_e_mese[0],
                    'testo'=>$TXT,
                    'allegato'=>$ALLEGATO,
                    'inviato'=>'0',
                    'codice_riferimento'=>$CODICE_RIFERIMENTO,
                    'id_utenti'=>$a['USER']['id'],
                    'data_creazione'=>time(),
                    //'dont_send_before'=>'',
                    );
                        
                        //pr($S);
                    
                    _EMAIL_ALERT::_schedule($a,$S);
                    }

                }//foreach ($ELENCO_AZIENDE AS $I)
                
                
                
                
                
                
        htmlHelper::setFlash("msg","Ordine di pagamento Aggiornato.");
        _CORE::redirect(array('location'=>URL_OFFICE."tab/".$TAB_doc_ordini_pagamento."/"));
        exit;
        }
        
        
        return $a;
    }
    
    
    
    
    
    function doit_donazioni_onp($a)
    {
        
        
        
        global $lang;
        //pr($lang);
        global $TAB_doc_ordini_pagamento;
        global $TAB_doc_ordini_pagamento_onp;
        
        global $TAB_doc_donazioni;
        
        global $TAB_donazioni;
        global $TAB_donazioni_info;
        global $TAB_donazioni_onp;
        
        global $TAB_utenti;
        global $TAB_utenti_cat;
        
        global $TAB_onlus;
        global $TAB_aziende;
        
        //pr($a['USER']['id']);
        $settings=$a['SETTINGS'];
        
        
        
        //if(is_natural_number($a['USER']['id']))
        if(is_natural_number($a['USER']['id']) && 
                (
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['unora'] || 
                $a['USER']['id_utenti_cat']==$a['_CAT_UTENTI_']['robot']
                ) 
           )
        {
            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * 
             *  Creo i record ed i documenti
             * 
             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
            $anno=empty($settings['anno'])?date('Y'):$settings['anno'];
            $mese=empty($settings['mese'])?date('m'):$settings['mese'];
            $annomese=$anno.$mese;
              
            
            
            $anno_e_mese[0]=substr($annomese,0,4);
            $anno_e_mese[1]=$lang['mesi'][(int)substr($annomese,4,2)];
                        
            
            
                // Last Day of the Mont:
                // -- > date("t");
                
            //
            $id_admin=$a['USER']['id'];
         
            
            if(isset($_GET['force']))
            {
                $del2=dbAction::_delete(array(
                    'tab'=>$TAB_donazioni_onp,
                    'where'=>"WHERE annomese='".$annomese."'"
                    
                    
                    ));
                
                
                
                
                
            }
            
            /*
             * 
             * PRIMA CONTROLLO CHE SIA STATO FATTO UN DOCUMENTO DI DONAZIONI
             * 
             */
            $DOC_Donazioni=dbAction::_record(array('tab'=>$TAB_doc_donazioni,'value'=>$annomese,'field'=>'annomese'));
            //pr($DOC_Donazioni);
            
            //Fermo tutto se non c'è un DOC di Donazioni
            if(!$DOC_Donazioni)
            {
                htmlHelper::setFlash("err","Errore:<br>non &egrave; stato possibile effettuare la<br>Creazione del documento di Ordine di Pagamento<br>Non &grave presente un documento di Donazioni");
                return $a;
            }
            
            
            
            
            
            
            //LOOP ONLUS
            $loop_onlus=dbAction::_loop(array('tab'=>$TAB_onlus,
                'where'=>"WHERE visibile='1' AND stato='1'" //LO STATO è ANCPRA DA INTEGRARE per le ONP e le AZIENDE
                ));
                foreach ($loop_onlus AS $onp_item)
                {
                    //
                    //Per ogni onlus dove trovo donazioni_onp registrate per il periodo dato
                    //creo / aggiorno il documento
                    //
                    $donazioni_onp=dbAction::_loop(array('tab'=>$TAB_donazioni_onp,'where'=>"WHERE annomese='".$annomese."' AND id_onlus='".$onp_item['id']."'"));
                    
                    $mysql_access = connect();
                    $ww=" WHERE annomese='".$annomese."' AND id_onlus='".$onp_item['id']."' ";
                    $s="SELECT SUM(importo) FROM ".$TAB_donazioni_onp." ".$ww;
                    $count=mysql_query($s,$mysql_access);
                    $totale_donaizoni_euro=mysql_fetch_array($count);
                    
                    //pr($s);
                    //pr("ONP --> ".$onp_item['titolo']." TOT: ".$totale_donaizoni_euro[0]);
                    //pr(count($donazioni_onp));
                    //    
                    //
                    //
                    if($donazioni_onp)
                    {
                        /*
                         * OK DONAZIONI TROVATE
                         * devo creare il DOC
                         * 
                         */
                        
                        /*
                         * DEVO FARE IL LOOP delle donazioni_onp e ricavare l'importo totale
                         */
                        $TOT_DONAZIONI_ONP="0";
                        //pr($donazioni_onp);
                        foreach ($donazioni_onp AS $donazioni_onp_item)
                        {
                            //pr($donazioni_onp_item['importo']);
                            $TOT_DONAZIONI_ONP=  format_euro($TOT_DONAZIONI_ONP+$donazioni_onp_item['importo']);
                        }
                        
                        
                        
                        
                        
                            //
                        //
                        // Inserimento in TAB doc_ordini_pagamento_onp
                        // $TAB_doc_ordini_pagamento_onp
                        //
                            //
                        $DOC_ordini_pagamento_onp=dbAction::_record(array(
                            'tab'=>$TAB_doc_ordini_pagamento_onp,
                            'set_w'=>"WHERE annomese='".$annomese."' AND id_onlus='".$onp_item['id']."'",
                            ));
                        
                        /* * * * * * * * * * * * * * * * * * * * * *
                         * CASO 1. IL DOCUMENTO ESISTE
                         */
                            
                            //PREPARO I DATI
                            $DOC_DATA=array(
                                'id_utenti'=>$a['USER']['id'],
                                'id_onlus'=>$onp_item['id'],
                                'data_creazione'=>time(),
                                'anno'=>$anno,
                                'mese'=>$mese,
                                'annomese'=>$annomese,
                                'importo'=>$TOT_DONAZIONI_ONP,
                                
                                
                            );
                            
                        if($DOC_ordini_pagamento_onp)
                        {
                            /*
                             * AGGIORNO DATE (creazione e modifica) E N° AGGIORNAMENTI
                             */
                            //1. lascio la data di creazione come era
                            $DOC_DATA['data_creazione']=$DOC_ordini_pagamento_onp['data_creazione'];
                            //Aggiorno la data di modifica
                            $DOC_DATA['data_aggiornamento']=time();
                            //Incremento il numero di modifiche del documento
                            $DOC_DATA['n_aggiornamenti']=$DOC_ordini_pagamento_onp['n_aggiornamenti']+1;
                            
                            $update=dbAction::_update(array(
                                'tab'=>$TAB_doc_ordini_pagamento_onp,
                                'id'=>$DOC_ordini_pagamento_onp['id'],
                                'data'=>$DOC_DATA,
                                    //'echo'=>true,
                            ));
                        }
                        /* * * * * * * * * * * * * * * * * * * * * *
                         * CASO 2. IL DOCUMENTO - N O N - ESISTE 
                         */
                        else
                        {
                            $insert=dbAction::_insert(array(
                                'tab'=>$TAB_doc_ordini_pagamento_onp,
                                'data'=>$DOC_DATA,
                                    'echo'=>true,
                            ));
                        }
                    }
                    else
                    {
                        //donazioni_onp non trovate non accio nulla...
                    }
                    
                    
                }
                
            
            
            
            
        }
        
        htmlHelper::setFlash("msg","Aggiornamento Completato!");
        _CORE::redirect(array('location'=>URL_OFFICE."tab/".$TAB_doc_ordini_pagamento_onp."/"));
        
        //return $a;
    }
}